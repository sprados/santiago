﻿//AGREGAR EN ESTE JS LAS FUNCIONES QUE SE DEBEN EJECUTAR EN AL FINAL DEL BODY DE LA MASTER. ESTO ES AL FINAL DE TODO, JUNTO A LAS DEMAS JS DE CHARISMA Y BOOTSTRAP.


//ACCORDION MENU PRINCIPAL
$(document).ready(function () {
    $('.accordion > a').click(function (e) {
        e.preventDefault();
        var $ul = $(this).siblings('ul');
        var $li = $(this).parent();
        if ($ul.is(':visible')) $li.removeClass('active');
        else $li.addClass('active');
        $ul.slideToggle();
    });
    $('.accordion li.active:first').parents('ul').slideDown();
});



//Impedir botones Back y Forward del Navegador (probado OK en Chrome y Firefox)
history.pushState({ page: 1 }, "title 1", "#nbb");
window.onhashchange = function (event) { window.location.hash = "nbb"; };




//PARA QUE FUNCIONEN LOS DIFERENTES CONTROLES JQUERY BOOTSTRAP LUEGO DE UNA UPDATEPANEL ASYNCCALLBACK
//IMPORTANTE: asegurarse que la página aspx NO implemente el script "function pageLoad()" porque pisará a esta y no funcionarán las declaradas aquí.
function pageLoad() {
        
    $('[data-rel="popover"],[rel="popover"]').popover(); //POPOVER
    $('[data-rel="chosen"],[rel="chosen"]').chosen(); //CHOSEN DROPDOWNLIST 
    $('.myDatePicker').datepicker({ dateFormat: 'dd/mm/yy' }); //DATEPICKER
    var mpe = $find("MPE"); if (mpe != null) mpe.add_shown(onShown); //HACER CLICK FUERA DE UN MODAL POPUP Y QUE SE CIERRE.
    $('input[type = radio], input[type = checkbox]').uniform(); //ES PARA DEJAR EL STYLE DE LOS CHECKBOX DESPUES DE UN ASYNC POSTBACK. INCLUIR NUEVOS EN EL FUTURO SI ES NECESARIO.
    $("#ob_Grid1TrialDiv").hide(); //oculta el div OBOUT GRID que avisa que es version trial.
    $(".ob_gPSTT").hide(); //oculta el PAGINADOR de OBOUT GRID


    //DESHABILITA TECLA "ENTER" EN TODOS LOS TEXTBOXES (excepto en los que pertenezcan la clase "habilitarEnter")
    $("input:text").keypress(function (event) {
        var exceptuar = $(document.activeElement).is(".habilitarEnter");
        if (event.keyCode === 13 && !exceptuar) {
            event.preventDefault();
            return false;
        }
    });
    //DESHABILITA TECLA "F5". Aún funciona el botón Refresh del navegador
    $(document).bind("keydown", disableF5);
    function disableF5(e) {
        if ((e.which || e.keyCode) == 116) e.preventDefault();
    };
    //DESHABILITA TECLA "BACKSPACE" (excepto en los TextBox y TextArea).
    $(document).keydown(function (e) {
        var elid = $(document.activeElement).is("input:focus, textarea:focus");
        if (e.keyCode === 8 && !elid) {
            return false;
        };
    });

    //MONTH PICKER
    $('.monthPicker').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        autoSize: true,
        monthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        dateFormat: 'mm/yy',
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
    });
    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
    
    
}






//PARA DATEPICKER DENTRO DE UPDATEPANEL. SIMPLEMENTE A LOS TEXTBOX PONER QUE USEN LA CSSCLASS=myDatePicker
$(document).ready(function () {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        $('.myDatePicker').datepicker({ dateFormat: 'dd/mm/yy' });
    }
});



//ClickOver: Similar al popover pero hay que hacer click sobre un boton para que se dispare el popover.. Agregada el 26/07/2013
$(function () {
    $('[rel="clickover"]').clickover();
})



//Deshabilitar ENTER en los INPUTs, para que no se produzca un submit form. (METODO B)
//Hay que poner en el TextBox la siguiente propiedad: 
//OnKeyPress = "return disableEnterKey(event)"
function disableEnterKey(e) {
    var key;
    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //firefox      

    return (key != 13);
}



//Usado para Progress Modal de Normal Postbacks (Buttons que no están en Update Panels)
function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("modalNormalPostBack");
        $('body').append(modal);
        var loading = $(".loadingNormalPostBack");
        loading.show();
        loading.css({ top: '31%', left: '43%' });
        if (!Page_IsValid) {
            //Si hay algún validator no válido, ocultar el loading y volver a la normalidad.
            modal.removeClass("modalNormalPostBack");
            loading.hide();
        }
    }, 5); //Tiempo de espera.

}
