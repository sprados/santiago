﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CSMMonitor.clases
{
    public class NumeroLetra
    {
        private string[] sUnidades = 
        {"", "un", "dos", "tres", "cuatro", "cinco", 
        "seis", "siete", "ocho", "nueve", "diez", 
        "once", "doce", "trece", "catorce", 
        "quince", "dieciseis", 
        "diecisiete", "dieciocho", "diecinueve", "veinte", 
        "veintiún", "veintidos", "veintitres", "veinticuatro",
        "veinticinco", "veintiseis", "veintisiete", 
        "veintiocho", "veintinueve"};

        private string[] sDecenas = 
        { "", "diez", "veinte", "treinta", "cuarenta", "cincuenta",
        "sesenta", "setenta", "ochenta", "noventa" };

        private string[] sCentenas = 
        { "", "ciento", "doscientos", "trescientos", 
        "cuatrocientos",
        "quinientos", "seiscientos", "setecientos", 
        "ochocientos", "novecientos" };


        private string sResultado = "";


        public string ConvertirCadena(string sNumero)
        {
            double dNumero;
            double dNumAux = 0;
            char x;
            string sAux;

            sResultado = " ";
            try
            {
                dNumero = Convert.ToDouble(sNumero);
            }
            catch
            {
                return "";
            }

            if (dNumero > 999999999999)
                return "";

            if (dNumero > 999999999)
            {
                dNumAux = dNumero % 1000000000000;
                sResultado += Numeros(dNumAux, 1000000000) + " mil ";
            }

            if (dNumero > 999999)
            {
                dNumAux = dNumero % 1000000000;
                sResultado += Numeros(dNumAux, 1000000) + " millones ";
            }

            if (dNumero > 999)
            {
                dNumAux = dNumero % 1000000;
                sResultado += Numeros(dNumAux, 1000) + " mil ";
            }

            dNumAux = dNumero % 1000;
            sResultado += Numeros(dNumAux, 1);

            //Enseguida verificamos si contiene punto, 
            //si es así, los convertimos a texto.
            sAux = dNumero.ToString();

            if (sAux.IndexOf(".") >= 0)
                sResultado += ObtenerDecimales(sAux);

            //Las siguientes líneas convierten 
            //el primer caracter a mayúscula.
            sAux = sResultado;
            x = char.ToUpper(sResultado[1]);
            sResultado = x.ToString();

            for (int i = 2; i < sAux.Length; i++)
                sResultado += sAux[i].ToString();

            return sResultado;
        }


        public string ConvertirNumero(double dNumero)
        {
            double dNumAux = 0;
            char x;
            string sAux;

            sResultado = " ";

            if (dNumero > 999999999999)
                return "";

            if (dNumero > 999999999)
            {
                dNumAux = dNumero % 1000000000000;
                sResultado += Numeros(dNumAux, 1000000000) + " mil ";
            }

            if (dNumero > 999999)
            {
                dNumAux = dNumero % 1000000000;
                sResultado += Numeros(dNumAux, 1000000) + " millones ";
            }

            if (dNumero > 999)
            {
                dNumAux = dNumero % 1000000;
                sResultado += Numeros(dNumAux, 1000) + " mil ";
            }

            dNumAux = dNumero % 1000;
            sResultado += Numeros(dNumAux, 1);


            //Enseguida verificamos si contiene punto, 
            //si es así, los convertimos a texto.
            sAux = dNumero.ToString();
            string varDecimales = "";
            if (sAux.IndexOf(".") >= 0)
            {
                varDecimales = ObtenerDecimales(sAux);
            }
            else
            {
                varDecimales = "";
            }

            //MEJORAS EN TEXTO:
            sResultado += varDecimales;
            sResultado = sResultado.Trim();
            sResultado = sResultado.Replace("  ", " ");

            if (sResultado.Length > 5 && sResultado.Substring(0, 5) == " con ")
                sResultado = sResultado.Remove(0, 5);
            
            sResultado = sResultado.Replace(" y un", " y uno");
            sResultado = sResultado.Replace("ciento mil ", "cien mil ");
            
            if (sResultado.Length > 9 && sResultado.Substring(sResultado.Length - 8, 8) == "veintiún")
                sResultado = sResultado.Replace("veintiún", "veintiuno");

            if (sResultado.Contains("veintiún con"))
                sResultado = sResultado.Replace("veintiún con", "veintiuno con");

            if (sResultado.Length > 9 && sResultado.Substring(sResultado.Length - 6, 6) == "ciento")
                sResultado = sResultado.Replace("ciento", "cien");

            if (sResultado.Contains("ciento con"))
                sResultado = sResultado.Replace("ciento con", "cien con");
            //FIN MEJORAS EN TEXTO

            //el primer caracter a mayúscula.
            x = char.ToUpper(sResultado[0]);
            sResultado = x + sResultado.Remove(0, 1);
            return sResultado;
        }


        private string Numeros(double dNumAux, double dFactor)
        {
            double dCociente = dNumAux / dFactor;
            double dNumero = 0;
            int iNumero = 0;
            string sNumero = "";
            string sTexto = "";

            if (dCociente >= 100)
            {
                dNumero = dCociente / 100;
                sNumero = dNumero.ToString();
                iNumero = int.Parse(sNumero[0].ToString());
                sTexto += this.sCentenas[iNumero] + " ";
            }

            dCociente = dCociente % 100;
            if (dCociente >= 30)
            {
                dNumero = dCociente / 10;
                sNumero = dNumero.ToString();
                iNumero = int.Parse(sNumero[0].ToString());
                if (iNumero > 0)
                    sTexto += this.sDecenas[iNumero] + " ";

                dNumero = dCociente % 10;
                sNumero = dNumero.ToString();
                iNumero = int.Parse(sNumero[0].ToString());
                if (iNumero > 0)
                    sTexto += "y " + this.sUnidades[iNumero] + " ";
            }

            else
            {
                dNumero = dCociente;
                sNumero = dNumero.ToString();
                if (sNumero.Length > 1)
                    if (sNumero[1] != '.')
                        iNumero = int.Parse(sNumero[0].ToString() +
                            sNumero[1].ToString());
                    else
                        iNumero = int.Parse(sNumero[0].ToString());
                else
                    iNumero = int.Parse(sNumero[0].ToString());
                sTexto += this.sUnidades[iNumero] + " ";
            }

            return sTexto;
        }


        private string ObtenerDecimales(string sNumero)
        {
            string[] sNumPuntos;
            string sTexto = "";
            double dNumero = 0;
            sNumPuntos = sNumero.Split('.');
            dNumero = Convert.ToDouble(sNumPuntos[1]);
            string s1 = sNumPuntos[1];
            string sub = s1.Substring(0, 1);
            string sub1 = "0";
            if (sub != sub1)
            {
                if(dNumero < 10)
                    dNumero = dNumero * 10;
            }
            sTexto = "con " + Numeros(dNumero, 1) + "centavos";

            return sTexto;
        }
    }   
} 
