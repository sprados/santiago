﻿using System;
using System.Drawing;
using helpers.helpers.barcode;

namespace CSMMonitor.clases
{
    public partial class BuildBarCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string barCode = "";
            if (Session["barCodeToPrint"] != null && Session["barCodeToPrint"].ToString() != "")
            {
                barCode = Session["barCodeToPrint"].ToString();

                Interleaved2of5 barcode = new Interleaved2of5(barCode);
                string s = barcode.Encoded_Value;
                Bitmap bitmap = barcode.Generate_Image(s, 640, 70);


                Response.ContentType = "image/jpeg";
                bitmap.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                bitmap.Dispose();
            }
            else
            {
                Response.Write("[ERROR AL GENERAR CÓDIGO DE BARRA]");
            }
        }
    }
}