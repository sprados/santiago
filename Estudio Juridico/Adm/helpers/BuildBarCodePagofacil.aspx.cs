﻿using System;
using System.Drawing;
using helpers.helpers.barcode;

namespace CSMMonitor.clases
{
    public partial class BuildBarCodePagofacil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string barCode = "";
            if (Session["barCodePagofacil"] != null && Session["barCodePagofacil"].ToString() != "")
            {
                barCode = Session["barCodePagofacil"].ToString();

                Interleaved2of5 barcode = new Interleaved2of5(barCode);
                string s = barcode.Encoded_Value;
                Bitmap bitmap = barcode.Generate_Image(s, 440, 30);


                Response.ContentType = "image/jpeg";
                bitmap.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                bitmap.Dispose();
            }
            else
            {
                Response.Write("[ERROR AL GENERAR CÓDIGO DE BARRA]");
            }
        }
    }
}