﻿using Estudio_Juridico.Adm.clases;
using helpers.helpers.barcode;
using System;
using System.Drawing;

namespace CSMMonitor.clases
{
    public partial class BuildBarCodeRoela : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string barCode = "";
            if (Session["barCodeRoela"] != null && Session["barCodeRoela"].ToString() != "")
            {
                barCode = Session["barCodeRoela"].ToString();

                Interleaved2of5 barcode = new Interleaved2of5(barCode);
                string s = barcode.Encoded_Value;
                Bitmap bitmap = barcode.Generate_Image(s, 440, 30);


                Response.ContentType = "image/jpeg";
                bitmap.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                bitmap.Dispose();
            }
            else
            {
                Response.Write("[ERROR AL GENERAR CÓDIGO DE BARRA]");
            }
        }

        /// <summary>
        /// Devuelve el codigo de barras numérico de Roela con 56 dígitos, listo para armar el cod de barras optico.
        /// </summary>
        /// <param name="idComprobante">Id Comprobante de Pago</param>
        /// <param name="primVto">Fecha del Primer Vencimiento (Formato YYYYMMDD)</param>
        /// <param name="primMonto">Monto del Primer Vencimiento (En formato como está en la BD. Con o Sin valores decimales)</param>
        /// <param name="cantDiasSegVto">Cantidad de días corridos entre el 1er y 2do Vencimiento. Si no usa segundo vencimiento, poner "00". Puede ser 1 digito o 2.</param>
        /// <param name="segMonto">Monto del Primer Vencimiento (En formato como está en la BD. Con o Sin valores decimales)</param>
        /// <param name="cantDiasTerVto">Cantidad de días corridos entre el 2do y 3er Vencimiento. Si no usa tercer vencimiento, poner "00". Puede ser 1 digito o 2.</param>
        /// <param name="terMonto">Monto del Primer Vencimiento (En formato como está en la BD. Con o Sin valores decimales)</param>
        /// <param name="nroCtaCte">Nro o Id de Cuenta otorgado por Roela. 10 dígitos.</param>
        /// <returns></returns>
        public string armarBarraRoela(string idComprobante, string primVto, string primMonto, string cantDiasSegVto, string segMonto, string cantDiasTerVto, string terMonto, string nroCtaCte)
        {
            Utiles u = new Utiles();
            string s = "04471" + idComprobante.PadLeft(8, '0');
            
            if (u.esFechaValida(primVto, "yyyyMMdd")) primVto = primVto.Remove(0, 2);
            else return "";

            primMonto = u.getDoubleConDecimales(primMonto);
            primMonto = primMonto.Replace(".", "").Replace(",", "");
            primMonto = primMonto.PadLeft(7, '0');

            segMonto = u.getDoubleConDecimales(segMonto);
            segMonto = segMonto.Replace(".", "").Replace(",", "");
            segMonto = segMonto.PadLeft(7, '0');

            cantDiasSegVto = cantDiasSegVto.PadLeft(2, '0');

            terMonto = u.getDoubleConDecimales(terMonto);
            terMonto = terMonto.Replace(".", "").Replace(",", "");
            terMonto = terMonto.PadLeft(7, '0');

            cantDiasTerVto = cantDiasTerVto.PadLeft(2, '0');

            s += primVto + primMonto + cantDiasSegVto + segMonto + cantDiasTerVto + terMonto + nroCtaCte;
            s += getPrimerNroVerificador(s);
            s += getSegundoNroVerificador(s);

            return s;
        }

        private string getPrimerNroVerificador(string s)
        {
            string secuencia = "135793579357935793579357935793579357935793579357935793";
            int suma = 0;
            for (int i = 0; i < 54; i++)
                suma += Int32.Parse(s[i].ToString()) * Int32.Parse(secuencia[i].ToString());
            double div2 = suma / 2;
            int enteroDiv2 = (int)div2;
            int resto = enteroDiv2 % 10;
            return resto.ToString();
        }

        private string getSegundoNroVerificador(string s)
        {
            string secuencia = "1357935793579357935793579357935793579357935793579357935";
            int suma = 0;
            for (int i = 0; i < 55; i++)
                suma += Int32.Parse(s[i].ToString()) * Int32.Parse(secuencia[i].ToString());
            double div2 = suma / 2;
            int enteroDiv2 = (int)div2;
            int resto = enteroDiv2 % 10;
            return resto.ToString();
        }
    }
}