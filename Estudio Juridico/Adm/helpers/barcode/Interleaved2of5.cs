using helpers.barcode;
using System;
using System.Drawing;

namespace helpers.helpers.barcode
{
    /// <summary>
    ///  Interleaved 2 of 5 encoding
    ///  Written by: Brad Barnhill
    /// </summary>
    class Interleaved2of5 : BarcodeCommon, IBarcode
    {
        private string[] I25_Code = { "NNWWN", "WNNNW", "NWNNW", "WWNNN", "NNWNW", "WNWNN", "NWWNN", "NNNWW", "WNNWN", "NWNWN" };

        public Interleaved2of5(string input)
        {
            Raw_Data = input;
        }
        /// <summary>
        /// Encode the raw data using the Interleaved 2 of 5 algorithm.
        /// </summary>
        private string Encode_Interleaved2of5()
        {
            //check length of input
            if (Raw_Data.Length % 2 != 0)
                Error("EI25-1: Data length invalid.");

            //if (!BarcodeLib.Barcode.CheckNumericOnly(Raw_Data))
            //    Error("EI25-2: Numeric Data Only");

            string result = "1010";

            for (int i = 0; i < Raw_Data.Length; i += 2)
            {
                bool bars = true;
                string patternbars = I25_Code[Int32.Parse(Raw_Data[i].ToString())];
                string patternspaces = I25_Code[Int32.Parse(Raw_Data[i + 1].ToString())];
                string patternmixed = "";

                //interleave
                while (patternbars.Length > 0)
                {
                    patternmixed += patternbars[0].ToString() + patternspaces[0].ToString();
                    patternbars = patternbars.Substring(1);
                    patternspaces = patternspaces.Substring(1);
                }//while

                foreach (char c1 in patternmixed)
                {
                    if (bars)
                    {
                        if (c1 == 'N')
                            result += "1";
                        else
                            result += "11";
                    }//if
                    else
                    {
                        if (c1 == 'N')
                            result += "0";
                        else
                            result += "00";
                    }//else
                    bars = !bars;
                }//foreach

            }//foreach

            //add ending bars
            result += "1101";
            return result;
        }//Encode_Interleaved2of5

        #region IBarcode Members

        public string Encoded_Value
        {
            get { return this.Encode_Interleaved2of5(); }
        }

        /// <summary>
        /// Gets a bitmap representation of the encoded data.
        /// </summary>
        /// <returns>Bitmap of encoded value.</returns>
        public Bitmap Generate_Image(string Encoded_Value, int Width, int Height)
        {
            if (Encoded_Value == "") throw new Exception("EGENERATE_IMAGE-1: Must be encoded first.");
            Bitmap b = null;

            b = new Bitmap(Width, Height);
            int iBarWidth = Width / Encoded_Value.Length;
            int shiftAdjustment = 0;
            int iBarWidthModifier = 1;

            shiftAdjustment = 0;

            if (iBarWidth <= 0)
                throw new Exception("EGENERATE_IMAGE-2: Image size specified not large enough to draw image. (Bar size determined to be less than 1 pixel)");

            //draw image
            int pos = 0;

            using (Graphics g = Graphics.FromImage(b))
            {
                //clears the image and colors the entire background
                g.Clear(Color.White);

                //lines are fBarWidth wide so draw the appropriate color line vertically
                using (Pen backpen = new Pen(Color.FromArgb(243, 242, 240), iBarWidth / iBarWidthModifier))
                {
                    using (Pen pen = new Pen(Color.Black, iBarWidth / iBarWidthModifier))
                    {
                        while (pos < Encoded_Value.Length)
                        {
                            if (Encoded_Value[pos] == '1')
                                g.DrawLine(pen, new Point(pos * iBarWidth + shiftAdjustment + (int)(iBarWidth * 0.5), 0), new Point(pos * iBarWidth + shiftAdjustment + (int)(iBarWidth * 0.5), Height));

                            pos++;
                        }//while
                    }//using
                }//using
            }//using

            return b;
        }
        #endregion
    }
}
