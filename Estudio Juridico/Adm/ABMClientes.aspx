﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="ABMClientes.aspx.cs" Inherits="CSMMonitor.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <ul class="breadcrumb">
            <li>
                <h3>CLIENTES
                </h3>
            </li>
            <li>
                <p>
                    Utilice el presente formulario para consultar sus Clientes. También podrá dar de alta, de baja o modificar los datos de los mismos.
                </p>
            </li>
        </ul>
    </div>
    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>
    <asp:Panel class="row-fluid" runat="server" ID="pnlVerListado" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-user"></i>&nbsp;&nbsp;LISTADO DE CLIENTES </h2>
                    <div class="box-icon">
                        <a id="lnkToExcel" class="btn btn-success" style="width: 40px">Excel</a>
                    </div>
                </div>
                <div class="box-content">
                    <table id="tab1" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 7%">Acciones</th>
                                <th style="width: auto">Cliente</th>
                                <th style="width: auto">Dirección</th>
                                <th style="width: auto">Teléfono / Celular</th>
                                <th style="width: auto">Email</th>
                                <th style="width: auto">Documento</th>
                                <th style="width: auto">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal ID="litGrilla" runat="server"></asp:Literal>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnNuevo" runat="server" Text="Registrar Nuevo Cliente" OnClick="btnNuevo_Click" class="btn btn-info" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnSalir1" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlAgregar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;NUEVO CLIENTE</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblAdd" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span6">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Apellido:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddApellido" runat="server" class="input-large" MaxLength="80"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Nombre:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddNombre" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Tipo Documento:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAddTipoDoc" runat="server" class="input-large">
                                        <asp:ListItem Value="96" Selected="True">DNI</asp:ListItem>
                                        <asp:ListItem Value="80">CUIT</asp:ListItem>
                                        <asp:ListItem Value="86">CUIL</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Nro. Documento:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddNroDoc" runat="server" MaxLength="15" class="input-large" TextMode="Number"></asp:TextBox>
                                    (solo números, sin guiones)
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Fec. Nacimiento:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddFecNac" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>                            
                            <div class="control-group">
                                <label class="control-label">
                                    Razón Social:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddRazSoc" runat="server" MaxLength="75" class="input-large"></asp:TextBox>
                                    (opcional)
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Tipo IVA:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAddTipoIVA" runat="server" class="input-large">
                                        <asp:ListItem Value="M">Monotributista</asp:ListItem>
                                        <asp:ListItem Value="I">Resp. Inscripto</asp:ListItem>
                                        <asp:ListItem Value="N">Resp. No Inscripto</asp:ListItem>
                                        <asp:ListItem Value="E">Exento</asp:ListItem>
                                        <asp:ListItem Value="C" Selected="True">Consumidor Final</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Teléfono:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddTelefono" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Celular:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddCelular" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Email:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddEmail" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Dirección:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddDireccion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Altura:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddAltura" runat="server" MaxLength="6" class="input-small"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Piso:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddPiso" runat="server" MaxLength="3" class="input-small"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Depto:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddDepto" runat="server" MaxLength="2" class="input-small"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnAgregarGuardar" runat="server" Text="Registrar Cliente" class="btn btn-primary" OnClick="btnAgregarGuardar_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnAgregarSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlModificar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;MODIFICAR CLIENTE</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblMod" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span6">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Apellido:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModApellido" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Nombre:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModNombre" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Tipo Documento:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlModTipoDoc" runat="server" class="input-large">
                                        <asp:ListItem Value="96" Selected="True">DNI</asp:ListItem>
                                        <asp:ListItem Value="80">CUIT</asp:ListItem>
                                        <asp:ListItem Value="86">CUIL</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Nro. Documento:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModNroDoc" runat="server" MaxLength="15" class="input-large" TextMode="Number"></asp:TextBox>
                                    (solo números, sin guiones)
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Fec. Nacimiento:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModFecNac" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Razón Social:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModRazSoc" runat="server" MaxLength="75" class="input-large"></asp:TextBox>
                                    (opcional)
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Tipo IVA:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlModTipoIVA" runat="server" class="input-large">
                                        <asp:ListItem Value="M">Monotributista</asp:ListItem>
                                        <asp:ListItem Value="I">Resp. Inscripto</asp:ListItem>
                                        <asp:ListItem Value="N">Resp. No Inscripto</asp:ListItem>
                                        <asp:ListItem Value="E">Exento</asp:ListItem>
                                        <asp:ListItem Value="C" Selected="True">Consumidor Final</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Estado:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlModEstado" runat="server">
                                        <asp:ListItem Selected="True" Value="a">Alta</asp:ListItem>
                                        <asp:ListItem Value="b">Baja</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Teléfono:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModTelefono" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Celular:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModCel" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Email:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModEmail" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Dirección:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModDireccion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Altura:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModAltura" runat="server" MaxLength="6" class="input-small"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Piso:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModPiso" runat="server" MaxLength="3" class="input-small"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Depto:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModDepto" runat="server" MaxLength="2" class="input-small"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnModGuardar" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnModGuardar_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnModSalir" runat="server" Text="Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlEliminar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;ELIMINAR CLIENTE</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblElim" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <asp:Label ID="lblEliminar" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnElimOK" runat="server" Text="Eliminar Cliente" class="btn btn-primary" OnClick="btnElimOK_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnElimSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
