using System;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using System.Text;
using System.Drawing;
using System.Collections;

namespace Estudio_Juridico.Adm.clases
{
    public class Utiles
    {
        public MySqlConnection getConexion()
        {
            MySqlConnection con = (MySqlConnection)HttpContext.Current.Session["con"];
            return con;
        }

        public void abrirConexion(HttpResponse response = null)
        {
            MySqlConnection con = (MySqlConnection)HttpContext.Current.Session["con"];
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
            }
            catch (System.Exception ex)
            {

                HttpContext.Current.Session.Add("errMsg", "0. Ocurrió un error: no se pudo establecer una conexión con la Base de Datos. Por favor, intente dentro de unos minutos.<br/>" + ex.Message);
                //if (response != null) response.Redirect("PageFail.aspx");
                if (response != null)
                {
                    response.Write("Ocurrió un error: no se pudo establecer una conexión con la Base de Datos. Por favor, intente dentro de unos minutos.<br/>" + ex.Message);
                    response.Flush();
                    response.End();
                }
                return;
            }
        }

        public void cerrarConexion()
        {
            MySqlConnection con = (MySqlConnection)HttpContext.Current.Session["con"];
            if (con != null)
            {
                con.Close();
                con = null;
            }
        }

        /// <summary>
        /// Ejecuta un select. Devuelve un DataTable con el resultado. Devuelve null si no hay resultados en el select.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable ejecutarSelect(string sql)
        {
            MySqlConnection con = null;
            DataTable dt = new DataTable();
            MySqlDataReader dr = null;
            try
            {
                con = getConexion();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = sql;
                dr = cmd.ExecuteReader();
                dt.Load(dr);
                if (dt == null || dt.Rows.Count == 0)
                    dt = null;
            }
            catch (System.Exception ex)
            {
                if (dr != null)
                {
                    dr.Close();
                    dr = null;
                }
                string sss = ex.Message;
                ejecutarNonSelect("insert into sqlerrores (sqlerror,fecha,hora,idUsuario,errormsg) values ('" + sqlSeguro(sql) + "'," + getFechaActual() + "," + getHoraActual() + "," + HttpContext.Current.Session["idUser"] + ",'" + sqlSeguro(ex.Message) + "')");
                dt = null;
            }
            if (dr != null)
            {
                dr.Close();
                dr = null;
            }
            return dt;
        }

        public bool ejecutarNonSelect(string sql)
        {
            bool correcto = false;
            MySqlConnection con = null;
            try
            {
                con = getConexion();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                correcto = true;
            }
            catch (Exception ex)
            {
                string sss = ex.Message;
                correcto = false;
                ejecutarNonSelect("insert into sqlerrores (sqlerror,fecha,hora,idUsuario,errormsg) values ('" + sqlSeguro(sql) + "'," + getFechaActual() + "," + getHoraActual() + "," + HttpContext.Current.Session["idUser"] + ",'" + sqlSeguro(ex.Message) + "')");
            }
            return correcto;
        }

        /// <summary>
        /// Método para expresar en un Label los éxitos o errores usando codAvisos (plantillas)
        /// </summary>
        /// <param name="lbl">El control a modificar</param>
        /// <param name="text">El texto a incluir en el Label</param>        
        /// <param name="codAviso"> 0 = éxito ; 1 = error ; 2 = warning ; 3 = info ; 4 = normal</param>
        /// <param name="titulo">El título en negrita que aparecerá primero antes del 'text' a incluir en el Label</param>
        public void labelResultado(Label lbl, string text, int codAviso, string titulo = "")
        {
            lbl.Visible = true;
            string tipoAlert = "";

            if (codAviso == 0)
                tipoAlert = "success";
            else if (codAviso == 1)
                tipoAlert = "error";
            else if (codAviso == 2)
                tipoAlert = "info";
            else if (codAviso == 3)
                tipoAlert = "block";

            lbl.Text = "<div class='alert alert-" + tipoAlert + "'><button type='button' class='close' data-dismiss='alert'>×</button><h4 class='alert-heading'>" + titulo + "</h4><p>" + text + "</p></div>";

            if (codAviso == 4)
            {
                lbl.Text = text;
            }
        }

        /// <summary>
        /// Determina si es correcto el formato de la fecha
        /// </summary>
        /// <param name="fecha">Fecha</param>
        /// <param name="format">Formato a determinar. Por ej: dd/MM/yyyy ó yyyyMMdd ó MM/YYYY ó YYYYMM</param>
        /// <returns></returns>
        public bool esFechaValida(string fecha, string format = "dd/MM/yyyy")
        {
            if (format == "YYYYMMDD") format = "yyyyMMdd";

            if (format == "MM/YYYY")
            {
                if (fecha.Length == 7 && fecha.Substring(2, 1) == "/")
                {
                    string mes = fecha.Substring(0, 2);
                    string ano = fecha.Substring(3, 4);
                    if (!EsNumerico(mes, true) || !EsNumerico(ano, true)) return false;
                    if (Int32.Parse(mes) >= 1 && Int32.Parse(mes) <= 12 && Int32.Parse(ano) >= 1900 && Int32.Parse(mes) <= 2999) return true;
                    else return false;
                }
                else return false;
            }
            else if (format == "YYYYMM")
            {
                if (fecha.Length == 6)
                {
                    string mes = fecha.Substring(4, 2);
                    string ano = fecha.Substring(0, 4);
                    if (!EsNumerico(mes, true) || !EsNumerico(ano, true)) return false;
                    if (Int32.Parse(mes) >= 1 && Int32.Parse(mes) <= 12 && Int32.Parse(ano) >= 1900 && Int32.Parse(mes) <= 2999) return true;
                    else return false;
                }
                else return false;
            }
            else
            {
                DateTime date;
                return DateTime.TryParseExact(fecha, format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date);
            }
        }


        /// <summary>
        /// Convierte un tipo de dato BLOB recuperado de la BD a un string
        /// </summary>
        /// <param name="dr">el DataReader</param>
        /// <param name="nomCol">el nombre de la columna a convertir</param>
        /// <returns></returns>
        public string blobToString(ref MySqlDataReader dr, string nomCol)
        {
            if (dr[nomCol].ToString() != "")
            {
                byte[] byteBLOBData = (Byte[])(dr[nomCol]);
                String s = System.Text.Encoding.UTF8.GetString(byteBLOBData);
                return s;
            }
            else return "";
        }

        /// <summary>
        /// Eliminar del string s los acentos y otros caracteres raros 
        /// </summary>
        /// <param name="s">el string a conevrtir</param>
        /// <returns>Devuelve el string sin esos caracteres raros</returns>
        public string evitarCaracteresRaros(string s)
        {
            s = s.Replace("á", "a");
            s = s.Replace("é", "e");
            s = s.Replace("í", "i");
            s = s.Replace("ó", "o");
            s = s.Replace("ú", "u");
            s = s.Replace("Á", "A");
            s = s.Replace("É", "E");
            s = s.Replace("Í", "I");
            s = s.Replace("Ó", "O");
            s = s.Replace("Ú", "U");
            s = s.Replace("&aacute;", "a");
            s = s.Replace("&eacute;", "e");
            s = s.Replace("&iacute;", "i");
            s = s.Replace("&oacute;", "o");
            s = s.Replace("&uacute;", "u");
            s = s.Replace("&aacute;", "A");
            s = s.Replace("&eacute;", "E");
            s = s.Replace("&iacute;", "I");
            s = s.Replace("&oacute;", "O");
            s = s.Replace("&uacute;", "U");
            s = s.Replace("ñ", "n");
            s = s.Replace("Ñ", "N");
            s = s.Replace("&", "y");
            s = s.Replace("´", "");
            s = s.Replace("'", "");
            s = s.Replace("`", "");
            s = s.Replace("¬", "");
            //s = s.Replace("+", "");
            s = s.Replace("^", "");
            //s = s.Replace("{", "");
            //s = s.Replace("}", "");
            s = s.Replace("?", "");
            s = s.Replace("¿", "");
            s = s.Replace("¡", "");
            //s = s.Replace("/", "");
            //s = s.Replace("%", "");
            //s = s.Replace("$", "");
            //s = s.Replace("°", "");
            s = s.Replace("~", "");
            s = s.Replace("ë", "");
            s = s.Replace("ï", "");
            s = s.Replace("_", "");
            s = s.Replace("–", "");
            s = s.Replace("—", "");
            s = s.Replace("“", "");
            s = s.Replace("”", "");
            s = s.Replace("•", "");
            s = s.Replace("…", "");
            s = s.Replace("€", "");
            s = s.Replace("Ç", "");
            s = s.Replace("ç", "");
            return s;
        }

        /// <summary>
        /// Devuelve el texto listo para ser insertado en una tabla, pero con el formato correcto HTML
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string convertirEnStringHTML(string s)
        {
            //s = s.Replace("'", "''");
            s = s.Replace("\r\n", "<br/>");
            s = s.Replace("\n", "<br/>");

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (char c in s)
            {
                if (c > 127) // special chars
                    sb.Append(String.Format("&#{0};", (int)c));
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Método que se utiliza para convertir el texto para que sean interpretados de manera correcta los acentos por el Excel.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string convertirStringExcel(string s)
        {
            s = s.Replace("¡", "&#161;");
            s = s.Replace("°", "&#176;");
            s = s.Replace("¿", "&#191;");
            s = s.Replace("Á", "&#193;");
            s = s.Replace("É", "&#201;");
            s = s.Replace("Í", "&#205;");
            s = s.Replace("Ó", "&#211;");
            s = s.Replace("Ú", "&#218;");
            s = s.Replace("á", "&#225;");
            s = s.Replace("é", "&#233;");
            s = s.Replace("í", "&#237;");
            s = s.Replace("ó", "&#243;");
            s = s.Replace("ú", "&#250;");
            s = s.Replace("_", "&#95;");
            s = s.Replace("`", "&#95;");
            s = s.Replace("~", "&#126;");
            s = s.Replace("ë", "&#235;");
            s = s.Replace("ï", "&#239;");
            s = s.Replace("–", "&#8211;");
            s = s.Replace("—", "&#8212;");
            s = s.Replace("‘", "&#8216;");
            s = s.Replace("’", "&#8217;");
            s = s.Replace("“", "&#8220;");
            s = s.Replace("”", "&#8221;");
            s = s.Replace("•", "&#8226;");
            s = s.Replace("…", "&#8230;");
            s = s.Replace("€", "&#8364;");
            s = s.Replace("Ç", "&#199;");
            s = s.Replace("ç", "&#231;");
            s = s.Replace("´", "&#180;");
            s = s.Replace("ñ", "&#241;");
            s = s.Replace("Ñ", "&#209;");
            return s;
        }

        public Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }

        /// <summary>
        /// M&eacute;todo para hacer que no fallen los INSERT y prevenir SQL INJECTION.
        /// Se colocan doble ap&oacute;strofe si viene alguno.
        /// Utilizar este metodo en todos los insert para todos los campos.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string sqlSeguro(string s)
        {
            s = s.Replace(@"'", @"''");
            s = s.Replace(@"´", @"''");
            s = s.Replace(@"`", @"''");
            s = s.Replace(@"--", @"");
            s = s.Replace("<script", "");
            s = s.Replace("&#60;script", "");
            s = s.Replace("&lt;script", "");
           

            return s;
        }

        private string preposicionesConMinuscula(string s)
        {
            if (s.Length > 0)
            {
                //Preposiciones
                s = s.Replace(" A ", " a ");
                s = s.Replace(" Ante ", " ante ");
                s = s.Replace(" Bajo ", " bajo ");
                s = s.Replace(" Cabe ", " cabe ");
                s = s.Replace(" Con ", " con ");
                s = s.Replace(" De ", " de ");
                s = s.Replace(" Desde ", " desde ");
                s = s.Replace(" En ", " en ");
                s = s.Replace(" Entre ", " Entre ");
                s = s.Replace(" Hacia ", " hacia ");
                s = s.Replace(" Hasta ", " hasta ");
                s = s.Replace(" Para ", " para ");
                s = s.Replace(" Por ", " por ");
                s = s.Replace(" Seg&uacute;n ", " seg&uacute;n ");
                s = s.Replace(" Segun ", " segun ");
                s = s.Replace(" Según ", " según ");
                s = s.Replace(" Sin ", " sin ");
                s = s.Replace(" Sobre ", " sobre ");
                s = s.Replace(" Tras ", " tras ");

                //Otros
                s = s.Replace(" Un ", " un ");
                s = s.Replace(" Y ", " y ");
                s = s.Replace(" La ", " la ");
                s = s.Replace(" El ", " el ");
            }
            return s;
        }

        /// <summary>
        /// Capitaliza las palabras.
        /// </summary>
        /// <param name="nom">Palabra a capitalizar</param>
        /// <param name="soloLaPrimerPalabraConMayuscula">Unicamente capitaliza la primer palabra. Al resto la deja en minuscula.</param>
        /// <returns></returns>
        public string primeroConMayuscula(string nom, bool soloLaPrimerPalabraConMayuscula)
        {
            string aux = "";
            nom = nom.ToLower().Trim();
            if (soloLaPrimerPalabraConMayuscula && nom.Length > 0)
            {
                char x = char.ToUpper(nom[0]);
                string sResultado = x + nom.Remove(0, 1);
                return sResultado;
            }

            bool obviarSiguiente = false;
            aux = nom.Substring(0, 1).ToUpper();
            for (int i = 1; i < nom.Length; i++)
            {
                if (nom.Substring(i, 1) == " ")
                {
                    aux += " " + nom.Substring(i + 1, 1).ToUpper();
                    obviarSiguiente = true;
                }
                else
                {
                    if (!obviarSiguiente)
                    {
                        aux += nom.Substring(i, 1);
                    }
                    else
                    {
                        obviarSiguiente = false;
                    }
                }
            }
            aux = preposicionesConMinuscula(aux);
            return aux;
        }

        /// <summary>
        /// Devuelve la fecha con formato "YYYY-MM-DD". Se espera que se le mande "DD/MM/YYYY". Es ideal para guardar el valor de un TextBox en la BD Mysql
        /// </summary>
        /// <param name="s">string del TextBox con formato "DD/MM/YYYY"</param>
        /// <returns></returns>
        public string stringToFechaYYYYMMAA(string s)
        {
            string aux = "";
            if (s.Contains('/'))
            {
                string[] fecha = s.Split('/');
                string año = fecha[2];
                string mes = fecha[1];
                string dia = fecha[0];
                aux = año + "-" + mes + "-" + dia;
            }
            else if (s.Contains('-'))
            {
                string[] fecha = s.Split('-');
                string año = fecha[2];
                string mes = fecha[1];
                string dia = fecha[0];
                aux = año + "-" + mes + "-" + dia;
            }
            return aux;
        }

        /// <summary>
        /// Determina si el valor es dinero, validando que sea solo numerico y puede tener centavos
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public bool esDinero(string valor)
        {
            valor = valor.Replace(",", ".");
            string[] ss = valor.Split('.');
            foreach (String s in ss)
            {
                if (!EsNumerico(s))
                    return false;
            }
            return true;
        }

        public bool EsNumerico(object Expression, bool soloEnterosPositivos = false)
        {
            bool isNum;
            if (soloEnterosPositivos)
            {
                Int64 nro;
                isNum = Int64.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out nro);
                if (isNum && nro < 0)
                    isNum = false;
            }
            else
            {
                double retNum;
                isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            }
            return isNum;
        }

        public string getAcentosBien(string s)
        {
            s = s.Replace("&nbsp;", "");
            s = s.Replace("&#161;", "¡");
            s = s.Replace("&#176;", "°");
            s = s.Replace("&#191;", "¿");
            s = s.Replace("&#193;", "&aacute;");
            s = s.Replace("&#201;", "&eacute;");
            s = s.Replace("&#205;", "&iacute;");
            s = s.Replace("&#211;", "&oacute;");
            s = s.Replace("&#218;", "&uacute;");
            s = s.Replace("&#225;", "&aacute;");
            s = s.Replace("&#233;", "&eacute;");
            s = s.Replace("&#237;", "&iacute;");
            s = s.Replace("&#243;", "&oacute;");
            s = s.Replace("&#250;", "&uacute;");
            s = s.Replace("&#95;", "_");
            s = s.Replace("&#95;", "`");
            s = s.Replace("&#126;", "~");
            s = s.Replace("&#235;", "ë");
            s = s.Replace("&#239;", "ï");
            s = s.Replace("&#8211;", "–");
            s = s.Replace("&#8212;", "—");
            s = s.Replace("&#8216;", "‘");
            s = s.Replace("&#8217;", "’");
            s = s.Replace("&#8220;", "“");
            s = s.Replace("&#8221;", "”");
            s = s.Replace("&#8226;", "•");
            s = s.Replace("&#8230;", "…");
            s = s.Replace("&#8364;", "€");
            s = s.Replace("&#199;", "Ç");
            s = s.Replace("&#231;", "ç");
            s = s.Replace("&#180;", "´");
            s = s.Replace("&ntilde", "ñ");
            s = s.Replace("&Ntilde", "Ñ");
            return s;
        }

        public string setUTF8(string s)
        {
            s = s.Replace("¡", "&#161;");
            s = s.Replace("°", "&#176;");
            s = s.Replace("¿", "&#191;");
            s = s.Replace("&aacute;", "&#193;");
            s = s.Replace("&eacute;", "&#201;");
            s = s.Replace("&iacute;", "&#205;");
            s = s.Replace("&oacute;", "&#211;");
            s = s.Replace("&uacute;", "&#218;");
            s = s.Replace("&aacute;", "&#225;");
            s = s.Replace("&eacute;", "&#233;");
            s = s.Replace("&iacute;", "&#237;");
            s = s.Replace("&oacute;", "&#243;");
            s = s.Replace("&uacute;", "&#250;");
            s = s.Replace("_", "&#95;");
            s = s.Replace("`", "&#95;");
            s = s.Replace("~", "&#126;");
            s = s.Replace("ë", "&#235;");
            s = s.Replace("ï", "&#239;");
            s = s.Replace("–", "&#8211;");
            s = s.Replace("—", "&#8212;");
            s = s.Replace("‘", "&#8216;");
            s = s.Replace("’", "&#8217;");
            s = s.Replace("“", "&#8220;");
            s = s.Replace("”", "&#8221;");
            s = s.Replace("•", "&#8226;");
            s = s.Replace("…", "&#8230;");
            s = s.Replace("€", "&#8364;");
            s = s.Replace("Ç", "&#199;");
            s = s.Replace("ç", "&#231;");
            s = s.Replace("´", "&#180;");
            s = s.Replace("ñ", "&ntilde");
            s = s.Replace("Ñ", "&Ntilde");

            s = s.Replace("'", "\""); //reemplaza el ' para que no de error de sql

            return s;
        }

        /// <summary>
        /// Devuelve un string en formato DD/MM/AAAA sumandole la cantidad de meses indicado.
        /// </summary>
        /// <param name="fecha">Fecha en formato DD/MM/AAAA</param>
        /// <param name="cantMesesSumar">Puede ser positivo o negativo </param>
        /// <returns></returns>
        public string sumarMesesToFecha(string fecha, int cantMesesSumar)
        {
            if (!esFechaValida(fecha)) return "";
            DateTime date = new DateTime(Int32.Parse(fecha.Substring(6, 4)), Int32.Parse(fecha.Substring(3, 2)), Int32.Parse(fecha.Substring(0, 2)));
            date = date.AddMonths(cantMesesSumar);
            return getFechaToString(date);
        }

        /// <summary>
        /// Devuelve un string en formato DD/MM/AAAA sumandole la cantidad de dias indicado.
        /// </summary>
        /// <param name="fecha">Fecha en formato DD/MM/AAAA</param>
        /// <param name="cantDiasSumar">Puede ser positivo o negativo</param>
        /// <returns></returns>
        public string sumarDiasToFecha(string fecha, int cantDiasSumar)
        {
            if (!esFechaValida(fecha)) return "";
            DateTime date = new DateTime(Int32.Parse(fecha.Substring(6, 4)), Int32.Parse(fecha.Substring(3, 2)), Int32.Parse(fecha.Substring(0, 2)));
            date = date.AddDays(cantDiasSumar);
            return getFechaToString(date);
        }

        /// <summary>
        /// Devuelve la fecha actual. 0= devuelve 'yyyyMMdd' ; 1= devuelve 'dd/MM/yyyy'
        /// </summary>
        /// <param name="tipoFecha">0= yyyyMMdd ; 1= dd/MM/yyyy</param>
        /// <returns></returns>
        public string getFechaActual(int tipoFecha = 0)
        {
            if (tipoFecha == 0)
                return getAño() + getMes() + getDia();
            else return getDia() + "/" + getMes() + "/" + getAño();
        }

        /// <summary>
        /// Devuelve la Hora Actual. 0= devuelve 'HHMMSS' ; 1= devuelve 'HH:MM:SS'
        /// </summary>
        /// <param name="tipoHora">0= HHMMSS ; 1= HH:MM:SS</param>
        /// <returns></returns>
        public string getHoraActual(int tipoHora = 0)
        {
            if (tipoHora == 0) return getHora() + getMinuto() + getSegundo();
            else return getHora() + ":" + getMinuto() + ":" + getSegundo();
        }

        public string getAño()
        {
            return DateTime.Now.Year.ToString();
        }

        public string getMes()
        {
            string s = DateTime.Now.Month.ToString();
            if (Int32.Parse(s) > 9)
            {
                return s;
            }
            else
            {
                return "0" + s;
            }
        }

        public string getDia()
        {
            string s = DateTime.Now.Day.ToString();
            if (Int32.Parse(s) > 9)
            {
                return s;
            }
            else
            {
                return "0" + s;
            }
        }

        public string getHora()
        {
            int h = DateTime.Now.Hour;
            if (h > 9) return h.ToString();
            else return "0" + h.ToString();
        }

        public string getMinuto()
        {
            int m = DateTime.Now.Minute;
            if (m > 9) return m.ToString();
            else return "0" + m.ToString();
        }

        public string getSegundo()
        {
            int s = DateTime.Now.Second;
            if (s > 9) return s.ToString();
            else return "0" + s.ToString();
        }

        public string saveUploadedFileIntoDisk(FileUpload file, string nombreParaPonerle)
        {
            if (file.HasFile)
            {
                if (file.PostedFile.ContentLength > 2048000)
                {
                    return "Archivo demasiado pesado.";
                }
                try
                {
                    file.SaveAs(nombreParaPonerle);
                }
                catch
                {
                    return "Error desconocido al hacerle FileUpload.SaveAs()";
                }
            }
            return "";
        }

        /// <summary>
        /// Convierte fecha de 'dd/MM/yyyy' a 'yyyyMMdd'. Devuelve null si no es una fecha válida.
        /// </summary>
        /// <param name="fecha">fecha con formato dd/mm/yyyy</param>
        /// <returns>yyyyMMdd ; else return null</returns>
        public string getFechaToYYYYMMDD(string fecha)
        {
            if (esFechaValida(fecha))
            {
                string[] ff = fecha.Split('/');
                if (ff[0].Length < 2) ff[0] = "0" + ff[0];
                if (ff[1].Length < 2) ff[1] = "0" + ff[1];
                if (ff[2].Length < 4)
                {
                    if (ff[2].Length == 2) ff[2] = "20" + ff[2];
                    else ff[2] = "2000";
                }
                else if (ff[2].Length == 4)
                {
                    fecha = ff[2] + ff[1] + ff[0];
                }
                else fecha = "2000" + ff[1] + ff[0];
                return fecha;
            }
            else return "";
        }

        /// <summary>
        /// Método para jQuery DatePicker. Transforma la fecha a MM/DD/YYYY.
        /// </summary>
        /// <param name="fecha">Fecha Aa convertir</param>
        /// <param name="formato">formato "YYYYMMDD" o "DD/MM/YYYY"</param>
        /// <returns></returns>
        public string getFechaToMMDDYYYY(string fecha, string formato = "YYYYMMDD")
        {
            string s = "";
            if (formato == "YYYYMMDD") s = fecha.Substring(4, 2) + "/" + fecha.Substring(6, 2) + "/" + fecha.Substring(0, 4);
            else s = fecha.Substring(3, 2) + "/" + fecha.Substring(0, 2) + "/" + fecha.Substring(6, 4);
            return s;
        }

        /// <summary>
        /// Método para transformar la fecha de 'yyyymmdd' a 'dd/mm/YYYY'
        /// </summary>
        /// <param name="fecha">string de fecha sacada de la BD</param>
        /// <param name="mesConLetras">True: establecemos si el resultado de los meses los queremos con Letras (por ej: ENE, FEB, MAR, Abr, etc). False: con números (01, 02, 03, etc)</param>
        /// <returns>DD/MMM/YYYY</returns>
        public string getFechaToString(string fecha, bool mesConLetras = false)
        {
            string f = "", aux = "";
            try
            {
                if (fecha.Length == 8)
                {
                    if (mesConLetras)
                    {
                        switch (fecha.Substring(4, 2))
                        {
                            case "01":
                                aux = "ENE";
                                break;
                            case "02":
                                aux = "FEB";
                                break;
                            case "03":
                                aux = "MAR";
                                break;
                            case "04":
                                aux = "ABR";
                                break;
                            case "05":
                                aux = "MAY";
                                break;
                            case "06":
                                aux = "JUN";
                                break;
                            case "07":
                                aux = "JUL";
                                break;
                            case "08":
                                aux = "AGO";
                                break;
                            case "09":
                                aux = "SEP";
                                break;
                            case "10":
                                aux = "OCT";
                                break;
                            case "11":
                                aux = "NOV";
                                break;
                            case "12":
                                aux = "DIC";
                                break;
                            default:
                                aux = "ZZZ";
                                break;
                        }
                    }
                    else aux = fecha.Substring(4, 2);
                    f = fecha.Substring(6, 2) + "/" + aux + "/" + fecha.Substring(0, 4);
                }
            }
            catch
            {
                f = "";
            }
            return f;
        }

        /// <summary>
        /// Método para transformar la fecha de DateTime a 'dd/mm/YYYY'
        /// </summary>
        /// <param name="fechaConvertir">Fecha como DateTime</param>
        /// <param name="mesConLetras">True: establecemos si el resultado de los meses los queremos con Letras (por ej: ENE, FEB, MAR, Abr, etc). False: con números (01, 02, 03, etc)</param>
        /// <returns>DD/MMM/YYYY</returns>
        public string getFechaToString(DateTime fechaConvertir, bool mesConLetras = false)
        {
            string fecha = fechaConvertir.Year.ToString();
            if (fechaConvertir.Month.ToString().Length == 1) fecha += "0" + fechaConvertir.Month.ToString();
            else fecha += fechaConvertir.Month.ToString();
            if (fechaConvertir.Day.ToString().Length == 1) fecha += "0" + fechaConvertir.Day.ToString();
            else fecha += fechaConvertir.Day.ToString();

            string f = "", aux = "";
            try
            {
                if (fecha != "")
                {
                    if (mesConLetras)
                    {
                        switch (fecha.Substring(4, 2))
                        {
                            case "01":
                                aux = "ENE";
                                break;
                            case "02":
                                aux = "FEB";
                                break;
                            case "03":
                                aux = "MAR";
                                break;
                            case "04":
                                aux = "ABR";
                                break;
                            case "05":
                                aux = "MAY";
                                break;
                            case "06":
                                aux = "JUN";
                                break;
                            case "07":
                                aux = "JUL";
                                break;
                            case "08":
                                aux = "AGO";
                                break;
                            case "09":
                                aux = "SEP";
                                break;
                            case "10":
                                aux = "OCT";
                                break;
                            case "11":
                                aux = "NOV";
                                break;
                            case "12":
                                aux = "DIC";
                                break;
                            default:
                                aux = "ZZZ";
                                break;
                        }
                    }
                    else aux = fecha.Substring(4, 2);
                    f = fecha.Substring(6, 2) + "/" + aux + "/" + fecha.Substring(0, 4);
                }
            }
            catch
            {
                f = "";
            }
            return f;
        }

        /// <summary>
        /// Método para transformar la hora de 'hhmmss' a 'hh:mm:ss'.
        /// </summary>
        /// <param name="hora">hora a convertir en hh:mm:ss</param>
        /// <returns></returns>
        public string getHoraToString(string hora)
        {
            string aux = "";
            if (hora.Length == 4)
            {
                aux = hora + "0";
            }
            if (hora.Length == 5)
            {
                aux = hora.Substring(0, 1) + ":" + hora.Substring(1, 2) + ":" + hora.Substring(3, 2);
            }
            else if (hora.Length == 6)
            {
                aux = hora.Substring(0, 2) + ":" + hora.Substring(2, 2) + ":" + hora.Substring(4, 2);
            }
            return aux;
        }

        public string explodeArrayToString(Array a)
        {
            string s = "";
            foreach (System.Web.UI.WebControls.ListItem item in a)
            {
                s += ", " + item.Text;
            }
            if (s.Length > 0) s = s.Remove(0, 2);
            return s;
        }

        public string parseUsingPDFBox(string filename, string idArchivo)
        {
            /*org.pdfbox.pdmodel.PDDocument doc = org.pdfbox.pdmodel.PDDocument.load(filename);
            org.pdfbox.util.PDFTextStripper stripper = new org.pdfbox.util.PDFTextStripper();
            //return stripper.getText(doc);

            //SACAMOS ACENTOS... CARACTERES RAROS LOS REEMPLAZAMOS POR ESPACIOS
            string s = "";
            try
            {
                s = stripper.getText(doc);
            }
            catch (System.Exception ex)
            {
                string sss = ex.Message;
            }
            s = s.Replace("\r\n", " ");
            s = borrarCaracteresEspeciales(s.ToUpper());
            string[] palabras = s.Split(' ');
            foreach (string palabra in palabras)
            {
                if (palabra.Trim() == "") continue;
                Palabras pal = new Palabras();
                pal.IdArchivo = idArchivo;
                pal.Palabra = palabra;
                pal.saveIntoDB();
            }        */
            return "";
        }

        public string parseWordDoc(string filename, string idArchivo)
        {
            /*Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            object file = filename;
            object nullobj = System.Reflection.Missing.Value;
            Microsoft.Office.Interop.Word.Document doc = wordApp.Documents.Open(
            ref file, ref nullobj, ref nullobj,
            ref nullobj, ref nullobj, ref nullobj,
            ref nullobj, ref nullobj, ref nullobj,
            ref nullobj, ref nullobj, ref nullobj);
            string s = "";
            
            try
            {
                s = doc.Content.Text;
            }
            catch (System.Exception ex)
            {
                string sss = ex.Message;
            }
            s = s.Replace("\r\n", " ");
            s = borrarCaracteresEspeciales(s.ToUpper());
            string[] palabras = s.Split(' ');
            foreach (string palabra in palabras)
            {
                if (palabra.Trim() == "") continue;
                Palabras pal = new Palabras();
                pal.IdArchivo = idArchivo;
                pal.Palabra = palabra;
                pal.saveIntoDB();
            }  
            ;
            wordApp.Quit(ref nullobj, ref nullobj, ref nullobj);
            return s;*/

            return "";
        }

        /// <summary>
        /// Borra todo lo que no sea números y letras (mayusc o minusc)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string borrarCaracteresEspeciales(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
                else sb.Append(' ');
            }
            return sb.ToString();
        }



        public string getIcono(string extension)
        {
            extension = extension.ToLower();
            if (extension == "pdf") return "<img src=\"../images/icono_pdf.png\" alt=\"\" />";//
            else if (extension == "doc") return "<img src=\"../images/icono_doc.png\" alt=\"\" />";//
            else if (extension == "tif") return "<img src=\"../images/icono_tif.png\" alt=\"\" />";//
            else if (extension == "jpg") return "<img src=\"../images/icono_jpg.png\" alt=\"\" />";//
            else if (extension == "png") return "<img src=\"../images/icono_png.png\" alt=\"\" />";//
            else return "<img src=\"../images/icono_file.png\" alt=\"\" />";//
        }

        /// <summary>
        /// Transforma la fecha de MM/YYYY a YYYYMM
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public string getFechaToYYYYMM(string fecha)
        {
            if (fecha.Length != 7) return fecha;
            else return fecha.Substring(3, 4) + fecha.Substring(0, 2);
        }

        /// <summary>
        /// Transforma de 'YYYYMM' a 'MMM-YYYY'
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public string getFechaFrom6digits(string fecha)
        {
            if (!EsNumerico(fecha) || fecha.Length != 6)
            {
                return "";
            }
            else
            {
                string s = "";
                switch (fecha.Substring(4, 2))
                {
                    case "01":
                        {
                            s = "Ene-";
                            break;
                        }
                    case "02":
                        {
                            s = "Feb-";
                            break;
                        }
                    case "03":
                        {
                            s = "Mar-";
                            break;
                        }
                    case "04":
                        {
                            s = "Abr-";
                            break;
                        }
                    case "05":
                        {
                            s = "May-";
                            break;
                        }
                    case "06":
                        {
                            s = "Jun-";
                            break;
                        }
                    case "07":
                        {
                            s = "Jul-";
                            break;
                        }
                    case "08":
                        {
                            s = "Ago-";
                            break;
                        }
                    case "09":
                        {
                            s = "Sep-";
                            break;
                        }
                    case "10":
                        {
                            s = "Oct-";
                            break;
                        }
                    case "11":
                        {
                            s = "Nov-";
                            break;
                        }
                    case "12":
                        {
                            s = "Dic-";
                            break;
                        }
                    default:
                        {
                            s = "";
                            break;
                        }
                }
                s += fecha.Substring(0, 4);
                return s;
            }
        }

        public void CreateThumbnail(string originalFileName, string thumbNailName, int lnWidth, int lnHeight)
        {
            System.Drawing.Bitmap bmpOut = null;
            try
            {
                Bitmap loBMP = new Bitmap(originalFileName);
                System.Drawing.Imaging.ImageFormat loFormat = loBMP.RawFormat;

                decimal lnRatio;
                int lnNewWidth = 0;
                int lnNewHeight = 0;

                if (loBMP.Width < lnWidth && loBMP.Height < lnHeight)
                    bmpOut.Save(thumbNailName, System.Drawing.Imaging.ImageFormat.Jpeg); //GRABA EL THUMBNAIL EN EL DISCO

                if (loBMP.Width > loBMP.Height)
                {
                    lnRatio = (decimal)lnWidth / loBMP.Width;
                    lnNewWidth = lnWidth;
                    decimal lnTemp = loBMP.Height * lnRatio;
                    lnNewHeight = (int)lnTemp;
                }
                else
                {
                    lnRatio = (decimal)lnHeight / loBMP.Height;
                    lnNewHeight = lnHeight;
                    decimal lnTemp = loBMP.Width * lnRatio;
                    lnNewWidth = (int)lnTemp;
                }
                bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
                g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);
                loBMP.Dispose();
                bmpOut.Save(thumbNailName, System.Drawing.Imaging.ImageFormat.Jpeg); //GRABA EL THUMBNAIL EN EL DISCO
            }
            catch (System.Exception ex)
            {
                string sss = ex.Message;
            }
        }


        /*/// <summary>
        /// Enviar EMAIL usando System.Net.Mail
        /// </summary>
        /// <param name="pPara"></param>
        /// <param name="pAsunto"></param>
        /// <param name="pBody"></param>
        /// <param name="pCc"></param>
        /// <param name="pCco"></param>
        ///*/


        /// <summary>
        /// Enviar EMAIL desde el servidor de BAEHOST usando System.Net.Mail
        /// </summary>
        /// <param name="pFrom">Email From. Ej: "news@admporceldeperalta.com.ar"</param>
        /// <param name="pFromDisplayName">Nombre a mostrar en el campo From. Ej: "Expensas y Deudas"</param>
        /// <param name="pPass">Password</param>
        /// <param name="pHostName">Hostname de Baehost. Ej: "mail.admporceldeperalta.com.ar"</param>
        /// <param name="pPara"></param>
        /// <param name="pAsunto"></param>
        /// <param name="pBody"></param>
        /// <param name="pAttachs">ArrayList de System.Net.Mail.Attachment</param>
        /// <param name="pCc"></param>
        /// <param name="pCco"></param>
        /// <returns>Devuelve un string VACIO en caso de éxito. Devuelve un string con el mensaje de error en caso de error.</returns>
        public string sendEmail(string pFrom, string pFromDisplayName, string pPass, string pHostName, string pPara, string pAsunto, string pBody, ArrayList pAttachs = null)
        {
            string from = pFrom;
            string pass = pPass;
            string tto = pPara;
            string subject = pAsunto;
            string body = pBody;
            string host = pHostName;
            string Username = from;
            string retValue = "";
            bool hayWarnings = false;

            string[] ttos = tto.Split(';');
            foreach (string para in ttos)
            {
                string para2 = para.Trim();
                if (para2 == "") continue;
                if (!esEmailValido(para2))
                {
                    retValue += "<br/>Formato de email incorrecto: \"" + para2 + "\".";
                    hayWarnings = true;
                    continue;
                }

                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
                System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(Username, pass);
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                System.Net.Mail.MailAddress fromAddress = new System.Net.Mail.MailAddress(from, pFromDisplayName);

                smtpClient.Host = host;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredential;

                message.From = fromAddress;
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                message.To.Add(para2);

                if (pAttachs != null)
                    foreach (System.Net.Mail.Attachment adjunto in pAttachs)
                        message.Attachments.Add(adjunto);
                try
                {
                    smtpClient.Send(message);
                    retValue += "<br/>Envío correcto a: \"" + para2 + "\".";
                }
                catch (Exception ex)
                {
                    //Si hay error lo muestra
                    string sss = ex.Message;
                    retValue += "<br/>" + sss + ".";
                    hayWarnings = true;
                }
            }

            if (hayWarnings)
                return retValue; //Curso alt: Devolución si hay emails no enviados. Incluye los emails correctos e incorrectos.
            else
                return "OK"; //curso normal.
        }



        public void guardarLogin(System.Web.HttpRequest req, string observaciones)
        {
            //Guardar login en la BD
            Utiles u = new Utiles();
            string ip = req.UserHostAddress, pag = req.Url.AbsoluteUri.ToLower(), so = req.Browser.Platform, hostName = req.LogonUserIdentity.Name, brow = req.Browser.Browser, browVer = req.Browser.Version;
            string sql = "insert into login (fecha,hora,IP,pagina,observ,so,brow,browver) values (" + u.getFechaActual(0) + ", " + u.getHoraActual(0) + ", '" + ip + "','" + pag + "','" + observaciones + "','" + so + "','" + brow + "','" + browVer + "')";
            u.ejecutarNonSelect(sql);
        }

        /// <summary>
        /// Obtiene la cantidad de IPs diferentes que han ingresado entre determinado periodo
        /// </summary>
        /// <param name="ipsDiferentes">true: Cuenta unicamente las IPs diferentes. ; False: cuenta el total de clicks/accesos</param>
        /// <param name="desde"></param>
        /// <param name="hasta"></param>
        /// <returns></returns>
        public string obtenerCantidadDeVisitas(bool ipsDiferentes, string desde = "", string hasta = "")
        {
            DataTable dt = null;
            string select = "";
            if (ipsDiferentes) select = "SELECT count(distinct ip) from login ";
            else select = "SELECT count(*) from login ";

            if (desde == "" && hasta == "")
            {
                dt = ejecutarSelect(select);
            }
            else if (desde != "" && hasta != "")
            {
                dt = ejecutarSelect(select + " where fecha between " + desde + " and " + hasta);
            }
            else if (desde != "")
            {
                dt = ejecutarSelect(select + " where fecha>=" + desde);
            }
            else dt = ejecutarSelect(select + " where fecha <=" + hasta);

            if (dt != null && dt.Rows.Count > 0)
                return dt.Rows[0][0].ToString();
            else return "0";
        }

        public bool esEmailValido(string pEmail)
        {
            pEmail = pEmail.Trim();
            if (pEmail == "") return false;
            string[] emails = pEmail.Split(';');
            bool ok = true;
            bool yaHayUnoValido = false;
            string strPattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            foreach (string email in emails)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(email, strPattern))
                {
                    yaHayUnoValido = true;
                    continue;
                }
                else if (email == "" && yaHayUnoValido) continue; //por ej si ingresan un punto y coma al final, que sea válido: "leoivix@hotmail.com;"
                else ok = false;
            }
            return ok;
        }

        /// <summary>
        /// Devuelve un DATATABLE con todas las fechas de envios distintas (DISTINCT de la tabla ENVIOS) 
        /// </summary>
        /// <param name="soloNoticia"></param>
        /// <returns></returns>
        /*public DataTable obtenerFechasEnviosDistintas()
        {
            return ejecutarSelect("SELECT distinct DATE_FORMAT(fecha, \"%d/%m/%Y\") fecha FROM envios");
        }*/

        /// <summary>
        /// Acorta un string y en caso de ser superior a "max" lo corta.
        /// </summary>
        /// <param name="s">el string a controlar</param>
        /// <param name="max">cantidad máxima de caracteres antes de cortarlo y agregarle los puntos suspensivos.</param>
        /// <param name="agregarPuntosSuspensivos">Le agrega "..." al final si supera max.</param>
        /// <returns></returns>
        public string acortarString(string s, int max, bool agregarPuntosSuspensivos = false)
        {
            if (s == null) return "";
            if (s.Length > max)
            {
                if (!agregarPuntosSuspensivos)
                    return s.Substring(0, max);
                else if (s.Length > 3)
                    return s.Substring(0, max - 3) + "...";
                else
                    return s.Substring(0, max);
            }
            else return s;
        }

        /// <summary>
        /// Método que valida si el string contiene solamente Letras (en mayusculas o minusculas) o Números
        /// </summary>
        /// <param name="s">string a verificar</param>
        /// <returns></returns>
        public bool esLetraOrNumero(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"^[a-zA-Z0-9]+$"))
                return true;
            else return false;
        }

        /// <summary>
        /// Método que valida si el string contiene solamente Letras (en mayusculas o minusculas)
        /// </summary>
        /// <param name="s">string a verificar</param>
        /// <returns></returns>
        public bool esLetra(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"^[a-zA-Z]+$"))
                return true;
            else return false;
        }

        /// <summary>
        /// Método que valida si el string contiene solamente Letras (en mayusculas o minusculas), números, y también un caracter especial pasado por parametro, por ej: punto ('.'), coma (',') , guión bajo ('_'), etc
        /// </summary>
        /// <param name="s">string a verificar</param>
        /// <param name="caracterEspecialSoportado">caracter especial que tambien es soportado por el validador. Por ej: punto ('.'), coma (',') , guión bajo ('_'), etc. Pasar solamente el caracter, sin las comillas</param>
        /// <returns></returns>
        public bool esLetraOrNumeroOrCharEspecial(string s, char caracterEspecialSoportado)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"^[a-zA-Z0-9" + caracterEspecialSoportado + "]+$"))
                return true;
            else return false;
        }



        /// <summary>
        /// Determina si el dispositivo con el que ingresaron al sistema es mobil o una PC
        /// </summary>
        /// <param name="request"></param>
        /// <returns>True: si es movil ; false: si es una PC</returns>
        public static bool esDispositivoMovil(HttpRequest request)
        {
            /*HttpBrowserCapabilities browser = request.Browser;
            if (browser.IsMobileDevice) return true;
            else return false;*/

            string userAgent = request.UserAgent.ToLower();
            if (userAgent.Contains("blackberry") || userAgent.Contains("iphone") || userAgent.Contains("android")
                || userAgent.Contains("sony") || userAgent.Contains("nokia") || userAgent.Contains("motorola") || userAgent.Contains("samsung"))
                return true;
            else return false;
        }

        /// <summary>
        /// Devuelve true si es Internet Explorer 9 o menor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool esIE9oMenor(HttpRequest request)
        {
            if (request.Browser.Browser == "IE" && request.Browser.MajorVersion < 10)
                return true;
            else return false;
        }

        /// <summary>
        /// En el caso que el requerimiento ha sido generado desde el sistema de reclamo del Sistema de Admin. de CSM, utilizar este método para que muestre correctamente quién ha generado el req.
        /// </summary>
        /// <param name="tipoPropietario">P I o D</param>
        /// <param name="codUsuario">Cod de 8 digitos</param>
        /// <param name="email">email del generador</param>
        /// <returns></returns>
        public string getDatosGeneradorReclamo(string tipoPropietario, string codUsuario, string email, bool incluirEmail)
        {
            string s = "";
            if (tipoPropietario == "P")
                s = "Propietario";
            else if (tipoPropietario == "I")
                s = "Inquilino";
            else if (tipoPropietario == "D")
                s = "Administrador";

            s += " de " + codUsuario + (incluirEmail ? (email != "" ? " (" + email + ")" : "") : "");
            return s;
        }

        public string quitarAcentos(string s)
        {
            s = s.Replace("á", "a");
            s = s.Replace("é", "e");
            s = s.Replace("í", "i");
            s = s.Replace("ó", "o");
            s = s.Replace("ú", "u");
            s = s.Replace("Á", "A");
            s = s.Replace("É", "E");
            s = s.Replace("Í", "I");
            s = s.Replace("Ó", "O");
            s = s.Replace("Ú", "U");
            return s;
        }

        /// <summary>
        /// Convierte una fecha a DateTime.
        /// </summary>
        /// <param name="fecha">Fecha a Convertir</param>
        /// <param name="formatoEntrada">Formato de la Fecha a Convertir. Puede ser 'dd/mm/yyyy' o 'yyyymmdd' </param>
        /// <returns></returns>
        public DateTime convertirADateTime(string fecha, string formatoEntrada = "dd/mm/yyyy")
        {
            if (formatoEntrada == "dd/mm/yyyy")
            {
                DateTime date = new DateTime(Int32.Parse(fecha.Substring(6, 4)), Int32.Parse(fecha.Substring(3, 2)), Int32.Parse(fecha.Substring(0, 2)));
                return date;
            }
            else
            {
                DateTime date = new DateTime(Int32.Parse(fecha.Substring(0, 4)), Int32.Parse(fecha.Substring(4, 2)), Int32.Parse(fecha.Substring(6, 2)));
                return date;
            }
        }

        /// <summary>
        /// Convierte un DateTime a YYYYMMDD
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public string getFechaToYYYYMMDD(DateTime fecha)
        {
            string mes = fecha.Month.ToString();
            string dia = fecha.Day.ToString();
            if (mes.Length == 1) mes = "0" + mes;
            if (dia.Length == 1) dia = "0" + dia;
            return fecha.Year + mes + dia;
        }

        public string getMesToLetras(string mes)
        {
            if (mes.Length == 1) mes = "0" + mes;
            switch (mes)
            {
                case "01":
                    mes = "ENE";
                    break;
                case "02":
                    mes = "FEB";
                    break;
                case "03":
                    mes = "MAR";
                    break;
                case "04":
                    mes = "ABR";
                    break;
                case "05":
                    mes = "MAY";
                    break;
                case "06":
                    mes = "JUN";
                    break;
                case "07":
                    mes = "JUL";
                    break;
                case "08":
                    mes = "AGO";
                    break;
                case "09":
                    mes = "SEP";
                    break;
                case "10":
                    mes = "OCT";
                    break;
                case "11":
                    mes = "NOV";
                    break;
                case "12":
                    mes = "DIC";
                    break;
                default:
                    mes = "ZZZ";
                    break;
            }
            return mes;
        }

        /// <summary>
        /// Devuelve un entero con la cantidad de Meses que hay de diferencia entre 2 Fechas
        /// </summary>
        /// <param name="fechaMenor">Fecha Menor</param>
        /// <param name="fechaMayor">Fecha Mayor</param>
        /// <returns></returns>
        public int getDiferenciaEntreMeses(DateTime fechaMenor, DateTime fechaMayor)
        {
            return Math.Abs((fechaMayor.Month - fechaMenor.Month) + 12 * (fechaMayor.Year - fechaMenor.Year));
        }

        /// <summary>
        /// Devuelve un entero con la cantidad de Dias que hay de diferencia entre 2 Fechas
        /// </summary>
        /// <param name="fechaMenor">Fecha Menor</param>
        /// <param name="fechaMayor">Fecha Mayor</param>
        /// <returns></returns>
        public int getDiferenciaEntreDias(DateTime fechaMenor, DateTime fechaMayor)
        {
            TimeSpan ts = fechaMayor - fechaMenor;
            return ts.Days;
        }

        /// <summary>
        /// Devuelve el Mes (en número de 2 dígitos) que corresponde la letra del mes entregada. Por ej: la de la Periodicidad
        /// </summary>
        /// <param name="letraMes">Letra correspondiente a EFMAYJLGSOND</param>
        /// <returns></returns>
        public string getMesQueCorresponde(char letraMes)
        {
            string mes = "";
            switch (letraMes)
            {
                case 'E':
                    {
                        mes = "01";
                        break;
                    }
                case 'F':
                    {
                        mes = "02";
                        break;
                    }
                case 'M':
                    {
                        mes = "03";
                        break;
                    }
                case 'A':
                    {
                        mes = "04";
                        break;
                    }
                case 'Y':
                    {
                        mes = "05";
                        break;
                    }
                case 'J':
                    {
                        mes = "06";
                        break;
                    }
                case 'L':
                    {
                        mes = "07";
                        break;
                    }
                case 'G':
                    {
                        mes = "08";
                        break;
                    }
                case 'S':
                    {
                        mes = "09";
                        break;
                    }
                case 'O':
                    {
                        mes = "10";
                        break;
                    }
                case 'N':
                    {
                        mes = "11";
                        break;
                    }
                case 'D':
                    {
                        mes = "12";
                        break;
                    }
                default:
                    {
                        mes = "01";
                        break;
                    }
            }
            return mes;
        }

        /// <summary>
        /// Devuelve el Mes (en char de un caracter EFMAYJLGSOND) que corresponde al mes en numero entregado. 
        /// </summary>
        /// <param name="letraMes">Mes en Número de 2 digitos</param>
        /// <returns>Devuelve algun mes como EFMAYJLGSOND</returns>
        public string getMesQueCorresponde(string numeroMes)
        {
            string mes = "";
            switch (numeroMes)
            {
                case "01":
                    {
                        mes = "E";
                        break;
                    }
                case "02":
                    {
                        mes = "F";
                        break;
                    }
                case "03":
                    {
                        mes = "M";
                        break;
                    }
                case "04":
                    {
                        mes = "A";
                        break;
                    }
                case "05":
                    {
                        mes = "Y";
                        break;
                    }
                case "06":
                    {
                        mes = "J";
                        break;
                    }
                case "07":
                    {
                        mes = "L";
                        break;
                    }
                case "08":
                    {
                        mes = "G";
                        break;
                    }
                case "09":
                    {
                        mes = "S";
                        break;
                    }
                case "10":
                    {
                        mes = "O";
                        break;
                    }
                case "11":
                    {
                        mes = "N";
                        break;
                    }
                case "12":
                    {
                        mes = "D";
                        break;
                    }
                default:
                    {
                        mes = "E";
                        break;
                    }
            }
            return mes;
        }

        /// <summary>
        /// Método para agregarle un Modal a un Boton. Incluir este método en el Page_Load, fuera del !Page.IsPostBack. NO HACER NADA EN EL BUTTON, HACE TODO AUTOMATICAMENTE. 
        /// </summary>
        /// <param name="boton">El Button a agregarle el Modal</param>
        /// <param name="textoEncabezado">Texto del Encabezado. Por ejemplo: "Confirmar"</param>
        /// <param name="textoCuerpo">Texto del cuerpo del Modal. Por ejemplo: "¿Está seguro que desea guardar?"</param>
        /// <param name="pagina">La página. Poner simplemente "Page"</param>
        /// <param name="estaDentroDeUpdatePanel">Está dentro de un Update Panel o no. True: SI ; False: NO</param>
        /// <param name="usaPaginaMaestro">True: usa página maestro ; False: no usa.</param>
        /// <param name="textoBotonAceptar">Texto del botón de accción Aceptar</param>
        /// <param name="textoBotonCancelar">Texto del botón de acción Cancelar</param>        
        public void agregarModal(ref Button boton, string textoEncabezado, string textoCuerpo, System.Web.UI.Page pagina, bool usaPaginaMaestro = true, string textoBotonAceptar = "Aceptar", string textoBotonCancelar = "Cancelar")
        {
            UpdatePanel uupp = new UpdatePanel();
            bool estaDentroDeUpdatePanel = tieneComoPadre(boton, uupp.GetType());
            string maestro = "ctl00$ContentPlaceHolder1$";
            if (!usaPaginaMaestro) maestro = "";
            System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            newDiv.ID = "div_" + boton.ID;
            boton.Attributes.Add("data-toggle", "modal");
            boton.Attributes.Add("data-target", "#modal_" + boton.ID);
            newDiv.InnerHtml = "<div class='modal fade' id='modal_" + boton.ID + "' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>" +
                   "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>" +
                   "<h4 class='modal-title' id='myModalLabel'>" + textoEncabezado + "</h4></div><div class='modal-body'>" + textoCuerpo + "</div><div class='modal-footer'>" +
                   "<button type='button' name='modalBtn_" + boton.ID + "' class='btn btn-primary' " + (estaDentroDeUpdatePanel ? "data-dismiss='modal'" : "") + " onclick=\"javascript:__doPostBack('" + maestro + boton.ID + "','')\">" + textoBotonAceptar + "</button>" +
                   "<button type='button' class='btn btn-default' data-dismiss='modal'>" + textoBotonCancelar + "</button></div></div></div></div>";
            pagina.Controls.Add(newDiv);
        }

        /// <summary>
        /// Determina si un Control tiene como padre, abuelo, bisabuelo, etc a otro Control según su tipo de dato. 
        /// </summary>
        /// <param name="childControl">Control a estudiar.</param>
        /// <param name="parentType">El Tipo de Dato a Determinar. Por ej: Button1.GetType().</param>
        /// <returns></returns>
        public bool tieneComoPadre(Control childControl, Type parentType)
        {
            Control parent = childControl.Parent;
            while (parent != null && parent.GetType() != parentType)
            {
                parent = parent.Parent;
            }
            if (parent != null && parent.GetType() == parentType)
                return true;
            else return false;

        }

        public string getPagadoPor(string pagadoPorUnaLetra)
        {
            string s = pagadoPorUnaLetra.ToUpper();
            if (s == "P")
                s = "Propietario";
            else if (s == "I")
                s = "Inquilino";
            else if (s == "A")
                s = "Administración";
            return s;
        }

        /// <summary>
        /// Mostrar Mensaje Emergente con jNotifiy. Funciona dentro y fuera de un UpdatePanel.
        /// </summary>
        /// <param name="ponerThis">Acá poner simplemente "this"</param>
        /// <param name="mensaje">El string conteniendo el Mensaje a mostrar</param>
        /// <param name="tipoNotificacion">0: Exito ; 1: Error; 2: Warning</param>
        /// <param name="miliSegundosAntesQueSeEsconda">Cantidad de milisegundos hasta que se esconda el message</param>
        public void showMessage(Control ponerThis, string mensaje, int tipoNotificacion, int miliSegundosAntesQueSeEsconda = 3000)
        {
            if (tipoNotificacion == 0)
                JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(ponerThis, mensaje, miliSegundosAntesQueSeEsconda);
            else if (tipoNotificacion == 1)
                JqueryNotification.NotificationExtensions.ShowErrorNotification(ponerThis, mensaje, miliSegundosAntesQueSeEsconda);
            else if (tipoNotificacion == 2)
                JqueryNotification.NotificationExtensions.ShowWarningNotification(ponerThis, mensaje, miliSegundosAntesQueSeEsconda);
        }

        public void scrollToPage(Control ponerThis, string nomAncla)
        {
            ScriptManager.RegisterClientScriptBlock(ponerThis, typeof(Page), "moveTo", "location.hash = '#" + nomAncla + "';", true); ;

        }



        /// <summary>
        /// Método para agregarle 2 decimales a un valor double. Si no tiene decimales, le agrega. Si tiene mas de 2, redondea.
        /// </summary>
        /// <param name="doublee"></param>
        /// <param name="cantDecimales"></param>
        /// <returns></returns>
        public string getDoubleConDecimales(string doublee)
        {
            if (!EsNumerico(doublee))
                return doublee;

            doublee = doublee.Replace(",", ".");
            if (!doublee.Contains("."))
            {
                return doublee + ".00";
            }
            else
            {
                string[] d = doublee.Split('.');
                if (d[1].Length == 1) return d[0] + "." + d[1] + "0";
                else if (d[1].Length == 2) return doublee;
                else
                {
                    double ddd = Math.Round(Double.Parse(doublee), 2);
                    return getDoubleConDecimales(ddd.ToString());
                }
            }
        }

        public string getDoubleConDecimales(double doublee)
        {
            return getDoubleConDecimales(doublee.ToString());
        }

        /// <summary>
        /// Devuelve un string con toda la periodicidad, 3 letras por mes, separadas por comas.
        /// </summary>
        /// <param name="per"></param>
        /// <returns></returns>
        public string getPeriodicidad(string per)
        {
            string s = "";
            if (per.Contains("E")) s += ", ENE";
            if (per.Contains("F")) s += ", FEB";
            if (per.Contains("M")) s += ", MAR";
            if (per.Contains("A")) s += ", ABR";
            if (per.Contains("Y")) s += ", MAY";
            if (per.Contains("J")) s += ", JUN";
            if (per.Contains("L")) s += ", JUL";
            if (per.Contains("G")) s += ", AGO";
            if (per.Contains("S")) s += ", SEP";
            if (per.Contains("O")) s += ", OCT";
            if (per.Contains("N")) s += ", NOV";
            if (per.Contains("D")) s += ", DIC";

            if (s.Length > 0) s = s.Remove(0, 2);
            return s;
        }

        /// <summary>
        /// Método para verificar si este valor viene vacío. Muy util para impedir que den errores los insert si no le pasamos un valor o también para ingresar valores por defecto.. Se recomienda llamar este método en cada INSERT o UPDATE que halla tipo de dato INT.
        /// </summary>
        /// <param name="valorVerificar">El valor a verificar</param>
        /// <param name="returnValorSiVacio">OPCIONAL. Valor a poner si está vacío.</param>
        /// <returns></returns>
        public string ceroInsert(string valorVerificar, string returnValorSiVacio = "0")
        {
            if (valorVerificar == null || valorVerificar == "") return returnValorSiVacio;
            else return valorVerificar;
        }

        public void ejecutarTareasMantenimiento()
        {
            //TAREA 1: Pasar al estado VENCIDO las propiedades con contrato vencido y que estén en estado ALQUILADA.
            //Facturas[] facturas = Facturas.obtenerViejas(getFechaToYYYYMMDD(sumarDiasToFecha(getFechaActual(1), -1)));


        }

        /// <summary>
        /// Rellena hasta con 4 ceros los conceptos de caja o banco
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        public string rellenarConCerosConceptos(string con)
        {
            if (con.Length == 1) return "000" + con;
            else if (con.Length == 2) return "00" + con;
            else if (con.Length == 3) return "0" + con;
            else return con;
        }

        /// <summary>
        /// Armamos el Menu PRincipal de la izquierda y guardamos el string en una variable de sesion para que no lo estemos generando cada vez que cambiemos de pantalla.
        /// </summary>
        /// <param name="idUsuarioLogueado"></param>
        public string armarMenuPrincipal(string idUsuarioLogueado)
        {
            string s = "";
            if (HttpContext.Current.Session["menuPrin"] == null || HttpContext.Current.Session["menuPrin"].ToString() == "")
            {
                Menues[] menuesPadres = Menues.obtenerPadres();
                if (menuesPadres != null)
                {
                    bool elAnteriorFueVisible = true;
                    bool esElPrimerPadre = true;
                    foreach (Menues menuPadre in menuesPadres)
                    {
                        elAnteriorFueVisible = true;
                        bool esElPrimerHijo = true;
                        Menues[] menuesHijos = Menues.obtenerHijos(menuPadre.Id, idUsuarioLogueado);
                        if (menuesHijos != null)
                        {
                            foreach (Menues menuHijo in menuesHijos)
                            {
                                Permisos permiso = Permisos.obtenerPermiso(menuHijo.Id, idUsuarioLogueado);
                                if (permiso != null)
                                {
                                    if (esElPrimerHijo)
                                    {
                                        if (esElPrimerPadre)
                                            s += "<li class='nav-header hidden-tablet titMenuIzq'>" + menuPadre.Nombre + "</li>";
                                        else
                                            s += "<li class='nav-header hidden-tablet titMenuIzq'><br/>" + menuPadre.Nombre + "</li>";
                                        esElPrimerHijo = false;
                                        esElPrimerPadre = false;
                                    }

                                    Menues[] menuesNietos = Menues.obtenerHijos(menuHijo.Id, idUsuarioLogueado);
                                    bool tieneNietos = false;
                                    if (menuesNietos != null)
                                        tieneNietos = true;

                                    //TIENE PERMISO ESTE USUARIO PARA QUE LO VEA A ESTE MENU, YA SEA POR PERMISO O PORQUE ES PUBLICO.
                                    if (permiso.Visible)
                                    {
                                        //ESTE MENU NO VA AL ACCORDION
                                        if (!elAnteriorFueVisible) s += "</ul></li>"; //cerramos el accordion y continuamos con el actual menu
                                        s += "<li " + (tieneNietos ? "class='dropdown'" : "") + "><a class='ajax-link' " + (tieneNietos ? "data-toggle='dropdown'" : "") + " href='../Adm" + menuHijo.Url + "' title='" + menuHijo.Tooltip + "'><i class='icon-" + (tieneNietos ? "tasks" : "chevron-right") + "'></i><span class='hidden-tablet'>&nbsp;&nbsp;" + menuHijo.Nombre + "</span></a>";
                                        if (!tieneNietos) s += "</li>";
                                        else
                                        {
                                            //SI TIENE NIETOS, HAY QUE ARMAR EL MENU DESPLEGABLE CONTEXTUAL
                                            s += "<ul class='dropdown-menu' role='menu'>";
                                            foreach (Menues menuNieto in menuesNietos)
                                            {
                                                s += "<li><a href='" + menuNieto.Url + "' title='" + menuNieto.Tooltip + "'>" + menuNieto.Nombre + "</a></li>";
                                            }
                                            s += "</ul></li>";
                                        }
                                        elAnteriorFueVisible = true;
                                    }
                                    else
                                    {
                                        //ESTE MENU VA AL ACCORDION
                                        if (elAnteriorFueVisible) s += "<li class='accordion'><a style='cursor: default;'><i class='icon-plus-sign'></i><span>&nbsp;&nbsp;<i>M&aacute;s...</i></span></a><ul class='nav nav-pills nav-stacked' style='margin-bottom: 4px;'>";
                                        elAnteriorFueVisible = false;
                                        s += "<li " + (tieneNietos ? "class='dropdown'" : "") + "><a " + (tieneNietos ? "data-toggle='dropdown'" : "") + " href='../Adm" + menuHijo.Url + "' title='" + menuHijo.Tooltip + "'>" + menuHijo.Nombre + "</a>";
                                        if (!tieneNietos) s += "</li>";
                                        else
                                        {
                                            //SI TIENE NIETOS, HAY QUE ARMAR EL MENU DESPLEGABLE CONTEXTUAL
                                            s += "<ul class='dropdown-menu' role='menu'>";
                                            foreach (Menues menuNieto in menuesNietos)
                                            {
                                                s += "<li><a href='" + menuNieto.Url + "' title='" + menuNieto.Tooltip + "'>" + menuNieto.Nombre + "</a></li>";
                                            }
                                            s += "</ul></li>";
                                        }
                                    }
                                }
                            }
                            if (!elAnteriorFueVisible)
                                s += "</ul></li>"; //cerramos el accordion y continuamos con el actual menu
                        }
                    }
                }
                //Session.Add("menuPrin", s);
                HttpContext.Current.Session["menuPrin"] = s;
            }
            else
                s = HttpContext.Current.Session["menuPrin"].ToString();
            return s;
        }

        /// <summary>
        /// Encripta y devuelve un texto utilizando CryptographyManager
        /// </summary>
        /// <param name="s">Texto a encriptar</param>
        /// <param name="key">Key o Semilla utilizada para encriptar. 8 dígitos máx.</param>
        /// <returns></returns>
        public string encriptarTexto(string s, string key = "QwErTyVT")
        {
            helpers.CryptographyManager crypt = new helpers.CryptographyManager();
            if (key.Length > 8) key = key.Substring(0, 8);
            return crypt.Encrypt(s, key);
        }

        /// <summary>
        /// Desencripta un texto encriptado previamente utilizando CryptographyManager
        /// </summary>
        /// <param name="s">Texto encriptado para desencriptar.</param>
        /// <param name="stringSiNoPuede">String que devuelve en el caso que no puede desencriptar.</param>
        /// <param name="key">Key o Semilla utilizada cuando se encriptó. 8 dígitos máx</param>
        /// <returns></returns>
        public string desencriptarTexto(string s, string stringSiNoPuede = "-99999", string key = "QwErTyVT")
        {
            helpers.CryptographyManager crypt = new helpers.CryptographyManager();
            if (key.Length > 8) key = key.Substring(0, 8);
            s = crypt.Decrypt(s, key);
            if (s == "") return stringSiNoPuede;
            else return s;
        }

        /// <summary>
        /// Devuelve la extensión de un archivo según su path. Puede ser fullpath o solo el nombre del archivo
        /// </summary>
        /// <param name="fileName">Devuelve la extensión, SIN EL PUNTO ANTERIOR A LA EXTENSION</param>
        /// <returns></returns>
        public string getExtensionArchivo(string fileName)
        {
            string s = "";
            string[] parts = fileName.Split('.');
            s = parts[parts.Length - 1];
            return s;
        }

        /// <summary>
        /// LLamar a este método para mostrar el footer en un gridview vacío. Util cuando en el footer están las funcionalidades de agregar nueva fila.
        /// </summary>
        /// <param name="source">El DataTable vacío con las columnas ya agregadas.</param>
        /// <param name="gv">El gridview en cuestión</param>
        /// <param name="textoVacio">El texto que queremos que aparezca en rojo avisando que no hay resultados.</param>
        public void mostrarFooterEnGridviewVacio(DataTable source, GridView gv, string textoVacio)
        {
            source.Rows.Add(source.NewRow()); // create a new blank row to the DataTable
            // Bind the DataTable which contain a blank row to the GridView
            gv.DataSource = source;
            gv.DataBind();
            // Get the total number of columns in the GridView to know what the Column Span should be
            int columnsCount = gv.Columns.Count;
            gv.Rows[0].Cells.Clear();// clear all the cells in the row
            gv.Rows[0].Cells.Add(new TableCell()); //add a new blank cell
            gv.Rows[0].Cells[0].ColumnSpan = columnsCount; //set the column span to the new added cell

            //You can set the styles here
            gv.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            gv.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
            gv.Rows[0].Cells[0].Font.Bold = true;
            //set No Results found to the new added cell
            gv.Rows[0].Cells[0].Text = textoVacio;
        }

        /// <summary>
        /// LLamar a este método para mostrar el footer en un gridview vacío. Util cuando en el footer están las funcionalidades de agregar nueva fila.
        /// </summary>
        /// <param name="gv">El gridview en cuestión</param>
        /// <param name="textoVacio">El texto que queremos que aparezca en rojo avisando que no hay resultados.</param>
        /// <param name="nameColumns">Parametros infinitos de los nombres de las columnas del DataTable. Separar por comas como si fueran parámetros</param>
        public void mostrarFooterEnGridviewVacio(GridView gv, string textoVacio, params string[] nameColumns)
        {
            DataTable dt = new DataTable();
            foreach (string column in nameColumns)
                dt.Columns.Add(column);

            dt.Rows.Add(dt.NewRow()); // create a new blank row to the DataTable
            // Bind the DataTable which contain a blank row to the GridView
            gv.DataSource = dt;
            gv.DataBind();
            // Get the total number of columns in the GridView to know what the Column Span should be
            int columnsCount = gv.Columns.Count;
            gv.Rows[0].Cells.Clear();// clear all the cells in the row
            gv.Rows[0].Cells.Add(new TableCell()); //add a new blank cell
            gv.Rows[0].Cells[0].ColumnSpan = columnsCount; //set the column span to the new added cell

            //You can set the styles here
            gv.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            gv.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
            gv.Rows[0].Cells[0].Font.Bold = true;
            //set No Results found to the new added cell
            gv.Rows[0].Cells[0].Text = textoVacio;
        }

        /// <summary>
        /// (USAR CUANDO HAY UPDATE PANEL) Muestra un Message Box con solamente un boton OK y un texto informativo
        /// </summary>
        /// <param name="titulo">Título. Por ej: Atención</param>
        /// <param name="texto">Texto Informativo</param>
        /// <param name="UpdatePanel1">UpdatePanel en donde está el botón que desencadenó el evento. Por lo gral: UpdatePanel1</param>
        public void showMessageOKconUpdatePanel(string titulo, string texto, UpdatePanel UpdatePanel1)
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "ooppeennModal",
                "$(function () {" +
                    "$(\"#dialog-message\").dialog({" +
                        "modal: true," +
                        "resizable: false," +
                        "buttons: {" +
                            "OK: function () {" +
                                "$(this).dialog(\"close\");" +
                            "}" +
                        "}" +
                    "});" +
                "});"
            , true);

            Literal lit1 = new Literal();
            lit1.Text = "<div id='dialog-message' title='" + titulo + "'><p>" + texto + "</p></div>";
            UpdatePanel1.ContentTemplateContainer.Controls.Add(lit1);
        }

        /// <summary>
        /// (USAR CUANDO NO HAY UPDATE PANEL) Muestra un Message Box con solamente un boton OK y un texto informativo
        /// </summary>
        /// <param name="titulo">Título. Por ej: Atención</param>
        /// <param name="texto">Texto Informativo</param>
        /// <param name="UpdatePanel1">UpdatePanel en donde está el botón que desencadenó el evento. Por lo gral: UpdatePanel1</param>
        public void showMessageOKsinUpdatePanel(string titulo, string texto, Page page)
        {
            page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ooppeenn",
            "<script>" +
                "$(function () {" +
                    "$(\"#dialog-message\").dialog({" +
                        "modal: true," +
                        "buttons: {" +
                            "OK: function () {" +
                                "$(this).dialog(\"close\");" +
                            "}" +
                        "}" +
                    "});" +
                "});" +
            "</script>" +
            "<div id='dialog-message' title='" + titulo + "'><p>" + texto + "</p></div>", false);
        }



        /// <summary>
        /// Devuelve el IVA en formato "1.21" y le pasamos por parametro en formato "21"
        /// </summary>
        /// <param name="iva">El IVA en formato "21" que se quiere convertir</param>
        /// <returns></returns>
        public double getIVA121(double iva)
        {
            double iva121 = double.Parse("1." + (iva.ToString().Replace(".", "").Replace(",", "")).ToString());
            return iva121;
        }

        /// <summary>
        /// Selecciona un Item del DDL según el ID-Value pasado por parametro. Si no lo encuentra selecciona el primer item (SelectedIndex = 0). Si no hay items, devuelve (SelectedIndex = -1)
        /// </summary>
        /// <param name="ddl">DropDownList a verificar</param>
        /// <param name="val">Value a encontrar</param>
        public void seleccionarDDLSegunIDValue(DropDownList ddl, string val)
        {
            if (ddl.Items.Count > 0)
            {
                int i = 0;
                bool seEncontro = false;
                foreach (System.Web.UI.WebControls.ListItem li in ddl.Items)
                {
                    if (li.Value == val)
                    {
                        seEncontro = true;
                        break;
                    }
                    i++;
                }
                if (seEncontro)
                    ddl.SelectedIndex = i;
                else
                    ddl.SelectedIndex = 0;
            }
            else
                ddl.SelectedIndex = -1;
        }




        private float PixelsToPdfPoints(float value, int dpi)
        {
            return value / dpi * 72; //convierte los pixels a un tamaño de "puntos" usado por itextsharp para determinar tamaños
        }


        /// <summary>
        /// Determina si los últimos N caracteres de un texto terminan con T texto. Tiene en cuenta la longitud para que no se sobrepase y de error de Index
        /// </summary>
        /// <param name="texto">Texto completo</param>
        /// <param name="termina">Strings a validar </param>
        /// <param name="exigirCaseSensitive">Exigir que el texto a comparar sea Case Sensitive</param>
        /// <returns></returns>
        public bool terminaCon(string texto, string termina, bool exigirCaseSensitive = false)
        {
            int n = termina.Length;
            if (texto.Length < n) return false;
            if (!exigirCaseSensitive)
            {
                texto = texto.ToLower();
                termina = termina.ToLower();
            }
            if (texto.Substring(texto.Length - n, n) == termina)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fecha">Formato YYYYMMDD</param>
        /// <param name="hora">Formato HHMMSS</param>
        /// <returns></returns>
        public DateTime getFechaHoraToDateTime(string fecha, string hora)
        {
            hora = hora.PadLeft(6, '0');
            DateTime date = new DateTime(Int32.Parse(fecha.Substring(0, 4)), Int32.Parse(fecha.Substring(4, 2)), Int32.Parse(fecha.Substring(6, 2)), Int32.Parse(hora.Substring(0, 2)), Int32.Parse(hora.Substring(2, 2)), Int32.Parse(hora.Substring(4, 2)));
            return date;
        }

        /// <summary>
        /// Convierte un DateTime a HHMMSS
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public string getHoraToHHMMSS(DateTime fecha)
        {
            string hor = fecha.Hour.ToString().PadLeft(2, '0');
            string min = fecha.Minute.ToString().PadLeft(2, '0');
            string sec = fecha.Second.ToString().PadLeft(2, '0');
            return hor + min + sec;
        }

        public void scrollToTop(UpdatePanel up1)
        {
            //ScriptManager.RegisterStartupScript(page, this.GetType(), "ScrollPage", "Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);function EndRequestHandler(sender, args){scrollTo(0,0);}", true);
            ScriptManager.RegisterStartupScript(up1, this.GetType(), "ScrollTop", "setTimeout('window.scrollTo(0,0)', 0);", true);
        }

        /// <summary>
        /// Exporta un GridView a Excel
        /// </summary>
        /// <param name="fileName">Nombre del Archivo Excel. Por ej: Requerimientos.xls</param>
        /// <param name="gv">El GridView a Exportar</param>
        public void exportToExcel(string fileName, GridView gv)
        {
            Utiles u = new Utiles();

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Charset = "UTF-8";


            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    Table table = new Table();

                    //  Agrega el encabezado de la grilla a la Table
                    if (gv.HeaderRow != null)
                    {
                        PrepareControlForExport(gv.HeaderRow);
                        table.Rows.Add(gv.HeaderRow);
                        table.Rows[0].HorizontalAlign = HorizontalAlign.Center;
                    }

                    //  Agrega cada fila de la grilla a la Table
                    int i = 1;
                    foreach (GridViewRow row in gv.Rows)
                    {
                        PrepareControlForExport(row);
                        table.Rows.Add(row);
                        table.Rows[i].HorizontalAlign = HorizontalAlign.Center;
                        table.Rows[i].VerticalAlign = VerticalAlign.Middle;
                        table.Rows[i].Cells[0].Dispose();
                        i++;
                    }

                    //  add the footer row to the table
                    if (gv.FooterRow != null)
                    {
                        PrepareControlForExport(gv.FooterRow);
                        table.Rows.Add(gv.FooterRow);
                    }

                    table.GridLines = GridLines.Both;


                    //Formatea todas las celdas y las desfiltra
                    for (i = 0; i < table.Rows.Count; i++)
                    {
                        for (int j = 0; j < table.Rows[i].Cells.Count; j++)
                        {
                            table.Rows[i].Cells[j].Text = u.convertirEnStringOriginal(table.Rows[i].Cells[j].Text);
                            table.Rows[i].Cells[j].Attributes.Add("style", "background: #EFF3FB; color: 333333;");
                            table.Rows[i].Cells[j].Text = u.convertirStringExcel(table.Rows[i].Cells[j].Text);
                        }
                    }

                    //Formatea el encabezado
                    for (i = 0; i < table.Rows[0].Cells.Count; i++)
                    {
                        //table.Rows[0].Cells[i].Attributes.Add("style", "background: #507CD1; color: FFFFFF;");
                        //table.Rows[0].Cells[i].Font.Bold = true;
                        table.Rows[0].Cells[i].Attributes.Add("style", "background: #507CD1; color: FFFFFF;");
                    }

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);


                    //  render the htmlwriter into the response     

                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();



                }
            }
        }

        /// <summary>
        /// Necesario para el ExportToExcel. Reemplaza cualquier Control por Literales.
        /// </summary>
        /// <param name="control"></param>
        private void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }

                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }

        /// <summary>
        /// Permite desfiltrar lo que está almacenado en la BD y llevarlo a texto presentable en una TextBox
        /// Por ejemplo:
        ///         |!|  lo convierte en  '
        ///         |#|  lo convierte en  %
        ///         
        /// </summary>
        /// <param name="texto">texto a descfiltrar</param>
        /// <returns>Devuelve el texto a presentar en el TextBox</returns>
        public string convertirEnStringOriginal(string s)
        {
            s = s.Replace("&#161;", "¡");
            s = s.Replace("&#176;", "°");
            s = s.Replace("&#191;", "¿");
            s = s.Replace("&#186;", "º");
            s = s.Replace("&#170;", "ª");
            s = s.Replace("&#95;", "_");
            s = s.Replace("&#96;", "`");
            s = s.Replace("&#126;", "~");
            s = s.Replace("&#235;", "ë");
            s = s.Replace("&#239;", "ï");
            s = s.Replace("&#8211;", "–");
            s = s.Replace("&#8212;", "—");
            s = s.Replace("&#8216;", "‘");
            s = s.Replace("&#8217;", "’");
            s = s.Replace("&#8220;", "“");
            s = s.Replace("&#8221;", "”");
            s = s.Replace("&#8226;", "•");
            s = s.Replace("&#8230;", "…");
            s = s.Replace("&#8364;", "€");
            s = s.Replace("&#199;", "Ç");
            s = s.Replace("&#231;", "ç");
            s = s.Replace("&#180;", "´");
            s = s.Replace("&ntilde", "ñ");
            s = s.Replace("&Ntilde", "Ñ");
            s = s.Replace("&amp;#225;", "á");
            s = s.Replace("&#225;", "á");
            s = s.Replace("&amp;#233;", "é");
            s = s.Replace("&#233;", "é");
            s = s.Replace("&amp;#237;", "í");
            s = s.Replace("&#237;", "í");
            s = s.Replace("&amp;#243;", "ó");
            s = s.Replace("&#243;", "ó");
            s = s.Replace("&amp;#250;", "ú");
            s = s.Replace("&#250;", "ú");
            s = s.Replace("&amp;#193;", "Á");
            s = s.Replace("&#193;", "Á");
            s = s.Replace("&amp;#201;", "É");
            s = s.Replace("&#201;", "É");
            s = s.Replace("&amp;#205;", "Í");
            s = s.Replace("&#205;", "Í");
            s = s.Replace("&amp;#211;", "Ó");
            s = s.Replace("&#211;", "Ó");
            s = s.Replace("&amp;#218;", "Ú");
            s = s.Replace("&#218;", "Ú");
            s = s.Replace("<br/>", "\n");
            return s;
        }
        
        //Para colocar en mayuscula la primera letra de una palabra y el resto en minuscula
        public String formatMayuscula(String palabra) {
            String formateado;
            formateado =  palabra.Substring(0, 1).ToUpper() + palabra.Substring(1).ToLower();
            return formateado;
        }
    }
}