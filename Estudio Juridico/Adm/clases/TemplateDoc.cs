﻿using NGS.Templater;
using System;
using System.IO;
using System.Web.UI.WebControls;


namespace Estudio_Juridico.Adm.clases
{
    public class TemplateDoc
    {

        public void loadTemplate(Clientes cli,String rutaTemplate,String nombreTemplate) {

            //Ruta donde se guardara el documento al que se le efectura el remplazo de tags.
            String carpetaDestino = "C:\\Plantillas";
            //Primero verificamos que si existe una carpeta ai no la creamos
            bool existe = System.IO.Directory.Exists(carpetaDestino);
            if (!existe)
            {
                System.IO.Directory.CreateDirectory(carpetaDestino);
            }

            string archivoFinal = carpetaDestino + "\\" + cli.Apellido + "-"+ nombreTemplate;

            System.IO.File.Copy(rutaTemplate+nombreTemplate,archivoFinal);
         
            if (archivoFinal != "")
            {
                var Factory = Configuration.Factory;
                var doc = Factory.Open(archivoFinal);
                doc.Process(new { FECHA = DateTime.Now.ToShortDateString() });
                doc.Process(new { TITULAR = cli.Apellido + " " + cli.Nombre });
                doc.Process(new { DNITITULAR = cli.Nrodoc });
                doc.Process(new { CALLE = cli.Direccion + " " + cli.Altura });
                doc.Dispose();
            }

        }

        public static void llenarDDL(DropDownList ddlParaLlenar,String rutaTemplates, bool primerValorEnBlanco)
        {
            ddlParaLlenar.Items.Clear();

            DirectoryInfo di = new DirectoryInfo(@rutaTemplates);

            if (primerValorEnBlanco)
            {
                ddlParaLlenar.Items.Add("");
                ddlParaLlenar.Items[0].Value = "";
            }

            foreach (var fi in di.GetFiles())
            {
                ddlParaLlenar.Items.Add(fi.Name);
                ddlParaLlenar.Items[ddlParaLlenar.Items.Count - 1].Value = fi.Name;
            }            

        }

    }
}