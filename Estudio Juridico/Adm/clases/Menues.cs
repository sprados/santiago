﻿namespace Estudio_Juridico.Adm.clases
{
    public class Menues
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Tooltip { get; set; }
        public string Url { get; set; }
        public string IdPadre { get; set; }
        public bool SiempreVisibleAdmin { get; set; }
        public string PosicionPorDefecto { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Nombre = dt.Rows[fila]["nombre"].ToString();
            Tooltip = dt.Rows[fila]["tooltip"].ToString();
            Url = dt.Rows[fila]["url"].ToString();
            IdPadre = dt.Rows[fila]["idpadre"].ToString();
            SiempreVisibleAdmin = (dt.Rows[fila]["SiempreVisibleAdmin"].ToString() == "0" ? false : true);
            PosicionPorDefecto = dt.Rows[fila]["posicionpordefecto"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into menues (id,nombre,tooltip,url,idPadre,siempreVisibleAdmin,PosicionPorDefecto) values ('" + Id + "','" + Nombre + "','" + Tooltip + "','" + Url + "','" + IdPadre + "','" + SiempreVisibleAdmin + "','" + PosicionPorDefecto + "')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update menues set id='" + Id + "',nombre='" + Nombre + "',tooltip='" + Tooltip + "',url='" + Url + "',idPadre='" + IdPadre + "',siempreVisibleAdmin='" + SiempreVisibleAdmin + "',PosicionPorDefecto='" + PosicionPorDefecto + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from menues where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static Menues getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Menues op = new Menues();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Menues[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Menues[] op = null;
            if (dt != null)
            {
                op = new Menues[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Menues();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Menues obtenerPorId(string id)
        {
            return getFila("select * from menues where id=" + id);
        }

        public static Menues[] obtenerPadres()
        {
            return getFilas("select * from menues where idPadre=0 order by PosicionPorDefecto");
        }

        public static Menues[] obtenerTodosHijos()
        {
            return getFilas("select * from menues where idPadre<>0 order by idpadre");
        }

        public static Menues[] obtenerTodos()
        {
            return getFilas("SELECT * FROM menues order by id");
        }

        public static Menues[] obtenerHijos(string idPadre, string idUsuario)
        {
            return getFilas("select m.* from menues m, menu_permisos p where m.id=p.idmenu and m.idPadre=" + idPadre + " and p.idusuario=" + idUsuario + " order by p.posicion");
        }

        public static Menues[] obtenerHijos(string idPadre)
        {
            return getFilas("select * from menues where idPadre=" + idPadre);
        }

        public static Menues obtenerPorURL(string url)
        {
            return getFila("select * from menues where url='" + url.Replace("Adm", "") + "'");
        }
    }
}