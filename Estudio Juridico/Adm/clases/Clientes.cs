﻿using System.Data;
using System.Web.UI.WebControls;
namespace Estudio_Juridico.Adm.clases
{
    public class Clientes
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Tipodoc { get; set; }
        public string Nrodoc { get; set; }
        public string Direccion { get; set; }
        public string Altura { get; set; }
        public string Piso { get; set; }
        public string Depto { get; set; }
        public string Telef { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Razsoc { get; set; }
        public string Tipoiva { get; set; }
        public string Fecnacimiento { get; set; }
        public string Estado { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Nombre = dt.Rows[fila]["nombre"].ToString();
            Apellido = dt.Rows[fila]["apellido"].ToString();
            Tipodoc = dt.Rows[fila]["tipodoc"].ToString();
            Nrodoc = dt.Rows[fila]["nrodoc"].ToString();
            Direccion = dt.Rows[fila]["direccion"].ToString();
            Altura = dt.Rows[fila]["altura"].ToString();
            Piso = dt.Rows[fila]["piso"].ToString();
            Depto = dt.Rows[fila]["depto"].ToString();
            Telef = dt.Rows[fila]["telef"].ToString();
            Celular = dt.Rows[fila]["celular"].ToString();
            Email = dt.Rows[fila]["email"].ToString();
            Razsoc = dt.Rows[fila]["razsoc"].ToString();
            Tipoiva = dt.Rows[fila]["tipoiva"].ToString();
            Fecnacimiento = dt.Rows[fila]["fecnacimiento"].ToString();
            Estado = dt.Rows[fila]["estado"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into clientes (nombre,apellido,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,razsoc,tipoiva,fecnacimiento) values ('" + u.formatMayuscula(Nombre) + "','" + Apellido + "','" + Tipodoc + "','" + Nrodoc + "','" + Direccion + "','" + Altura + "','" + Piso + "','" + Depto + "','" + Telef + "','" + Celular + "','" + Email + "','" + Razsoc + "','" + Tipoiva + "','" + Fecnacimiento + "')";

            if (u.ejecutarNonSelect(sql))
            {
                DataTable dt = u.ejecutarSelect("SELECT last_insert_id()");
                Id = dt.Rows[0][0].ToString();
                return true;
            }
            else return false;
        }

        public bool update()
        {
            string sql = "update clientes set nombre='"+u.formatMayuscula(Nombre)+"',apellido='"+Apellido+"',tipodoc='" + Tipodoc + "',nrodoc='" + Nrodoc + "',direccion='" + Direccion + "',altura='" + Altura + "',piso='" + Piso + "',depto='" + Depto + "',telef='" + Telef + "',celular='" + Celular + "',email='" + Email + "',razsoc='" + Razsoc + "',tipoiva='" + Tipoiva + "',fecnacimiento='" + Fecnacimiento + "',estado='" + Estado + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "update clientes set estado = 'b' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static Clientes getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Clientes op = new Clientes();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Clientes[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Clientes[] op = null;
            if (dt != null)
            {
                op = new Clientes[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Clientes();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Clientes obtenerPorId(string id)
        {
            return getFila("select * from clientes where id=" + id);
        }

        public static Clientes obtenerPorDNI(string dni)
        {
            return getFila("select * from clientes where nrodoc=" + dni);
        }

        public static Clientes[] obtenerTodos()
        {
            return getFilas("select * from clientes order by apellido asc, nombre");
        }

        public static Clientes[] obtenerTodosTipoClientes()
        {
            return getFilas("select * from clientes order by apellido asc, nombre");
        }

        public static void llenarDDL(DropDownList ddlParaLlenar, bool primerValorEnBlanco)
        {
            ddlParaLlenar.Items.Clear();
            Clientes[] clis = (Clientes.obtenerTodos());
            if (clis != null)
            {
                if (primerValorEnBlanco)
                {
                    ddlParaLlenar.Items.Add("");
                    ddlParaLlenar.Items[0].Value = "";
                }
                foreach (Clientes c in clis)
                {
                    ddlParaLlenar.Items.Add(c.Apellido + " " + c.Nombre);
                    ddlParaLlenar.Items[ddlParaLlenar.Items.Count - 1].Value = c.Id;
                }
            }
        }
    }
}