﻿using System.Web;
using System.Web.UI;

namespace Estudio_Juridico.Adm.clases
{
    public class Permisos
    {
        Utiles u = new Utiles();

        public string IdMenu { get; set; }
        public string IdUsuario { get; set; }
        public bool Visible { get; set; }
        public string Posicion { get; set; }
        public bool Alta { get; set; }
        public bool Baja { get; set; }
        public bool Modif { get; set; }
        public bool Consulta { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            IdMenu = dt.Rows[fila]["idmenu"].ToString();
            IdUsuario = dt.Rows[fila]["idusuario"].ToString();
            Visible = (dt.Rows[fila]["visible"].ToString() == "0" ? false : true);
            Posicion = dt.Rows[fila]["posicion"].ToString();
            Alta = (dt.Rows[fila]["alta"].ToString() == "0" ? false : true);
            Baja = (dt.Rows[fila]["baja"].ToString() == "0" ? false : true);
            Modif = (dt.Rows[fila]["modif"].ToString() == "0" ? false : true);
            Consulta = (dt.Rows[fila]["consulta"].ToString() == "0" ? false : true);
        }

        public bool guardar()
        {
            string sql = "insert into menu_permisos (idmenu,idusuario,visible,posicion,alta,baja,modif,consulta) " +
                "values (" + IdMenu + ", " + IdUsuario + "," + u.ceroInsert(Visible.ToString()) + "," + u.ceroInsert(Posicion) + "," + u.ceroInsert(Alta.ToString()) + "," + u.ceroInsert(Baja.ToString()) + "," + u.ceroInsert(Modif.ToString()) + "," + u.ceroInsert(Consulta.ToString()) + ")";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update menu_permisos set visible=" + u.ceroInsert(Visible.ToString()) + ", posicion=" + u.ceroInsert(Posicion) + " where idmenu=" + IdMenu + " and idusuario=" + IdUsuario;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from menu_permisos where idMenu=" + IdMenu;
            return u.ejecutarNonSelect(sql);
        }

        private static Permisos getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Permisos op = new Permisos();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Permisos[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Permisos[] op = null;
            if (dt != null)
            {
                op = new Permisos[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Permisos();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Permisos obtenerPorId(string id)
        {
            return getFila("select * from menu_permisos where idMenu=" + id);
        }

        public static Permisos[] obtenerTodos()
        {
            return getFilas("select * from menu_permisos");
        }

        public static Permisos obtenerPermiso(string idMenu, string idUsuario)
        {
            return getFila("select * from menu_permisos where idMenu=" + idMenu + " and idusuario=" + idUsuario);
        }

        public bool DeleteTodosDeUsuario()
        {
            Utiles u = new Utiles();
            string sql = "delete from menu_permisos where idusuario=" + IdUsuario;
            return u.ejecutarNonSelect(sql);
        }

        public static bool verificarPermiso(Page pagina, bool autoRedireccionarSiError = true)
        {
            Menues menu = Menues.obtenerPorURL(pagina.AppRelativeVirtualPath.Remove(0, 2)); //RECUPERAMOS EL ID MENU DE LA PAGINA 
            if (menu == null)
            {
                if (autoRedireccionarSiError)
                {
                    HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a esta página.");
                    pagina.Response.Redirect("Avisos.aspx");
                    return false;
                }
                else
                    return false;
            }

            Permisos mp = obtenerPermiso(menu.Id, HttpContext.Current.Session["idUser"].ToString());
            if (mp != null)
                return true;
            else if (autoRedireccionarSiError)
            {
                HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a esta página.");
                pagina.Response.Redirect("Avisos.aspx");
                return false;
            }
            else return false;
        }

        public static bool verificarPermisoConsulta(Page pagina, bool autoRedireccionarSiError = true)
        {
            Menues menu = Menues.obtenerPorURL(pagina.AppRelativeVirtualPath.Remove(0, 2)); //RECUPERAMOS EL ID MENU DE LA PAGINA 
            if (menu == null)
            {
                if (autoRedireccionarSiError)
                {
                    HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a CONSULTAR a esta página.");
                    pagina.Response.Redirect("Avisos.aspx");
                    return false;
                }
                else
                    return false;
            }

            Permisos mp = obtenerPermiso(menu.Id, HttpContext.Current.Session["idUser"].ToString());
            if (mp != null && mp.Consulta)
                return true;
            else if (autoRedireccionarSiError)
            {
                HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a CONSULTAR a esta página.");
                pagina.Response.Redirect("Avisos.aspx");
                return false;
            }
            else return false;
        }

        public static bool verificarPermisoModificar(Page pagina, bool autoRedireccionarSiError = true)
        {
            Menues menu = Menues.obtenerPorURL(pagina.AppRelativeVirtualPath.Remove(0, 2)); //RECUPERAMOS EL ID MENU DE LA PAGINA 
            if (menu == null)
            {
                if (autoRedireccionarSiError)
                {
                    HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a MODIFICAR a esta página.");
                    pagina.Response.Redirect("Avisos.aspx");
                    return false;
                }
                else
                    return false;
            }

            Permisos mp = obtenerPermiso(menu.Id, HttpContext.Current.Session["idUser"].ToString());
            if (mp != null && mp.Modif)
                return true;
            else if (autoRedireccionarSiError)
            {
                HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a MODIFICAR a esta página.");
                pagina.Response.Redirect("Avisos.aspx");
                return false;
            }
            else return false;
        }

        public static bool verificarPermisoBaja(Page pagina, bool autoRedireccionarSiError = true)
        {
            Menues menu = Menues.obtenerPorURL(pagina.AppRelativeVirtualPath.Remove(0, 2)); //RECUPERAMOS EL ID MENU DE LA PAGINA 
            if (menu == null)
            {
                if (autoRedireccionarSiError)
                {
                    HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a dar BAJAS a esta página.");
                    pagina.Response.Redirect("Avisos.aspx");
                    return false;
                }
                else
                    return false;
            }

            Permisos mp = obtenerPermiso(menu.Id, HttpContext.Current.Session["idUser"].ToString());
            if (mp != null && mp.Baja)
                return true;
            else if (autoRedireccionarSiError)
            {
                HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a dar BAJAS a esta página.");
                pagina.Response.Redirect("Avisos.aspx");
                return false;
            }
            else return false;
        }

        public static bool verificarPermisoAlta(Page pagina, bool autoRedireccionarSiError = true)
        {
            Menues menu = Menues.obtenerPorURL(pagina.AppRelativeVirtualPath.Remove(0, 2)); //RECUPERAMOS EL ID MENU DE LA PAGINA 
            if (menu == null)
            {
                if (autoRedireccionarSiError)
                {
                    HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a dar ALTAS a esta página.");
                    pagina.Response.Redirect("Avisos.aspx");
                    return false;
                }
                else
                    return false;
            }

            Permisos mp = obtenerPermiso(menu.Id, HttpContext.Current.Session["idUser"].ToString());
            if (mp != null && mp.Alta)
                return true;
            else if (autoRedireccionarSiError)
            {
                HttpContext.Current.Session.Add("avisos", "Ud. no tiene permisos para ingresar a dar ALTAS a esta página.");
                pagina.Response.Redirect("Avisos.aspx");
                return false;
            }
            else return false;
        }
    }
}