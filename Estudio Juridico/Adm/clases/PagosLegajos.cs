﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Estudio_Juridico.Adm.clases
{
    public class PagosLegajo
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Monto { get; set; }
        public string Fecha { get; set; }
        public string IdLegajo { get; set; }
        public string Observaciones { get; set; }
        public string Moneda { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Monto = dt.Rows[fila]["monto"].ToString();
            Fecha = dt.Rows[fila]["fecha"].ToString();
            IdLegajo = dt.Rows[fila]["idlegajo"].ToString();
            Observaciones = dt.Rows[fila]["observaciones"].ToString();
            Moneda = dt.Rows[fila]["moneda"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into pago_x_legajo (monto,fecha,idLegajo,observaciones,moneda) values ('" + Monto + "','" + Fecha + "','" + IdLegajo + "','" + Observaciones + "','" + Moneda + "')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update pago_x_legajo set monto='" + Monto + "',fecha='" + Fecha + "',idLegajo='" + IdLegajo + "',observaciones='" + Observaciones + "',moneda='" + Moneda + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from pago_x_legajo where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static PagosLegajo getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            PagosLegajo op = new PagosLegajo();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static PagosLegajo[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            PagosLegajo[] op = null;
            if (dt != null)
            {
                op = new PagosLegajo[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new PagosLegajo();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static PagosLegajo obtenerPorId(string id)
        {
            return getFila("select * from pago_x_legajo where id=" + id);
        }

        public static PagosLegajo[] obtenerTodos()
        {
            return getFilas("select * from pago_x_legajo order by fecha desc,id");
        }

        public static PagosLegajo[] obtenerTodosPorIdLegajo(string idLegajo)
        {
            return getFilas("select * from pago_x_legajo where idLegajo=" + idLegajo + " order by fecha desc,id desc");
        }
    }
}