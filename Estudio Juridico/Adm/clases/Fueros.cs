﻿using System.Web.UI.WebControls;
namespace Estudio_Juridico.Adm.clases
{
    public class Fueros
    {
        Utiles u = new Utiles();

        public string IdFueros { get; set; }
        public string Nombre { get; set; }
        public string Estado { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            IdFueros = dt.Rows[fila]["idfueros"].ToString();
            Nombre = dt.Rows[fila]["nombre"].ToString();
            Estado = dt.Rows[fila]["estado"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into fueros (nombre,estado) values ('" + Nombre + "','a')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update fueros set nombre='" + Nombre + "',estado='" + Estado + "' where idFueros=" + IdFueros;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "update fueros set estado = 'b' where idFueros=" + IdFueros;
            return u.ejecutarNonSelect(sql);
        }

        private static Fueros getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Fueros op = new Fueros();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Fueros[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Fueros[] op = null;
            if (dt != null)
            {
                op = new Fueros[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Fueros();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Fueros obtenerPorId(string id)
        {
            return getFila("select * from fueros where idFueros=" + id);
        }


        public static Fueros obtenerPorNombre(string nombre)
        {
            return getFila("select * from fueros where nombre like '" + nombre + "'");
        }


        public static Fueros[] obtenerTodos()
        {
            return getFilas("select * from fueros order by nombre");
        }

        public static void llenarDDL(DropDownList ddlParaLlenar, bool primerValorEnBlanco)
        {
            ddlParaLlenar.Items.Clear();
            Fueros[] fue = Fueros.obtenerTodos();
            if (fue != null)
            {
                if (primerValorEnBlanco)
                {
                    ddlParaLlenar.Items.Add("");
                    ddlParaLlenar.Items[0].Value = "";
                }
                foreach (Fueros c in fue)
                {
                    ddlParaLlenar.Items.Add(c.Nombre);
                    ddlParaLlenar.Items[ddlParaLlenar.Items.Count - 1].Value = c.IdFueros;
                }
            }
        }
    }
}