﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Estudio_Juridico.Adm.clases
{
    public class AdjuntosLegajo
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string IdLegajo { get; set; }
        public string RutaAdjunto { get; set; }
        public string Fecha_alta { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            IdLegajo = dt.Rows[fila]["idlegajo"].ToString();
            RutaAdjunto = dt.Rows[fila]["rutaadjunto"].ToString();
            Fecha_alta = dt.Rows[fila]["fecha_alta"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into adjuntos_x_legajo (idLegajo,rutaAdjunto,fecha_alta) values ('" + IdLegajo + "','" + RutaAdjunto + "','" + Fecha_alta + "')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update adjuntos_x_legajo set id='" + Id + "',idLegajo='" + IdLegajo + "',rutaAdjunto='" + RutaAdjunto + "',fecha_alta='" + Fecha_alta + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from adjuntos_x_legajo where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static AdjuntosLegajo getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            AdjuntosLegajo op = new AdjuntosLegajo();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static AdjuntosLegajo[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            AdjuntosLegajo[] op = null;
            if (dt != null)
            {
                op = new AdjuntosLegajo[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new AdjuntosLegajo();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static AdjuntosLegajo obtenerPorId(string id)
        {
            return getFila("select * from adjuntos_x_legajo where id=" + id);
        }

        public static AdjuntosLegajo[] obtenerTodos()
        {
            return getFilas("select * from adjuntos_x_legajo");
        }

        public static AdjuntosLegajo[] obtenerPorIdLegajo(string idLegajo)
        {
            // DATE_FORMAT( fecha_alta,  '%d/%m/%Y %H:%i:%s' ) fecha_alta
            return getFilas("SELECT * from adjuntos_x_legajo where idlegajo = " + idLegajo);
        }

        public bool DeleteAdjuntoPorId()
        {
            Utiles u = new Utiles();
            string sql = "delete from adjuntos_x_legajo where id = '" + Id + "'";
            return u.ejecutarNonSelect(sql);
        }

        public bool InsertIntoDB()
        {
            Utiles u = new Utiles();
            string sql = "insert into adjuntos_x_legajo (idLegajo,rutaAdjunto,fecha_alta) values ('" + IdLegajo + "','" + RutaAdjunto + "','" + Fecha_alta + "')";
            if (u.ejecutarNonSelect(sql))
            {
                DataTable dt = u.ejecutarSelect("SELECT last_insert_id()");
                Id = dt.Rows[0][0].ToString();
                return true;
            }
            else return false;
        }
    }
}