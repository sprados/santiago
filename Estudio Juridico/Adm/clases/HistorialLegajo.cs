﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Estudio_Juridico.Adm.clases
{
    public class HistorialLegajo
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Historial { get; set; }
        public string IdLegajo { get; set; }
        public string Fecha { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Historial = dt.Rows[fila]["historial"].ToString();
            IdLegajo = dt.Rows[fila]["idlegajo"].ToString();
            Fecha = dt.Rows[fila]["fecha"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into historial_x_legajo (historial,idLegajo,fecha) values ('" + Historial + "','" + IdLegajo + "','" + Fecha + "')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update historial_x_legajo set historial='" + Historial + "',idLegajo='" + IdLegajo + "',fecha='" + Fecha + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from historial_x_legajo where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static HistorialLegajo getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            HistorialLegajo op = new HistorialLegajo();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static HistorialLegajo[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            HistorialLegajo[] op = null;
            if (dt != null)
            {
                op = new HistorialLegajo[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new HistorialLegajo();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static HistorialLegajo obtenerPorId(string id)
        {
            return getFila("select * from historial_x_legajo where id=" + id);
        }

        public static HistorialLegajo[] obtenerTodos()
        {
            return getFilas("select * from historial_x_legajo order by fecha desc,id desc");
        }

        public static HistorialLegajo[] obtenerTodosPorIdLegajo(string id)
        {
            return getFilas("select * from historial_x_legajo where idLegajo=" + id);
        }
    }
}