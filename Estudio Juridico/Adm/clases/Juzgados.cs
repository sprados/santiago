﻿namespace Estudio_Juridico.Adm.clases
{
    public class Juzgados
    {
        Utiles u = new Utiles();

        public string IdJuzgados { get; set; }
        public string IdFueros { get; set; }
        public string Juzgado { get; set; }
        public string Estado { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            IdJuzgados = dt.Rows[fila]["idjuzgados"].ToString();
            IdFueros = dt.Rows[fila]["idfueros"].ToString();
            Juzgado = dt.Rows[fila]["juzgado"].ToString();
            Estado = dt.Rows[fila]["estado"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into juzgados (idFueros,juzgado,estado) values ('" + IdFueros + "','" + Juzgado + "','a')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update juzgados set idFueros='" + IdFueros + "',juzgado='" + Juzgado + "',estado='" + Estado + "' where idJuzgados=" + IdJuzgados;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from juzgados where idJuzgados=" + IdJuzgados;
            return u.ejecutarNonSelect(sql);
        }

        private static Juzgados getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Juzgados op = new Juzgados();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Juzgados[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Juzgados[] op = null;
            if (dt != null)
            {
                op = new Juzgados[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Juzgados();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Juzgados obtenerPorId(string id)
        {
            return getFila("select * from juzgados where idJuzgados=" + id);
        }

        public static Juzgados[] obtenerTodos()
        {
            return getFilas("select * from juzgados order by juzgado");
        }


    }
}