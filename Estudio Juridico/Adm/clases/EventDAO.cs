﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Estudio_Juridico.Adm.clases;

/// <summary>
/// Esta clase interectua con la tabla de la base de datos llamada event
/// Formato de la tabla event (event_id int, title varchar(100), description varchar(200),event_start datetime, event_end datetime)
/// event_id is es primary key (Autoincremental)
/// </summary>
/// 
public class EventDAO
{

    // Este método recupera todos los eventos dentro del rango de inicio a fin
    public static List<CalendarEvent> getEvents(DateTime start, DateTime end)
    {

        List<CalendarEvent> events = new List<CalendarEvent>();


        String fechaDesde = start.Year + "-" + start.Month + "-" + start.Day + " " + start.TimeOfDay;
        String fechaHasta = end.Year + "-" + end.Month + "-" + end.Day + " " + end.TimeOfDay;


        Utiles u = new Utiles();
        u.abrirConexion();
        String sqlEvents = "SELECT event_id, description, title, event_start, event_end FROM event where event_start>='" + fechaDesde + "' AND event_end<='" + fechaHasta + "'";
        System.Data.DataTable dtEvents = u.ejecutarSelect(sqlEvents);



        if (dtEvents != null)
        {
            foreach (DataRow fila in dtEvents.Rows)
            {
                CalendarEvent cevent = new CalendarEvent();
                cevent.id = (int)fila["event_id"];
                cevent.title = (string)fila["title"];
                cevent.description = (string)fila["description"];
                cevent.start = (DateTime)fila["event_start"];
                cevent.end = (DateTime)fila["event_end"];
                events.Add(cevent);
            }
        }

        return events;
        //side note: if you want to show events only related to particular users,
        //if user id of that user is stored in session as Session["userid"]
        //the event table also contains a extra field named 'user_id' to mark the event for that particular user
        //then you can modify the SQL as:
        //SELECT event_id, description, title, event_start, event_end FROM event where user_id=@user_id AND event_start>=@start AND event_end<=@end
        //then add paramter as:cmd.Parameters.AddWithValue("@user_id", HttpContext.Current.Session["userid"]);
    }

    //Este método actualiza el título y la descripción del evento
    public static void updateEvent(int id, String title, String description)
    {
        Utiles u = new Utiles();
        string sql = "UPDATE event SET title='" + title + "', description='" + description + "' WHERE event_id=" + id;
        u.ejecutarNonSelect(sql);


    }

    // Este método actualiza la hora de inicio y finalización del evento
    public static void updateEventTime(int id, DateTime start, DateTime end)
    {

        Utiles u = new Utiles();
        String fechaDesde = start.Year + "-" + start.Month + "-" + start.Day + " " + start.TimeOfDay;
        String fechaHasta = end.Year + "-" + end.Month + "-" + end.Day + " " + end.TimeOfDay;

        string sql = "UPDATE event SET event_start = '" + fechaDesde + "', event_end='" + fechaHasta + "' WHERE event_id = " + id;
        u.ejecutarNonSelect(sql);


    }

    //Este mehtod elimina el evento con el id pasado
    public static void deleteEvent(int id)
    {


        Utiles u = new Utiles();
        string sql = "DELETE FROM event WHERE (event_id =" + id + ")";
        u.ejecutarNonSelect(sql);
    }


    //Este método agrega eventos a la base de datos devuelve la clave primaria de la fila de evento agregada
    public static int addEvent(CalendarEvent cevent)
    {

        int key = 0;

        String fechaDesde = cevent.start.Year + "-" + cevent.start.Month + "-" + cevent.start.Day + " " + cevent.start.TimeOfDay;
        String fechaHasta = cevent.end.Year + "-" + cevent.end.Month + "-" + cevent.end.Day + " " + cevent.end.TimeOfDay;

        Utiles u = new Utiles();
        string sql = "INSERT INTO event(title, description, event_start, event_end) VALUES('" + cevent.title + "','" + cevent.description + "','" + fechaDesde + "', '" + fechaHasta + "')";
        u.ejecutarNonSelect(sql);


        string sqlKey = "SELECT max(event_id) FROM event where title='" + cevent.title + "' AND description='" + cevent.description + "' AND event_start='" + fechaDesde + "' AND event_end='" + fechaHasta + "'";
        System.Data.DataTable dtkey = u.ejecutarSelect(sqlKey);

        foreach (DataRow fila in dtkey.Rows)
        {
            key = (int)fila[0];
        }

        return key;

    }



}
