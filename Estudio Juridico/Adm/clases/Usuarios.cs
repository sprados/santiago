﻿namespace Estudio_Juridico.Adm.clases
{
    public class Usuarios
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string Email { get; set; }
        public string Estado { get; set; }
        public string Nuevospermisos { get; set; }
        public string Nuevanovedad { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Username = dt.Rows[fila]["username"].ToString();
            Pass = dt.Rows[fila]["pass"].ToString();
            Nombre = dt.Rows[fila]["nombre"].ToString();
            Tipo = dt.Rows[fila]["tipo"].ToString();
            Email = dt.Rows[fila]["email"].ToString();
            Estado = dt.Rows[fila]["estado"].ToString();
            Nuevospermisos = dt.Rows[fila]["nuevospermisos"].ToString();
            Nuevanovedad = dt.Rows[fila]["nuevanovedad"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into usuarios (username,pass,nombre,tipo,email,estado,nuevospermisos) values ('" + Username + "','" + Pass + "','" + Nombre + "','" + Tipo + "','" + Email + "','" + Estado + "','" + Nuevospermisos + "')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update usuarios set id='" + Id + "',username='" + Username + "',pass='" + Pass + "',nombre='" + Nombre + "',tipo='" + Tipo + "',email='" + Email + "',estado='" + Estado + "',nuevospermisos='" + Nuevospermisos + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "delete from usuarios where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static Usuarios getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Usuarios op = new Usuarios();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Usuarios[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Usuarios[] op = null;
            if (dt != null)
            {
                op = new Usuarios[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Usuarios();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Usuarios obtenerPorId(string id)
        {
            return getFila("select * from usuarios where id=" + id);
        }

        public static Usuarios[] obtenerTodos()
        {
            return getFilas("select * from usuarios order by nombre");
        }

        public static Usuarios obtenerPorIdUsuario(string id)
        {
            return getFila("select * from usuarios where id=" + id);
        }

        public bool updateUser()
        {
            Utiles u = new Utiles();
            string sql = "update usuarios set nombre='" + Nombre + "',username='" + Username + "', pass='" + Pass + "',email='" + Email + "',estado='" + Estado + "'" + ",nuevospermisos=" + u.ceroInsert(Nuevospermisos.ToString()) + " where id=" + Id;

            return u.ejecutarNonSelect(sql);
        }

        public bool updateEsNuevaNovedad(string valor)
        {
            string sql = "update usuarios set nuevanovedad='" + valor + "' where id=" + Id;
            Utiles u = new Utiles();
            return u.ejecutarNonSelect(sql);
        }

        public static Usuarios esUsuario(string Username, string password)
        {
            Utiles u = new Utiles();
            Username = u.sqlSeguro(Username);
            password = u.sqlSeguro(password);
            return getFila("select * from usuarios where username='" + Username + "' and pass='" + password + "'");
        }

        public static Usuarios obtenerUserName(string UserName)
        {
            return getFila("select * from usuarios where username='" + UserName+"'");
        }


    }
}