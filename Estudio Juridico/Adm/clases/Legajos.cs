﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Estudio_Juridico.Adm.clases
{
    public class Legajos
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Fecha { get; set; }
        public string IdCliente { get; set; }
        public string Codigo { get; set; }
        public string Estado { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Descripcion = dt.Rows[fila]["descripcion"].ToString();
            Fecha = dt.Rows[fila]["fecha"].ToString();
            IdCliente = dt.Rows[fila]["idcliente"].ToString();
            Codigo = dt.Rows[fila]["codigo"].ToString();
            Estado = dt.Rows[fila]["estado"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into legajos (descripcion,fecha,idCliente,codigo) values ('" + Descripcion + "','" + Fecha + "','" + IdCliente + "','" + Codigo + "')";
            if (u.ejecutarNonSelect(sql))
            {
                DataTable dt = u.ejecutarSelect("SELECT last_insert_id()");
                Id = dt.Rows[0][0].ToString();
                return true;
            }
            else return false;
        }

        public bool update()
        {
            string sql = "update legajos set descripcion='" + Descripcion + "',fecha='" + Fecha + "',idCliente='" + IdCliente + "',codigo='" + Codigo + "',estado='" + Estado + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "update legajos set estado = 'b' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static Legajos getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            Legajos op = new Legajos();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static Legajos[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            Legajos[] op = null;
            if (dt != null)
            {
                op = new Legajos[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new Legajos();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static Legajos obtenerPorId(string id)
        {
            return getFila("select * from legajos where id=" + id);
        }

        public static Legajos[] obtenerTodos()
        {
            return getFilas("select * from legajos order by estado asc");
        }

        public static Legajos[] obtenerTodosConFiltros(string where)
        {
            return getFilas("select l.* from legajos l " + where);
        }
    }
}