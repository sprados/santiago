﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Estudio_Juridico.Adm.clases
{
    public class ActoresLegajo
    {
        Utiles u = new Utiles();

        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Tipodoc { get; set; }
        public string Nrodoc { get; set; }
        public string Direccion { get; set; }
        public string Altura { get; set; }
        public string Piso { get; set; }
        public string Depto { get; set; }
        public string Telef { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Fecnacimiento { get; set; }
        public string Estado { get; set; }
        public string IdLegajo { get; set; }

        private void loadData(System.Data.DataTable dt, int fila)
        {
            Id = dt.Rows[fila]["id"].ToString();
            Nombre   = dt.Rows[fila]["nombre"].ToString();
            Apellido = dt.Rows[fila]["apellido"].ToString();
            Tipodoc  = dt.Rows[fila]["tipodoc"].ToString();
            Nrodoc    = dt.Rows[fila]["nrodoc"].ToString();
            Direccion = dt.Rows[fila]["direccion"].ToString();
            Altura = dt.Rows[fila]["altura"].ToString();
            Piso = dt.Rows[fila]["piso"].ToString();
            Depto = dt.Rows[fila]["depto"].ToString();
            Telef = dt.Rows[fila]["telef"].ToString();
            Celular = dt.Rows[fila]["celular"].ToString();
            Email = dt.Rows[fila]["email"].ToString();
            Fecnacimiento = dt.Rows[fila]["fecnacimiento"].ToString();
            Estado = dt.Rows[fila]["estado"].ToString();
            IdLegajo = dt.Rows[fila]["idLegajo"].ToString();
        }

        public bool guardar()
        {
            string sql = "insert into actores_x_legajo (nombre,apellido,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values ('" + u.formatMayuscula(Nombre) + "','" + Apellido + "','" + Tipodoc + "','" + Nrodoc + "','" + Direccion + "','" + Altura + "','" + Piso + "','" + Depto + "','" + Telef + "','" + Celular + "','" + Email + "','" + Fecnacimiento + "','" + IdLegajo + "')";
            return u.ejecutarNonSelect(sql);
        }

        public bool update()
        {
            string sql = "update actores_x_legajo set nombre='" + Nombre + "',apellido='" +Apellido+ "',tipodoc='" + Tipodoc + "',nrodoc='" + Nrodoc + "',direccion='" + Direccion + "',altura='" + Altura + "',piso='" + Piso + "',depto='" + Depto + "',telef='" + Telef + "',celular='" + Celular + "',email='" + Email + "',fecnacimiento='" + Fecnacimiento + "',estado='" + Estado + "' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        public bool delete()
        {
            string sql = "update actores_x_legajo set estado = 'b' where id=" + Id;
            return u.ejecutarNonSelect(sql);
        }

        private static ActoresLegajo getFila(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = u.ejecutarSelect(sql);
            ActoresLegajo op = new ActoresLegajo();
            if (dt != null)
            {
                op.loadData(dt, 0);
                return op;
            }
            else return null;
        }

        private static ActoresLegajo[] getFilas(string sql)
        {
            Utiles u = new Utiles();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = u.ejecutarSelect(sql);
            ActoresLegajo[] op = null;
            if (dt != null)
            {
                op = new ActoresLegajo[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    op[i] = new ActoresLegajo();
                    op[i].loadData(dt, i);
                }
            }
            return op;
        }

        public static ActoresLegajo obtenerPorId(string id)
        {
            return getFila("select * from actores_x_legajo where id=" + id);
        }

        public static ActoresLegajo[] obtenerTodos()
        {
            return getFilas("select * from actores_x_legajo");
        }

        public static ActoresLegajo[] obtenerTodosPorLegajo(string idLegajo)
        {
            return getFilas("select * from actores_x_legajo where idLegajo = " + idLegajo + " order by estado asc");
        }
    }
}