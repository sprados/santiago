﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Web;

namespace helpers.tpt
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //CONTROLAMOS QUE EL USUARIO SE HAYA LOGUEADO Y QUE LA SESION ESTÉ ABIERTA
            if (Session["sUsuario"] == null)
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~/Inicio.aspx");
                return;
            }
            else Session.Add("errMsg", "");
            //FIN CONTROL DE SESION

            Utiles u = new Utiles();
            u.abrirConexion(Response);
        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            Utiles u = new Utiles();
            u.cerrarConexion();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sUsuario"] != null)
            {
                Usuarios usu = (Usuarios)Session["sUsuario"];
                Utiles u = new Utiles();
                Usuarios usuLogueado = Usuarios.obtenerPorIdUsuario(usu.Id);
                if (usuLogueado != null)
                {
                   // lblSesion.Text = "Sesión: " + usu.Nombre;
                    lblSesion.Text =  usu.Nombre;


                    //si el adm. le asignó nuevos permisos tiene que rearmar el menu principal.
                    if (usuLogueado.Nuevospermisos == "1" && HttpContext.Current.Session["menuPrin"] != null)
                    {
                        HttpContext.Current.Session.Remove("menuPrin"); //limiamos la variable de sesion que arma el menu de la izquiera, para que sea rearmado.
                        usuLogueado.Nuevospermisos = "0";
                        usuLogueado.updateUser();
                        u.showMessage(this, "Atención: El administrador del sistema acaba de modificar sus permisos. El menú principal fue modificado.", 2, 7000);
                    }

                    //si le asignaron nuevas novedades, se le debe rearmar el menú nuevamente para que le aparezcan en el baloon rojo
                    if (usuLogueado.Nuevanovedad == "1")
                    {
                        HttpContext.Current.Session.Remove("menuPrin"); //limiamos la variable de sesion que arma el menu de la izquiera, para que sea rearmado.
                        usuLogueado.updateEsNuevaNovedad("1");
                        u.showMessage(this, "Una nueva Novedad fue generada. El indicador en el menú principal fue modificado.", 0, 7000);
                    }
                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("login.aspx?idAviso=0");
                }

                litMenuPrin.Text = u.armarMenuPrincipal(usu.Id); //ARMA EL MENU PRINCIPAL
            }
            else
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("login.aspx");
                return;
            }
        }

        protected void lnkCerrarSesion_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/Inicio.aspx");
        }
    }
}