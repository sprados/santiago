﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="helpers.tpt.login" %>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Estudio Jurídico</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Ing. Leonardo Tchobanian">

    <!-- The styles -->

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }

        .sidebar-nav {
            padding: 9px 0;
        }
    </style>

    <link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/charisma-app.css" rel="stylesheet">

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">
</head>

<body>



    <div class="container-fluid">
        <div class="row-fluid">

            <div class="row-fluid">
                <div class="span12 center login-header">

                    <h1>Estudio Jurídico</h1>
                    <h2>Sistema Web de Administración y Gestión</h2>

                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <center>
                <asp:Label ID="lblResultadoDerivado" runat="server" Text=""></asp:Label>
            </center>

            <div class="row-fluid">
                <div class="well span5 center login-box">
                    <p>
                        Por favor ingrese su nombre de usuario y contraseña.
                    </p>
                    <br />
                    <form class="form-horizontal" id="form1" runat="server" autocomplete="off">
                        <fieldset>
                            <div class="input-prepend" title="Nombre de Usuario" data-rel="tooltip">
                                <span class="add-on"><i class="icon-user"></i></span>
                                <asp:TextBox ID="txtUsername" class="input-large span10" runat="server" placeholder="Usuario" Text="martin"></asp:TextBox>
                            </div>
                            <div class="clearfix"></div>

                            <div class="input-prepend" title="Password" data-rel="tooltip">
                                <span class="add-on"><i class="icon-lock"></i></span>
                                <asp:TextBox ID="txtPassword" class="input-large span10" runat="server" placeholder="Password" TextMode="Password" Text="1234"></asp:TextBox>
                            </div>
                            <div class="clearfix"></div>

                            <p class="center span5">
                                <asp:Button ID="lnkIniciarSesion" runat="server" Text="Ingresar"
                                    class="btn btn-primary" OnClick="lnkIniciarSesion_Click" />
                            </p>

                            <%--<p class="center span5">
                                <asp:Button ID="lnkPassword" runat="server" Text="Cambiar Contraseña"
                                    class="btn btn-info" OnClick="lnkCambPass_Click" />
                            </p>--%>
                        </fieldset>
                    </form>
                </div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span12 center" style="font-size: 8pt">
                    <p>
                        © Todos los derechos reservados | Sitio desarrollado por <a href="mailto:martintchobanian@gmail.com?Subject=Sitio Web"
                            target="_blank"><b>Sistemas Web Blunian.</b></a>
                    </p>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
        </div>
    </div>

    <!--
	<script src="js/jquery-1.7.2.min.js"></script>
	<script src="js/charisma.js"></script>
		-->
</body>
</html>
