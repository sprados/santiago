﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Text;
using System.Web.UI;

namespace Estudio_Juridico.Adm
{
    public partial class ABMFueros : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Permisos.verificarPermiso(Page);
                if (Request.QueryString["mId"] == null && Request.QueryString["eId"] == null)
                {
                    //ESTA QUERIENDO VER EL LISTADO
                    Permisos.verificarPermisoConsulta(Page);
                    llenarGrilla();
                    pnlVerListado.Visible = true;
                }
                else if (Request.QueryString["mId"] != null)
                {
                    //ESTA QUERIENDO MODIFICAR
                    Permisos.verificarPermisoModificar(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
                    Fueros fue = Fueros.obtenerPorId(idModif);
                    if (fue != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = false;
                        pnlModificar.Visible = true;
                        pnlVerListado.Visible = false;
                        txtModFuero.Text = fue.Nombre;
                        u.seleccionarDDLSegunIDValue(ddlModEstado, fue.Estado);
                    }
                    else
                    {
                        u.showMessage(this, "Error: no se encuentra el fuero.", 1);
                        return;
                    }
                }
                else if (Request.QueryString["eId"] != null)
                {
                    //ESTA QUERIENDO ELIMINAR
                    Permisos.verificarPermisoBaja(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
                    Fueros fue = Fueros.obtenerPorId(idModif);
                    if (fue != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = true;
                        pnlModificar.Visible = false;
                        pnlVerListado.Visible = false;
                        u.labelResultado(lblEliminar, "¿Está seguro que desea eliminar el fuero " + fue.Nombre + "?", 3, "Atención!");
                        u.agregarModal(ref btnElimOK, "Confirmar", "¿Está seguro que desea eliminar el fuero " + fue.Nombre + "?", Page);
                    }
                }
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            pnlAgregar.Visible = true;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = false;
            lblResultado.Visible = false;

            txtAddFuero.Text = "";
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABMFueros.aspx");
        }

        protected void btnAgregarGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);

            string fue = u.sqlSeguro(txtAddFuero.Text.Trim());

            if (fue.Length > 100)
            {
                u.labelResultado(lblResultado, "El fuero no puede tener más de 100 caracteres.", 1, "Error!");
                return;

            }
            else { 
            
            Fueros fexistente = new Fueros();
            fexistente = Fueros.obtenerPorNombre(fue);
            if (fexistente != null) {
                u.labelResultado(lblResultado, "Ya Existe un Fuero Con este Nombre", 1, "Error!");
                return;
                }
            }

            Fueros f = new Fueros();
            f.Nombre = fue;
            f.guardar();

            u.labelResultado(lblResultado, "Fuero agregado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnModGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoModificar(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
            Fueros f = Fueros.obtenerPorId(idModif);
            if (f == null)
            {
                u.showMessage(this, "Error: no se encuentra el fuero.", 1);
                return;
            }

            string fue = u.sqlSeguro(txtModFuero.Text.Trim());

            if (fue.Length > 100)
            {
                u.labelResultado(lblResultado, "El fuero no puede tener más de 100 caracteres.", 1, "Error!");
                return;
            }

            f.Nombre = fue;
            f.Estado = ddlModEstado.SelectedValue;
            f.update();

            u.labelResultado(lblResultado, "Fuero modificado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnElimOK_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoBaja(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
            Fueros fue = Fueros.obtenerPorId(idModif);
            if (fue == null)
            {
                u.showMessage(this, "Error: no se encuentra el fuero.", 1);
                return;
            }

            fue.delete();
            u.labelResultado(lblResultado, "Fuero eliminado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        private void llenarGrilla()
        {
            Fueros[] fue = Fueros.obtenerTodos();
            StringBuilder s = new StringBuilder();

            if (fue != null)
            {
                foreach (Fueros x in fue)
                {
                    //LOS CARGAMOS EN LA GRILLA.
                    string idEncrip = u.encriptarTexto(x.IdFueros);
                    s.Append("<tr>");
                    s.Append("<td class='center'>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-info' href='ABMFueros.aspx?mId=" + idEncrip + "' data-rel='tooltip' data-title='Modificar Fuero...'><i class='icon-edit icon-white'></i></a>" +
                        "<a class='btn btn-mini btn-danger' href='ABMFueros.aspx?eId=" + idEncrip + "' data-rel='tooltip' data-title='Eliminar Fuero...'><i class='icon-trash icon-white'></i></a></td>");
                    s.Append("<td>" + x.Nombre + "</td>");
                    s.Append("<td class='monedaTD'>" + (x.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                    s.Append("</tr>");

                }
                litGrilla.Text = s.ToString();
            }
        }
    }
}