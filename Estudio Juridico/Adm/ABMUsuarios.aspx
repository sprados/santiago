﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="ABMUsuarios.aspx.cs" Inherits="CSMMonitor.WebForm10" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function openWin(irUrl) {
            window.open(irUrl, "_blank", "width=700,height=550,left=100,top=100,menubar=no,scrollbars=yes");
            return false;
        }
    </script>
    <%--EXPORT 2 EXCEL--%>
    <script type="text/javascript" src="js/jquery.table2excel.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#lnkToExcel").click(function () {
                $("#tab1").table2excel({
                    exclude: ".noExl",
                    name: "Excel Document Name"
                });
            });
        });
	</script>
    <%--FIN EXPORT 2 EXCEL--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div>
        <ul class="breadcrumb">
            <li>
                <h3>USUAROIS DEL SISTEMA
                </h3>
            </li>
            <li>
                <p>
                    Utilice el presente formulario para consultar los usuarios del sistema. También podrá dar de alta, de baja o modificar los datos de los mismos.
                </p>
            </li>
        </ul>
    </div>


    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>

    <asp:Panel class="row-fluid" runat="server" ID="pnlVerListado" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;LISTADO DE USUARIOS</h2>
                    <div class="box-icon">
                        <a id="lnkToExcel" class="btn btn-success" style="width: 40px">Excel</a>
                    </div>
                </div>
                <div class="box-content">
                    <table id="tab1" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 100px">Acciones</th>
                                <th>Username (Login)</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th style="text-align: right">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal ID="litGrilla" runat="server"></asp:Literal>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnNuevo" runat="server" Text="Registrar Nuevo Usuario" OnClick="btnNuevo_Click" class="btn btn-info" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnSalir1" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>



    <asp:Panel class="row-fluid" runat="server" ID="pnlAgregar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;AGREGAR UN USUARIO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblAdd" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span7">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Username (Login):</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddLogin" runat="server" MaxLength="45" class="input-xlarge"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Password:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddPass1" runat="server" MaxLength="45" class="input-xlarge" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Repetir Password:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddPass2" runat="server" MaxLength="45" class="input-xlarge" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Nombre:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddNombre" runat="server" MaxLength="80" class="input-xlarge"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Email (Opcional):</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddEmail" runat="server" MaxLength="80" class="input-xlarge"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Tipo Usuario:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAddTipo" runat="server">
                                        <asp:ListItem Value="A">Administrador</asp:ListItem>
                                        <asp:ListItem Value="O">Operador</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                &nbsp;<div class="controls">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnAgregarGuardar" runat="server" Text="Guardar Usuario" class="btn btn-primary" OnClick="btnAgregarGuardar_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnAgregarSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>



    <asp:Panel class="row-fluid" runat="server" ID="pnlModificar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;MODIFICAR UN USUARIO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblMod" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span7">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Username (Login):</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModLogin" runat="server" MaxLength="45" class="input-xlarge"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Password (dejar vacío si no se quiere modificar):</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModPass1" runat="server" MaxLength="45" class="input-xlarge" TextMode="Password"
                                        data-rel="popover" data-title="Password" data-content="Dejar este campo vacío si NO se quiere modificar la password del usuario."></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Repetir Password:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModPass2" runat="server" MaxLength="45" class="input-xlarge" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Nombre:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModNombre" runat="server" MaxLength="80" class="input-xlarge"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Email (Opcional):</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModEmail" runat="server" MaxLength="80" class="input-xlarge"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Tipo Usuario:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlModTipo" runat="server">
                                        <asp:ListItem Value="A">Administrador</asp:ListItem>
                                        <asp:ListItem Value="O">Operador</asp:ListItem>
                                        <asp:ListItem  Selected="True" Value="T">Técnico</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Estado:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlModEstado" runat="server">
                                        <asp:ListItem Selected="True" Value="a">Alta</asp:ListItem>
                                        <asp:ListItem Value="b">Baja</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnModGuardar" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnModGuardar_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnModSalir" runat="server" Text="Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel class="row-fluid" runat="server" ID="pnlEliminar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;ELIMINAR UN USUARIO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblElim" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <asp:Label ID="lblEliminar" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnElimOK" runat="server" Text="Eliminar Usuario" class="btn btn-primary" OnClick="btnElimOK_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnElimSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>
