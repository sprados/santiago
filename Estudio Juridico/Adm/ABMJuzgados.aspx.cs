﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Estudio_Juridico.Adm
{
    public partial class ABMJuzgados : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Permisos.verificarPermiso(Page);
                if (Request.QueryString["mId"] == null && Request.QueryString["eId"] == null)
                {
                    //ESTA QUERIENDO VER EL LISTADO
                    Permisos.verificarPermisoConsulta(Page);
                    llenarGrilla();
                    pnlVerListado.Visible = true;
                }
                else if (Request.QueryString["mId"] != null)
                {
                    //ESTA QUERIENDO MODIFICAR
                    Permisos.verificarPermisoModificar(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
                    Juzgados juz = Juzgados.obtenerPorId(idModif);
                    Fueros.llenarDDL(ddlModFuero, false);

                    if (juz != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = false;
                        pnlModificar.Visible = true;
                        pnlVerListado.Visible = false;
                        txtModJuzgado.Text = juz.Juzgado;
                        u.seleccionarDDLSegunIDValue(ddlModFuero, juz.IdFueros);
                        u.seleccionarDDLSegunIDValue(ddlModEstado, juz.Estado);
                    }
                    else
                    {
                        u.showMessage(this, "Error: no se encuentra el juzgado.", 1);
                        return;
                    }
                }
                else if (Request.QueryString["eId"] != null)
                {
                    //ESTA QUERIENDO ELIMINAR
                    Permisos.verificarPermisoBaja(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
                    Juzgados juz = Juzgados.obtenerPorId(idModif);
                    if (juz != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = true;
                        pnlModificar.Visible = false;
                        pnlVerListado.Visible = false;
                        u.labelResultado(lblEliminar, "¿Está seguro que desea eliminar el juzgado " + juz.Juzgado + "?", 3, "Atención!");
                        u.agregarModal(ref btnElimOK, "Confirmar", "¿Está seguro que desea eliminar el juzgado " + juz.Juzgado + "?", Page);
                    }
                }
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            pnlAgregar.Visible = true;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = false;
            lblResultado.Visible = false;

            Fueros.llenarDDL(ddlAddFuero, false);
            txtAddJuzgado.Text = "";
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABMJuzgados.aspx");
        }

        protected void btnAgregarGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);

            string juz = u.sqlSeguro(txtAddJuzgado.Text.Trim());

            if (juz.Length > 100)
            {
                u.labelResultado(lblResultado, "El Juzgado no puede tener más de 100 caracteres.", 1, "Error!");
                return;
            }

            Juzgados f = new Juzgados();
            f.Juzgado = juz;
            f.IdFueros = ddlAddFuero.SelectedValue;
            f.guardar();

            u.labelResultado(lblResultado, "Juzgado agregado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnModGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoModificar(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
            Juzgados f = Juzgados.obtenerPorId(idModif);
            if (f == null)
            {
                u.showMessage(this, "Error: no se encuentra el juzgado.", 1);
                return;
            }

            string juz = u.sqlSeguro(txtModJuzgado.Text.Trim());

            if (juz.Length > 100)
            {
                u.labelResultado(lblResultado, "El juzgado no puede tener más de 100 caracteres.", 1, "Error!");
                return;
            }

            f.Juzgado = juz;
            f.IdFueros = ddlModFuero.SelectedValue;
            f.Estado = ddlModEstado.SelectedValue;
            f.update();

            u.labelResultado(lblResultado, "juzgado modificado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnElimOK_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoBaja(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
            Juzgados juz = Juzgados.obtenerPorId(idModif);
            if (juz == null)
            {
                u.showMessage(this, "Error: no se encuentra el juzgado.", 1);
                return;
            }

            juz.delete();

            u.labelResultado(lblResultado, "juzgado eliminado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        private void llenarGrilla()
        {
            Juzgados[] juz = Juzgados.obtenerTodos();
            StringBuilder s = new StringBuilder();

            if (juz != null)
            {
                foreach (Juzgados x in juz)
                {
                    //LOS CARGAMOS EN LA GRILLA.
                    string idEncrip = u.encriptarTexto(x.IdJuzgados);
                    s.Append("<tr>");
                    s.Append("<td class='center'>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-info' href='ABMJuzgados.aspx?mId=" + idEncrip + "' data-rel='tooltip' data-title='Modificar Juzgado...'><i class='icon-edit icon-white'></i></a>" +
                        "<a class='btn btn-mini btn-danger' href='ABMJuzgados.aspx?eId=" + idEncrip + "' data-rel='tooltip' data-title='Eliminar Juzgado...'><i class='icon-trash icon-white'></i></a></td>");
                    s.Append("<td>" + Fueros.obtenerPorId(x.IdFueros).Nombre + "</td>");
                    s.Append("<td>" + x.Juzgado + "</td>");
                    s.Append("<td class='monedaTD'>" + (x.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                    s.Append("</tr>");

                }
                litGrilla.Text = s.ToString();
            }
        }
    }
}