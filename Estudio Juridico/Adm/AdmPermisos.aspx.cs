﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace helpers
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Usuarios usu = (Usuarios)Session["sUsuario"];
                if (usu.Tipo != "A")
                {
                    Session.Add("avisos", "Ud. no tiene permisos para ingresar a esta página.");
                    Response.Redirect("avisos.aspx");
                    return;
                }

                Usuarios[] u = Usuarios.obtenerTodos();
                ddlUsuario.Items.Add("");
                if (u != null)
                {
                    foreach (Usuarios usr in u)
                    {
                        ddlUsuario.Items.Add(usr.Nombre);
                        ddlUsuario.Items[ddlUsuario.Items.Count - 1].Value = usr.Id;
                    }
                }
            }
        }

        protected void ddlUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlUsuario.Items[0].Text == "") ddlUsuario.Items.RemoveAt(0);
            llenarGrilla();
        }

        private void llenarGrilla()
        {
            grid.DataSource = null;
            grid.DataBind();

            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("PosicionPorDefecto");
            dt.Columns.Add("AdminSiempre");
            dt.Columns.Add("Menu");

            Menues[] menues = Menues.obtenerTodosHijos();
            if (menues != null)
            {
                foreach (Menues m in menues)
                {
                    dt.Rows.Add();
                    dt.Rows[dt.Rows.Count - 1]["Id"] = m.Id;
                    dt.Rows[dt.Rows.Count - 1]["PosicionPorDefecto"] = m.PosicionPorDefecto;
                    dt.Rows[dt.Rows.Count - 1]["AdminSiempre"] = (m.SiempreVisibleAdmin ? "1" : "0");
                    dt.Rows[dt.Rows.Count - 1]["Menu"] = Menues.obtenerPorId(m.IdPadre).Nombre + " / " + m.Nombre + " " + (m.SiempreVisibleAdmin ? "*" : "");
                }

                grid.DataSource = dt;
                grid.DataBind();

                foreach (GridViewRow row in grid.Rows)
                {
                    CheckBox chkAlta = (CheckBox)row.FindControl("chkAlta");
                    CheckBox chkBaja = (CheckBox)row.FindControl("chkBaja");
                    CheckBox chkModif = (CheckBox)row.FindControl("chkModif");
                    CheckBox chkConsulta = (CheckBox)row.FindControl("chkConsulta");
                    Label lblId = (Label)row.FindControl("lblId");
                    string idMenu = u.sqlSeguro(lblId.Text.Trim());
                    chkAlta.Checked = false;
                    chkBaja.Checked = false;
                    chkModif.Checked = false;
                    chkConsulta.Checked = false;

                    //Traemos de la BD los permisos para este usuario y para este menu.
                    Permisos permisos = Permisos.obtenerPermiso(idMenu, ddlUsuario.SelectedValue);
                    if (permisos != null)
                    {
                        if (permisos.Alta) chkAlta.Checked = true;
                        if (permisos.Baja) chkBaja.Checked = true;
                        if (permisos.Modif) chkModif.Checked = true;
                        if (permisos.Consulta) chkConsulta.Checked = true;
                    }
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (ddlUsuario.SelectedValue == "")
            {
                u.showMessage(this, "Error: por favor seleccione un usuario.", 1);
                return;
            }
            Permisos menu = new Permisos();
            menu.IdUsuario = u.sqlSeguro(ddlUsuario.SelectedValue.Trim());
            menu.DeleteTodosDeUsuario(); //borramos todos los permisos anteriores del usuario para asignarle nuevos.
            Usuarios usu = Usuarios.obtenerPorIdUsuario(ddlUsuario.SelectedValue);
            if (usu != null)
            {
                foreach (GridViewRow row in grid.Rows)
                {
                    CheckBox chkAlta = (CheckBox)row.FindControl("chkAlta");
                    CheckBox chkBaja = (CheckBox)row.FindControl("chkBaja");
                    CheckBox chkModif = (CheckBox)row.FindControl("chkModif");
                    CheckBox chkConsulta = (CheckBox)row.FindControl("chkConsulta");
                    Label lblId = (Label)row.FindControl("lblId");
                    Label lblPosicionPorDefecto = (Label)row.FindControl("lblPosicionPorDefecto");
                    Label lblAdminSiempreVisible = (Label)row.FindControl("lblAdm");
                    string idMenu = u.sqlSeguro(lblId.Text.Trim());
                    bool tieneAlta = false;
                    bool tieneBaja = false;
                    bool tieneModif = false;
                    bool tieneCons = false;
                    if (chkAlta.Checked) tieneAlta = true;
                    if (chkBaja.Checked) tieneBaja = true;
                    if (chkModif.Checked) tieneModif = true;
                    if (chkConsulta.Checked) tieneCons = true;

                    if (!tieneAlta && !tieneBaja && !tieneModif && !tieneCons)
                        continue;

                    //ACTUALIZAMOS LA BD DE PERMISOS DE ESTE USUARIO.
                    Permisos perm = new Permisos();
                    perm.IdMenu = idMenu;
                    perm.IdUsuario = menu.IdUsuario;
                    perm.Visible = true;
                    perm.Posicion = u.sqlSeguro(lblPosicionPorDefecto.Text.Trim());
                    perm.Alta = tieneAlta;
                    perm.Baja = tieneBaja;
                    perm.Modif = tieneModif;
                    perm.Consulta = tieneCons;
                    perm.guardar();
                }

                u.showMessage(this, "Permisos Actualizados con Éxito", 0);
                if (ddlUsuario.SelectedValue == usu.Id)
                {
                    Session.Add("menuPrin", "");
                    Literal lit = (Literal)this.Master.FindControl("litMenuPrin");
                    lit.Text = u.armarMenuPrincipal(usu.Id);
                }
                usu.Nuevospermisos = "1"; //nuevos permisos
                usu.updateUser();
            }
            else
            {
                u.showMessage(this, "ERROR: No se encontró el usuario solicitado.", 1);
                return;
            }
        }

        protected void chkTodoAlta_CheckedChanged(object sender, EventArgs e)
        {
            tildarTodo("chkAlta", sender);
        }

        protected void chkTodoBaja_CheckedChanged(object sender, EventArgs e)
        {
            tildarTodo("chkBaja", sender);
        }

        protected void chkTodoModif_CheckedChanged(object sender, EventArgs e)
        {
            tildarTodo("chkModif", sender);
        }

        protected void chkTodoConsulta_CheckedChanged(object sender, EventArgs e)
        {
            tildarTodo("chkConsulta", sender);
        }

        private void tildarTodo(string idCheckbox, Object sender)
        {
            CheckBox chk = (CheckBox)sender;
            bool tildarTodo = false;
            if (chk.Checked)
                tildarTodo = true;
            foreach (GridViewRow row in grid.Rows)
            {
                CheckBox check = (CheckBox)row.FindControl(idCheckbox);
                check.Checked = tildarTodo;
            }
        }




        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Inicio.aspx");
        }


    }
}