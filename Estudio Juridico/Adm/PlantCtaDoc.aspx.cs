﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Estudio_Juridico.Adm
{
    public partial class PlantCtaDoc : System.Web.UI.Page
    {
        Utiles u = new Utiles();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            string rutaTemplate = Server.MapPath("/adj/TemplatesDoc/");
            
            if (!Page.IsPostBack) {
                //Llena el combo de Clientes 
                DropDownListClientes.Items.Clear();
                Clientes.llenarDDL(DropDownListClientes, false);

                //Llena el combo de tipo de Documentos 

                TemplateDoc.llenarDDL(DropDownListTipoPlantilla, rutaTemplate, true);
                
                /*
                DropDownListTipoPlantilla.Items.Clear();
                DropDownListTipoPlantilla.Items.Add("");
                DropDownListTipoPlantilla.Items[0].Value = "";
                DropDownListTipoPlantilla.Items.Add("IntimacionDePago");
                DropDownListTipoPlantilla.Items[1].Value = "IntimacionDePago.docx";
                DropDownListTipoPlantilla.Items.Add("Declaratoria de Herederos");
                DropDownListTipoPlantilla.Items[2].Value = "IntimacionDePago.docx";
                DropDownListTipoPlantilla.Items.Add("Presentacion de Jubilacion");
                DropDownListTipoPlantilla.Items[3].Value = "IntimacionDePago.docx";
               */
            }

        }

        protected void btnGenerarDoc_Click(object sender, EventArgs e)
        {
            string idcli = DropDownListClientes.SelectedValue;
            Clientes cli = Clientes.obtenerPorId(idcli);

            string rutaTemplate = Server.MapPath("/adj/TemplatesDoc/");
            string nombreTemplate = DropDownListTipoPlantilla.SelectedValue;
            //string destinoPlantilla = FolderDestini.

            TemplateDoc template = new TemplateDoc();
            
            try {
                template.loadTemplate(cli, rutaTemplate, nombreTemplate);
                u.labelResultado(lblResultado, "Documento Generado con Exito.", 0, "Éxito!");
                System.Diagnostics.Process.Start(@"C:\\Plantillas\\"+ cli.Apellido + "-" + nombreTemplate);

            }
            catch (Exception ex) {
                u.labelResultado(lblResultado,ex.Message, 1, "Error!");
            }

        }


    }
    
}