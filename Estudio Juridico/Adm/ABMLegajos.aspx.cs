﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Estudio_Juridico.Adm
{
    public partial class ABMLegajos : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Permisos.verificarPermiso(Page);
                if (Request.QueryString["mId"] == null && Request.QueryString["eId"] == null)
                {
                    //ESTA QUERIENDO VER EL LISTADO
                    Permisos.verificarPermisoConsulta(Page);
                    llenarGrilla();
                    pnlVerListado.Visible = true;
                }
                else if (Request.QueryString["mId"] != null)
                {
                    //ESTA QUERIENDO MODIFICAR
                    Permisos.verificarPermisoModificar(Page);

                    string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
                    Legajos leg = Legajos.obtenerPorId(idModif);
                    if (leg != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = false;
                        pnlModificar.Visible = true;
                        pnlVerListado.Visible = false;

                        //si viene con el parámetro fId cargado, es porque quiere eliminar un archivo adjunto
                        if (!String.IsNullOrEmpty(Request.QueryString["fId"]))//está quriendo eliminar un archivo adjunto (envía por parámetro la ruta física del archivo)
                        {
                            AdjuntosLegajo axp = new AdjuntosLegajo();
                            string fileName = u.desencriptarTexto(Request.QueryString["fn"].ToString());
                            axp.Id = u.desencriptarTexto(Request.QueryString["fId"].ToString());
                            axp.IdLegajo = idModif;

                            axp.DeleteAdjuntoPorId();//eliminamos en la BD el registro

                            //también tenemos que borrar del servidor el archivo físico para que libere espacio
                            {
                                FileInfo fi = new FileInfo(Server.MapPath(fileName));
                                if (fi.Exists)
                                {
                                    try
                                    {
                                        fi.Delete();
                                        u.labelResultado(lblResultado, "Archivo adjunto eliminado correctamente.", 0, "Éxito!");
                                    }
                                    catch { u.labelResultado(lblResultado, "No se pudo eliminar el archivo adjunto.", 1, "Error!"); }
                                }
                            }
                        }

                        //si viene con el parámetro aId cargado, es porque quiere eliminar un actor
                        if (!String.IsNullOrEmpty(Request.QueryString["aId"]))//está quriendo eliminar un actor
                        {
                            ActoresLegajo axl = new ActoresLegajo();
                            axl.Id = u.desencriptarTexto(Request.QueryString["aId"].ToString());

                            try
                            {
                                axl.delete();
                                u.labelResultado(lblResultado, "Actor eliminado correctamente.", 0, "Éxito!");
                            }
                            catch
                            {
                                u.labelResultado(lblResultado, "No se pudo eliminar el actor.", 1, "Error!");
                            }
                        }

                        lblLegajo.Text = leg.Descripcion;

                        //GENERAL
                        Clientes.llenarDDL(ddlAddClientes, false);
                        txtModDescripcion.Text = leg.Descripcion;
                        txtModFecha.Text = leg.Fecha;
                        txtModCodigo.Text = leg.Codigo;
                        ddlModEstadoLeg.SelectedIndex = (leg.Estado == "a" ? 0 : 1);

                        //CLIENTE
                        Clientes cli = Clientes.obtenerPorId(leg.IdCliente);
                        if (cli != null)
                        {
                            txtModNombre.Text = cli.Nombre;
                            txtModApellido.Text = cli.Apellido;
                            txtModTelefono.Text = cli.Telef;
                            txtModCel.Text = cli.Celular;
                            txtModEmail.Text = cli.Email;
                            txtModDireccion.Text = cli.Direccion;
                            txtModAltura.Text = cli.Altura;
                            txtModPiso.Text = cli.Piso;
                            txtModDepto.Text = cli.Depto;
                            ddlModEstado.SelectedIndex = (cli.Estado == "a" ? 0 : 1);
                            u.seleccionarDDLSegunIDValue(ddlModTipoDoc, cli.Tipodoc);
                            txtModNroDoc.Text = cli.Nrodoc;
                            txtModRazSoc.Text = cli.Razsoc;
                            u.seleccionarDDLSegunIDValue(ddlModTipoIVA, cli.Tipoiva);
                            txtModFecNac.Text = cli.Fecnacimiento;
                        }

                        //HISTORIAL
                        HistorialLegajo[] historial = HistorialLegajo.obtenerTodosPorIdLegajo(idModif);
                        StringBuilder h = new StringBuilder();
                        litHistorial.Text = "";

                        if (historial != null)
                        {

                            foreach (HistorialLegajo hl in historial)
                            {
                                h.Append("<tr>");
                                h.Append("<td>" + u.getFechaToMMDDYYYY(hl.Fecha) + "</td>"); //concepto
                                h.Append("<td>" + hl.Historial + "</td>"); //sucursal que cobró
                                h.Append("</tr>");
                            }

                        }
                        litHistorial.Text = h.ToString();


                        //ACTORES
                        ActoresLegajo[] actores = ActoresLegajo.obtenerTodosPorLegajo(idModif);
                        StringBuilder a = new StringBuilder();
                        litActores.Text = "";
                        string tipoDoc = "";

                        if (actores != null)
                        {
                            foreach (ActoresLegajo al in actores)
                            {
                                switch (al.Tipodoc)
                                {
                                    case "80":
                                        tipoDoc = "CUIT";
                                        break;
                                    case "86":
                                        tipoDoc = "CUIL";
                                        break;
                                    case "96":
                                        tipoDoc = "DNI";
                                        break;
                                    default:
                                        tipoDoc = "Sin Datos";
                                        break;
                                }

                                //LOS CARGAMOS EN LA GRILLA.
                                a.Append("<tr>");
                                a.Append("<td class='center'>" +
                                    "<a class='btn btn-mini btn-danger' href='ABMLegajos.aspx?mId=" + u.encriptarTexto(leg.Id) + "&aId=" + u.encriptarTexto(al.Id) + "' data-rel='tooltip' data-title='Eliminar Actor...'><i class='icon-trash icon-white'></i></a></td>");
                                a.Append("<td>" + al.Apellido + " " + al.Nombre + "</td>");
                                a.Append("<td>" + al.Direccion + " " + al.Altura + " (Piso " + al.Piso + " Depto " + al.Depto + ")</td>");
                                a.Append("<td>" + al.Telef + " / " + al.Celular + "</td>");
                                a.Append("<td>" + al.Email + "</td>");
                                if (!String.IsNullOrEmpty(al.Tipodoc) && !String.IsNullOrEmpty(al.Nrodoc))
                                    a.Append("<td>(" + tipoDoc + ") - " + al.Nrodoc + "</td>");
                                else
                                    a.Append("<td>" + tipoDoc + "</td>");
                                a.Append("<td>" + (al.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                                a.Append("</tr>");
                            }

                            litActores.Text = a.ToString();
                        }

                        //DOCUMENTACIÓN
                        cargarAdjuntos(leg.Id);


                        //APORTES
                        PagosLegajo[] pagos = PagosLegajo.obtenerTodosPorIdLegajo(idModif);
                        StringBuilder p = new StringBuilder();
                        litPagos.Text = "";

                        if (pagos != null)
                        {
                            foreach (PagosLegajo pl in pagos)
                            {
                                p.Append("<tr>");
                                p.Append("<td>" + pl.Fecha + "</td>");
                                p.Append("<td>" + pl.Moneda + "</td>");
                                p.Append("<td>" + pl.Monto + "</td>");
                                p.Append("<td>" + pl.Observaciones + "</td>");
                                p.Append("</tr>");
                            }
                        }
                        litPagos.Text = p.ToString();
                    }
                    else
                    {
                        u.showMessage(this, "Error: no se encuentra el Legajo.", 1);
                        return;
                    }
                }
                else if (Request.QueryString["eId"] != null)
                {
                    //ESTA QUERIENDO ELIMINAR
                    Permisos.verificarPermisoBaja(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
                    Legajos leg = Legajos.obtenerPorId(idModif);
                    if (leg != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = true;
                        pnlModificar.Visible = false;
                        pnlVerListado.Visible = false;
                        u.labelResultado(lblEliminar, "¿Está seguro que desea eliminar el legajo " + leg.Descripcion + "?", 3, "Atención!");
                        u.agregarModal(ref btnElimOK, "Confirmar", "¿Está seguro que desea eliminar el legajo " + leg.Descripcion + "?", Page);
                    }
                }
            }
        }

        private void llenarGrilla()
        {
            Legajos[] legajos = Legajos.obtenerTodos();
            StringBuilder s = new StringBuilder();

            if (legajos != null)
            {
                foreach (Legajos l in legajos)
                {
                    //las siguientes líneas verifican en el Servidor si la propiedad tiene una carpeta creada con su número. En caso de no tenerla, la crea.
                    string carpeta = "/adj/legajos/" + l.Id;

                    //primero verificamos que si existe una carpeta con el mismo ID de propiedad
                    bool existe = System.IO.Directory.Exists(Server.MapPath(carpeta));
                    if (!existe)
                    {
                        System.IO.Directory.CreateDirectory(Server.MapPath(carpeta));
                    }

                    //LOS CARGAMOS EN LA GRILLA.
                    string idEncrip = u.encriptarTexto(l.Id);
                    Clientes cli = Clientes.obtenerPorId(l.IdCliente);

                    s.Append("<tr>");
                    s.Append("<td class='center'>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-inverse' href='ABMLegajos.aspx?mId=" + idEncrip + "' data-rel='tooltip' data-title='Ingresar al Legajo...'><i class='icon-color icon-folder-open'></i></a>" +
                        "<a class='btn btn-mini btn-danger' href='ABMLegajos.aspx?eId=" + idEncrip + "' data-rel='tooltip' data-title='Eliminar Legajo...'><i class='icon-trash icon-white'></i></a></td>");

                    s.Append("<td>" + l.Descripcion + "</td>");
                    s.Append("<td>" + cli.Apellido + " " + cli.Nombre + "</td>");
                    s.Append("<td>" + l.Fecha + "</td>");
                    s.Append("<td>" + l.Codigo + "</td>");
                    s.Append("<td>" + (l.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                    s.Append("</tr>");
                }

                litGrilla.Text = s.ToString();

            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            pnlAgregar.Visible = true;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = false;
            lblResultado.Visible = false;

            txtAddDescripcion.Text = "";
            txtAddFecha.Text = "";
            txtAddCodigo.Text = "";
            Clientes.llenarDDL(ddlAddClientes, true);
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABMLegajos.aspx");
        }

        protected void btnAgregarGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);

            //SOLAPA GENERAL
            Legajos leg = new Legajos();
            string des = u.sqlSeguro(txtAddDescripcion.Text.Trim());
            string cod = u.sqlSeguro(txtAddCodigo.Text.Trim());
            string fec = u.sqlSeguro(txtAddFecha.Text.Trim());
            string cli = ddlAddClientes.SelectedValue;

            if (des.Length > 150)
            {
                u.labelResultado(lblResultado, "La Descripción del legajo no puede tener más de 150 caracteres.", 1, "Error!");
                return;
            }
            if (cod.Length > 50)
            {
                u.labelResultado(lblResultado, "El Código del expediente no puede tener más de 50 caracteres.", 1, "Error!");
                return;
            }
            if (fec.Length > 10)
            {
                u.labelResultado(lblResultado, "La Fecha no puede tener más de 10 caracteres.", 1, "Error!");
                return;
            }

            leg.Codigo = cod;
            leg.Descripcion = des;
            leg.Fecha = fec;
            leg.IdCliente = cli;

            leg.guardar();


            u.labelResultado(lblResultado, "Legajo agregado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnModGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoModificar(Page);

            string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());

            Legajos leg = Legajos.obtenerPorId(idModif);
            if (leg == null)
            {
                u.showMessage(this, "Error: no se encuentra el legajo.", 1);
                return;
            }

            //SOLAPA GENERAL
            string des = u.sqlSeguro(txtModDescripcion.Text.Trim());
            string cod = u.sqlSeguro(txtModCodigo.Text.Trim());
            string fec = u.sqlSeguro(txtModFecha.Text.Trim());
            string estLeg = ddlModEstadoLeg.SelectedValue;

            if (des.Length > 150)
            {
                u.labelResultado(lblResultado, "La Descripción del legajo no puede tener más de 150 caracteres.", 1, "Error!");
                return;
            }
            if (cod.Length > 50)
            {
                u.labelResultado(lblResultado, "El Código del expediente no puede tener más de 50 caracteres.", 1, "Error!");
                return;
            }
            if (fec.Length > 10)
            {
                u.labelResultado(lblResultado, "La Fecha no puede tener más de 10 caracteres.", 1, "Error!");
                return;
            }
            leg.Codigo = cod;
            leg.Descripcion = des;
            leg.Fecha = fec;
            leg.Estado = estLeg;

            leg.update();


            //SOLAPA CLIENTE
            Clientes cli = Clientes.obtenerPorId(leg.IdCliente);
            if (cli == null)
            {
                u.showMessage(this, "Error: no se encuentra el cliente.", 1);
                return;
            }

            string nom = u.sqlSeguro(txtModNombre.Text.Trim());
            string ape = u.sqlSeguro(txtModApellido.Text.Trim().ToUpper());
            string tel = u.sqlSeguro(txtModTelefono.Text.Trim());
            string cel = u.sqlSeguro(txtModCel.Text.Trim());
            string ema = u.sqlSeguro(txtModEmail.Text.Trim());
            string dir = u.sqlSeguro(txtModDireccion.Text.Trim());
            string alt = u.sqlSeguro(txtModAltura.Text.Trim());
            string pis = u.sqlSeguro(txtModPiso.Text.Trim());
            string dep = u.sqlSeguro(txtModDepto.Text.Trim());
            string est = ddlModEstado.SelectedValue;
            string nroDoc = u.sqlSeguro(txtModNroDoc.Text.Trim());
            string raz = u.sqlSeguro(txtModRazSoc.Text.Trim());

            if (nom.Length > 80)
            {
                u.labelResultado(lblResultado, "El nombre del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (ape.Length > 80)
            {
                u.labelResultado(lblResultado, "El Apellido del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (tel.Length > 45)
            {
                u.labelResultado(lblResultado, "El teléfono del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (cel.Length > 45)
            {
                u.labelResultado(lblResultado, "El Celular del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (ema.Length > 45)
            {
                u.labelResultado(lblResultado, "El Email del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (dir.Length > 80)
            {
                u.labelResultado(lblResultado, "La Dirección del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (alt.Length > 6)
            {
                u.labelResultado(lblResultado, "La Altura del cliente no puede tener más de 6 caracteres.", 1, "Error!");
                return;
            }
            if (pis.Length > 3)
            {
                u.labelResultado(lblResultado, "El Piso del cliente no puede tener más de 3 caracteres.", 1, "Error!");
                return;
            }
            if (dep.Length > 2)
            {
                u.labelResultado(lblResultado, "El Depto del cliente no puede tener más de 2 caracteres.", 1, "Error!");
                return;
            }
            if (nroDoc.Length > 0)
            {
                if (!u.EsNumerico(nroDoc, true))
                {
                    u.labelResultado(lblResultado, "El Nro. de Documento del Cliente no es un dato numérico, sin guiones ni puntos.", 1, "Error!");
                    return;
                }
                else //validamos que el nro. de doc. no exista previamente en BD con otro cliente
                {
                    Clientes cliExistente = new Clientes();
                    cliExistente = Clientes.obtenerPorDNI(nroDoc);
                    if (cliExistente != null)
                    {
                        if (cliExistente.Id != cli.Id)
                        {
                            u.labelResultado(lblResultado, "Ya existe un cliente registrado con ese número de documento.", 1, "Error!");
                            return;
                        }
                    }

                }
            }
            if (raz.Length > 75)
            {
                u.labelResultado(lblResultado, "La Razón Social del Cliente no puede tener más de 75 caracteres.", 1, "Error!");
                return;
            }

            cli.Estado = est;
            cli.Nombre = nom;
            cli.Apellido = ape;
            cli.Direccion = dir;
            cli.Altura = alt;
            cli.Piso = pis;
            cli.Depto = dep;
            cli.Telef = tel;
            cli.Celular = cel;
            cli.Email = ema;
            cli.Tipodoc = (nroDoc.Length > 0 ? ddlModTipoDoc.SelectedValue : "");
            cli.Nrodoc = nroDoc;
            cli.Razsoc = raz;
            cli.Tipoiva = ddlModTipoIVA.SelectedValue;
            cli.Fecnacimiento = txtModFecNac.Text;
            cli.update();


            //SOLAPA HISTORIAL
            if (!String.IsNullOrEmpty(txtModHistorial.Text)) //si el historial no es nulo, entonces guardamos
            {
                HistorialLegajo hl = new HistorialLegajo();
                string his = u.sqlSeguro(txtModHistorial.Text.Trim());
                if (his.Length > 5000)
                {
                    u.labelResultado(lblResultado, "El Historial no puede tener más de 5000 caracteres.", 1, "Error!");
                    return;
                }

                hl.IdLegajo = leg.Id;
                hl.Historial = u.sqlSeguro(txtModHistorial.Text.Trim());
                hl.Fecha = u.getFechaActual(0);
                hl.guardar();
            }


            //SOLAPA ACTORES
            if (!String.IsNullOrEmpty(txtModActorNombre.Text) && !String.IsNullOrEmpty(txtModActorApellido.Text) && !String.IsNullOrEmpty(txtModActorNroDoc.Text))
            {
                string nomAc = u.sqlSeguro(txtModActorNombre.Text.Trim());
                string apeAc = u.sqlSeguro(txtModActorApellido.Text.Trim().ToUpper());
                string telAc = u.sqlSeguro(txtModActorTel.Text.Trim());
                string celAc = u.sqlSeguro(txtModActorCel.Text.Trim());
                string emaAc = u.sqlSeguro(txtModActorEmail.Text.Trim());
                string dirAc = u.sqlSeguro(txtModActorDireccion.Text.Trim());
                string altAc = u.sqlSeguro(txtModActorAltura.Text.Trim());
                string pisAc = u.sqlSeguro(txtModActorPiso.Text.Trim());
                string depAc = u.sqlSeguro(txtModActorDepto.Text.Trim());
                string nroDocAc = u.sqlSeguro(txtModActorNroDoc.Text.Trim());

                ActoresLegajo actl = new ActoresLegajo();

                if (nomAc.Length > 80)
                {
                    u.labelResultado(lblResultado, "El Nombre del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                    return;
                }
                if (apeAc.Length > 80)
                {
                    u.labelResultado(lblResultado, "El Apellido del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                    return;
                }
                if (telAc.Length > 45)
                {
                    u.labelResultado(lblResultado, "El Teléfono del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                    return;
                }
                if (celAc.Length > 45)
                {
                    u.labelResultado(lblResultado, "El Celular del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                    return;
                }
                if (emaAc.Length > 45)
                {
                    u.labelResultado(lblResultado, "El Email del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                    return;
                }
                if (dirAc.Length > 80)
                {
                    u.labelResultado(lblResultado, "La Dirección del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                    return;
                }
                if (altAc.Length > 6)
                {
                    u.labelResultado(lblResultado, "La Altura del cliente no puede tener más de 6 caracteres.", 1, "Error!");
                    return;
                }
                if (pisAc.Length > 3)
                {
                    u.labelResultado(lblResultado, "El Piso del cliente no puede tener más de 3 caracteres.", 1, "Error!");
                    return;
                }
                if (depAc.Length > 2)
                {
                    u.labelResultado(lblResultado, "El Depto del cliente no puede tener más de 2 caracteres.", 1, "Error!");
                    return;
                }
                if (nroDocAc.Length > 0)
                {
                    if (!u.EsNumerico(nroDoc, true))
                    {
                        u.labelResultado(lblResultado, "El Nro. de Documento del Cliente no es un dato numérico, sin guiones ni puntos.", 1, "Error!");
                        return;
                    }
                }

                actl.Nombre = nomAc;
                actl.Apellido = apeAc;
                actl.Direccion = dirAc;
                actl.Altura = altAc;
                actl.Piso = pisAc;
                actl.Depto = depAc;
                actl.Telef = telAc;
                actl.Celular = celAc;
                actl.Email = emaAc;
                actl.Tipodoc = (nroDocAc.Length > 0 ? ddlModActorTipo.SelectedValue : "");
                actl.Nrodoc = nroDocAc;
                actl.Fecnacimiento = txtModActorFecNac.Text;
                actl.IdLegajo = leg.Id;
                actl.guardar();
            }


            //SOLAPA DOCUMENTACIÓN
            string carpeta = "/adj/legajos/" + leg.Id;

            //primero verificamos que si existe una carpeta con el mismo ID de propiedad
            bool existe = System.IO.Directory.Exists(Server.MapPath(carpeta));
            if (!existe)
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(carpeta));
            }

            HttpFileCollection fileUploadAdd1 = Request.Files;

            AdjuntosLegajo al = new AdjuntosLegajo();

            al.IdLegajo = leg.Id;

            for (int i = 0; i < fileUploadAdd1.Count; i++)
            {
                HttpPostedFile adjunto = fileUploadAdd1[i];

                //bloque para validar que el archivo adjuntado acupe menos de 2mb aprox
                if (adjunto.ContentLength < 4000000 && adjunto.ContentLength > 0)
                {
                    string ext = u.getExtensionArchivo(adjunto.FileName).ToLower();
                    al.RutaAdjunto = carpeta + "/" + al.Id + "-" + u.getFechaActual() + u.getHoraActual() + "-" + i + "." + ext; //sumamos "i", ya que al adjuntar 2 o más archivos, es la forma de diferenciarlos, ya que todos se grabarían con la misma fecha y hora
                    FileInfo fi = new FileInfo(Server.MapPath(al.RutaAdjunto));

                    adjunto.SaveAs(fi.FullName);

                    al.Fecha_alta = DateTime.Today.ToString("dd-MM-yy");
                    al.InsertIntoDB();
                }
            }

            cargarAdjuntos(leg.Id);

            //SOLAPA APORTES
            if (!String.IsNullOrEmpty(txtModFecPago.Text) && !String.IsNullOrEmpty(txtModMonto.Text)) //si la fecha y el monto no son nulos, entonces guardamos
            {
                PagosLegajo pl = new PagosLegajo();
                string fecPago = u.sqlSeguro(txtModFecPago.Text.Trim());
                string montoPago = u.sqlSeguro(txtModMonto.Text.Trim());
                string obsPago = u.sqlSeguro(txtModObservaciones.Text.Trim());

                if (fecPago.Length > 10)
                {
                    u.labelResultado(lblResultado, "La Fecha no puede tener más de 10 caracteres.", 1, "Error!");
                    return;
                }
                if (montoPago.Length > 10)
                {
                    u.labelResultado(lblResultado, "El monto no puede tener más de 10 caracteres.", 1, "Error!");
                    return;
                }
                if (obsPago.Length > 500)
                {
                    u.labelResultado(lblResultado, "La Observación no puede tener más de 500 caracteres.", 1, "Error!");
                    return;
                }

                pl.IdLegajo = leg.Id;
                pl.Fecha = fec;
                pl.Monto = montoPago;
                pl.Observaciones = obsPago;
                pl.Moneda = ddlModMoneda.SelectedValue;
                pl.guardar();
            }

            u.labelResultado(lblResultado, "Legajo modificado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnElimOK_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoBaja(Page);

            string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
            Legajos leg = Legajos.obtenerPorId(idModif);
            if (leg == null)
            {
                u.showMessage(this, "Error: no se encuentra el legajo.", 1);
                return;
            }

            leg.delete();
            u.labelResultado(lblResultado, "Legajo eliminado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        public void cargarAdjuntos(string id)
        {
            //ARCHIVOS ADJUNTOS
            AdjuntosLegajo[] ap = AdjuntosLegajo.obtenerPorIdLegajo(id);
            if (ap != null)
            {
                StringBuilder s = new StringBuilder();
                foreach (AdjuntosLegajo adj in ap)
                {
                    if (!String.IsNullOrEmpty(adj.RutaAdjunto)) //si existe un archivo adjunto 
                    {
                        string fileName = adj.RutaAdjunto;

                        FileInfo fi = new FileInfo(Server.MapPath(fileName));
                        if (fi.Exists)
                        {
                            string ext = u.getExtensionArchivo(fileName).ToLower();
                            string thumbnail = "";
                            switch (ext)
                            {
                                case "pdf":
                                case "pptx":
                                    thumbnail = "class='thumbnail ImgPdf'";
                                    break;
                                case "xls":
                                case "xml":
                                case "xlt":
                                case "xlsx":
                                case "gsh":
                                case "gsheet":
                                    thumbnail = "class='thumbnail ImgExc'";
                                    break;
                                case "gif":
                                case "jpg":
                                case "png":
                                case "bmp":
                                case "jpeg":
                                    thumbnail = "class='thumbnail ImgPic'";
                                    break;
                                case "doc":
                                case "docx":
                                case "txt":
                                case "gdoc":
                                    thumbnail = "class='thumbnail ImgTxt'";
                                    break;
                                default:
                                    thumbnail = "class='thumbnail'";
                                    break;
                            }

                            s.Append("<li " + thumbnail + " id=" + adj.Id + ">");
                            s.Append("<a href=" + fileName + " class='cboxElement' target='_blank'>");
                            s.Append("<img src=" + fileName + "></a>");
                            s.Append("<div class='well gallery-controls' style='margin-top: -1px; text-align:center'><p><a href='ABMLegajos.aspx?mId=" + u.encriptarTexto(id) + "&fId=" + u.encriptarTexto(adj.Id) + "&fn=" + u.encriptarTexto(fileName) + "' class='gallery-delete btn'><i class='icon-remove'></i></a></p></div>");
                            s.Append("</li>");
                        }
                    }
                }
                litAdjuntos.Text = s.ToString();
            }
        }
    }
}