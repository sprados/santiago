﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Text;
using System.Web.UI;

namespace CSMMonitor
{
    public partial class WebForm10 : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Permisos.verificarPermiso(Page);
                if (Request.QueryString["mId"] == null && Request.QueryString["eId"] == null)
                {
                    //ESTA QUERIENDO VER EL LISTADO
                    Permisos.verificarPermisoConsulta(Page);
                    llenarGrilla();
                    pnlVerListado.Visible = true;
                }
                else if (Request.QueryString["mId"] != null)
                {
                    //ESTA QUERIENDO MODIFICAR
                    Permisos.verificarPermisoModificar(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
                    Usuarios usu = Usuarios.obtenerPorIdUsuario(idModif);
                    if (usu != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = false;
                        pnlModificar.Visible = true;
                        pnlVerListado.Visible = false;
                        txtModEmail.Text = usu.Email;
                        txtModLogin.Text = usu.Username;
                        txtModNombre.Text = usu.Nombre;
                        txtModPass1.Text = "";
                        txtModPass2.Text = "";
                        ddlModTipo.SelectedIndex = (usu.Tipo == "A" ? 0 : 1);
                       // ddlModEstado.SelectedIndex = (usu.Estado == "a" ? 0 : 1);
                    }
                    else
                    {
                        u.showMessage(this, "Error: no se encuentra el usuario .", 1);
                        return;
                    }
                }
                else if (Request.QueryString["eId"] != null)
                {
                    //ESTA QUERIENDO ELIMINAR
                    Permisos.verificarPermisoBaja(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
                    Usuarios usu = Usuarios.obtenerPorIdUsuario(idModif);
                    if (usu != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = true;
                        pnlModificar.Visible = false;
                        pnlVerListado.Visible = false;
                        u.labelResultado(lblEliminar, "¿Está seguro que desea eliminar el usuario " + usu.Username + "?", 3, "Atención!");
                        u.agregarModal(ref btnElimOK, "Confirmar", "¿Está seguro que desea eliminar el usuario " + usu.Username + "?", Page);
                    }
                }
            }
        }

        private void llenarGrilla()
        {
            Usuarios[] usus = Usuarios.obtenerTodos();
            StringBuilder s = new StringBuilder();
            if (usus != null)
            {
                foreach (Usuarios usu in usus)
                {
                    //LOS CARGAMOS EN LA GRILLA.
                    string idEncrip = u.encriptarTexto(usu.Id);
                    s.Append("<tr>");
                    s.Append("<td class='center'>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-success'  href='#' onclick='openWin(\"VerUsuario.aspx?id=" + idEncrip + "\")' data-rel='tooltip' data-title='Ver Datos Completos...'><i class='icon-zoom-in icon-white'></i></a>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-info' href='ABMUsuarios.aspx?mId=" + idEncrip + "' data-rel='tooltip' data-title='Modificar Usuario...'><i class='icon-edit icon-white'></i></a>" +
                        "<a class='btn btn-mini btn-danger' href='ABMUsuarios.aspx?eId=" + idEncrip + "' data-rel='tooltip' data-title='Eliminar Usuario...'><i class='icon-trash icon-white'></i></a></td>");
                    s.Append("<td>" + usu.Username + "</td>");
                    s.Append("<td>" + usu.Nombre + "</td>");
                    s.Append("<td>" + usu.Email + "</td>");
                    s.Append("<td>" + (usu.Tipo == "A" ? "Administrador" : "Operador") + "</td>");
                    s.Append("<td class='monedaTD'>" + (usu.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                    s.Append("</tr>");
                }
                litGrilla.Text = s.ToString();
            }
        }

        protected void btnAgregarGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            string user = u.sqlSeguro(txtAddLogin.Text.Trim());
            string pass1 = u.sqlSeguro(txtAddPass1.Text.Trim());
            string pass2 = u.sqlSeguro(txtAddPass2.Text.Trim());
            string nom = u.sqlSeguro(txtAddNombre.Text.Trim());
            string email = u.sqlSeguro(txtAddEmail.Text.Trim());
            string est = "a";// ddlAddEstado.SelectedValue;

            Usuarios us = new Usuarios();


            if (user.Length > 45)
            {
                u.labelResultado(lblResultado, "El Username no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            else
            {
                if (user.Length < 3)
                {
                    u.labelResultado(lblResultado, "El Username no puede tener menos de 3 caracteres.", 1, "Error!");
                    return;
                }
                else {

                    us = Usuarios.obtenerUserName(user);
                    if (us != null){
                        u.labelResultado(lblResultado, "Ya existe un Usuario con ese Username", 1, "Error!");
                        return;
                    }
                }
            }
            if (pass1.Length > 45)
            {
                u.labelResultado(lblResultado, "El Password no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (pass1.Length < 8)
            {
                u.labelResultado(lblResultado, "El Password no puede tener menos de 8 caracteres.", 1, "Error!");
                return;
            }
            if (pass1 != pass2)
            {
                u.labelResultado(lblResultado, "Las passwords no coinciden.", 1, "Error!");
                return;
            }
            if (nom.Length > 45)
            {
                u.labelResultado(lblResultado, "El Nomre no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (nom.Length < 3)
            {
                u.labelResultado(lblResultado, "El Nombre no puede tener menos de 3 caracteres.", 1, "Error!");
                return;
            }
            if (email.Length > 80)
            {
                u.labelResultado(lblResultado, "El Email no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (email != "" && !u.esEmailValido(email))
            {
                u.labelResultado(lblResultado, "El Email no tiene un formato correcto.", 1, "Error!");
                return;
            }

            Usuarios usu = new Usuarios();
            usu.Email = email;
            usu.Nombre = nom;
            usu.Pass = pass1;
            usu.Username = user;
            usu.Estado = est;
            usu.Tipo = ddlAddTipo.SelectedValue;
            usu.guardar();

            u.labelResultado(lblResultado, "Usuario agregado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            pnlAgregar.Visible = true;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = false;
            lblResultado.Visible = false;
            txtAddEmail.Text = "";
            txtAddLogin.Text = "";
            txtAddNombre.Text = "";
            txtAddPass1.Text = "";
            txtAddPass2.Text = "";
        }

        protected void btnModGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoModificar(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
            Usuarios usu = Usuarios.obtenerPorIdUsuario(idModif);
            if (usu == null)
            {
                u.showMessage(this, "Error: no se encuentra el usuario.", 1);
                return;
            }

            string user = u.sqlSeguro(txtModLogin.Text.Trim());
            string pass1 = u.sqlSeguro(txtModPass1.Text.Trim());
            string pass2 = u.sqlSeguro(txtModPass2.Text.Trim());
            string nom = u.sqlSeguro(txtModNombre.Text.Trim());
            string email = u.sqlSeguro(txtModEmail.Text.Trim());
            string est = "a";//ddlModEstado.SelectedValue;

            if (user.Length > 45)
            {
                u.labelResultado(lblResultado, "El Username no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            else
            {

                if (user.Length < 3)
                {
                    u.labelResultado(lblResultado, "El Username no puede tener menos de 3 caracteres.", 1, "Error!");
                    return;
                }
                else {
                    Usuarios usExistente = new Usuarios();
                    usExistente = Usuarios.obtenerUserName(user);
                    if (usExistente != null)
                    {

                        if (usExistente.Id != usExistente.Id)
                        {
                            u.labelResultado(lblResultado, "Ya existe un Usuario con ese Username", 1, "Error!");
                            return;
                        }
                    }
                
                }
            }
            if (pass1 != "")
            {
                if (pass1.Length > 45)
                {
                    u.labelResultado(lblResultado, "El Password no puede tener más de 45 caracteres.", 1, "Error!");
                    return;
                }
                if (pass1.Length < 8)
                {
                    u.labelResultado(lblResultado, "El Password no puede tener menos de 8 caracteres.", 1, "Error!");
                    return;
                }
                if (pass1 != pass2)
                {
                    u.labelResultado(lblResultado, "Las passwords no coinciden.", 1, "Error!");
                    return;
                }
            }
            if (nom.Length > 45)
            {
                u.labelResultado(lblResultado, "El Nomre no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (nom.Length < 3)
            {
                u.labelResultado(lblResultado, "El Nombre no puede tener menos de 3 caracteres.", 1, "Error!");
                return;
            }
            if (email.Length > 80)
            {
                u.labelResultado(lblResultado, "El Email no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (email != "" && !u.esEmailValido(email))
            {
                u.labelResultado(lblResultado, "El Email no tiene un formato correcto.", 1, "Error!");
                return;
            }

            usu.Email = email;
            usu.Nombre = nom;
            usu.Pass = (pass1 != "" ? pass1 : usu.Pass);
            usu.Username = user;
            usu.Estado = est;
            usu.Tipo = ddlModTipo.SelectedValue;
            usu.updateUser();
            u.labelResultado(lblResultado, "Usuario modificado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnElimOK_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoBaja(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
            Usuarios usu = Usuarios.obtenerPorIdUsuario(idModif);
            if (usu == null)
            {
                u.showMessage(this, "Error: no se encuentra el usuario.", 1);
                return;
            }

            usu.delete();
            u.labelResultado(lblResultado, "Usuario eliminado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABMUsuarios.aspx");
        }




    }
}