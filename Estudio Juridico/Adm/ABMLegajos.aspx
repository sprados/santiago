﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="ABMLegajos.aspx.cs" Inherits="Estudio_Juridico.Adm.ABMLegajos" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" charset="utf-8">
        $(function () {
            $("#tabs").tabs({
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <ul class="breadcrumb">
            <li>
                <h3>LEGAJOS
                </h3>
            </li>
            <li>
                <p>
                    Utilice el presente formulario para dar de alta, baja o modificar Legajos.
                </p>
            </li>
        </ul>
    </div>

    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>

    <asp:Panel class="row-fluid" runat="server" ID="pnlVerListado" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;LISTADO DE LEGAJOS</h2>
                    <div class="box-icon">
                    </div>
                </div>
                <div class="box-content">
                    <table id="tab1" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 100px">Acciones</th>
                                <th style="width: auto">Descripción</th>
                                <th style="width: auto">Cliente</th>
                                <th style="width: auto">Fecha</th>
                                <th style="width: auto">Código Exp.</th>
                                <th style="width: auto">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal ID="litGrilla" runat="server"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnNuevo" runat="server" Text="Registrar Nuevo Legajo" OnClick="btnNuevo_Click" class="btn btn-info" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnSalir1" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlAgregar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;NUEVO LEGAJO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblAdd" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span4">
                        <div class="box-content form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Cliente:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAddClientes" runat="server" data-rel="chosen" CssClass="input-xlarge">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Descripción:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddDescripcion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Fecha Inicio:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddFecha" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">
                                    Código Expediente:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddCodigo" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnAgregarGuardar" runat="server" Text="Registrar Legajo" class="btn btn-primary" OnClick="btnAgregarGuardar_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnAgregarSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlModificar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-user"></i>&nbsp;&nbsp;<asp:Label ID="lblLegajo" runat="server"></asp:Label></h2>
                    <div class="box-icon">
                    </div>
                </div>
                <div id="tabs">
                    <ul>
                        <li><a href="#Tab1">
                            <asp:Label ID="lbl1" runat="server" Text="General"></asp:Label></a></li>
                        <li><a href="#Tab2">
                            <asp:Label ID="lbl2" runat="server" Text="Cliente"></asp:Label></a></li>
                        <li><a href="#Tab3">
                            <asp:Label ID="lbl3" runat="server" Text="Historial"></asp:Label></a></li>
                        <li><a href="#Tab4">
                            <asp:Label ID="lbl4" runat="server" Text="Actores"></asp:Label></a></li>
                        <li><a href="#Tab5">
                            <asp:Label ID="lbl5" runat="server" Text="Documentación"></asp:Label></a></li>
                        <li><a href="#Tab6">
                            <asp:Label ID="lbl6" runat="server" Text="Aportes"></asp:Label></a></li>
                    </ul>
                    <div id="Tab1" class="box-content">
                        <%--GENERAL--%>
                        <div class="box-content">
                            <asp:Panel class="row-fluid" runat="server" ID="pnl1">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="span4">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Descripción:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModDescripcion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Fecha Inicio:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModFecha" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Código Expediente:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModCodigo" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Estado:</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlModEstadoLeg" runat="server">
                                                                <asp:ListItem Value="a">Alta</asp:ListItem>
                                                                <asp:ListItem Value="b">Baja</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div id="Tab2" class="box-content">
                        <%--CLIENTE--%>
                        <div class="box-content">
                            <asp:Panel class="row-fluid" runat="server" ID="pnl2">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="span6">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Apellido:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModApellido" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Nombre:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModNombre" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Tipo Documento:</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlModTipoDoc" runat="server" class="input-large">
                                                                <asp:ListItem Value="96" Selected="True">DNI</asp:ListItem>
                                                                <asp:ListItem Value="80">CUIT</asp:ListItem>
                                                                <asp:ListItem Value="86">CUIL</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Nro. Documento:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModNroDoc" runat="server" MaxLength="15" class="input-large" TextMode="Number"></asp:TextBox>
                                                            (solo números, sin guiones)
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Fec. Nacimiento:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModFecNac" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Razón Social:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModRazSoc" runat="server" MaxLength="75" class="input-large"></asp:TextBox>
                                                            (opcional)
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Tipo IVA:</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlModTipoIVA" runat="server" class="input-large">
                                                                <asp:ListItem Value="M">Monotributista</asp:ListItem>
                                                                <asp:ListItem Value="I">Resp. Inscripto</asp:ListItem>
                                                                <asp:ListItem Value="N">Resp. No Inscripto</asp:ListItem>
                                                                <asp:ListItem Value="E">Exento</asp:ListItem>
                                                                <asp:ListItem Value="C" Selected="True">Consumidor Final</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Estado:</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlModEstado" runat="server">
                                                                <asp:ListItem Selected="True" Value="a">Alta</asp:ListItem>
                                                                <asp:ListItem Value="b">Baja</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Teléfono:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModTelefono" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Celular:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModCel" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Email:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModEmail" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Dirección:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModDireccion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Altura:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModAltura" runat="server" MaxLength="6" class="input-small"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Piso:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModPiso" runat="server" MaxLength="3" class="input-small"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Depto:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModDepto" runat="server" MaxLength="2" class="input-small"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div id="Tab3" class="box-content">
                        <%--HISTORIAL--%>
                        <div class="box-content">
                            <asp:Panel class="row-fluid" runat="server" ID="pnl3">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="span12">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Historial:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModHistorial" runat="server" class="input-xxlarge" Rows="3" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel class="row-fluid" runat="server" ID="Panel4">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="control-group">
                                                <table id="Table2" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 10%; text-align: center">Fecha</th>
                                                            <th style="width: auto">Historial</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Literal ID="litHistorial" runat="server"></asp:Literal>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div id="Tab4" class="box-content">
                        <%--ACTORES--%>
                        <div class="box-content">
                            <asp:Panel class="row-fluid" runat="server" ID="Panel5">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="span6">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Apellido:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorApellido" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Nombre:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorNombre" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Tipo Documento:</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlModActorTipo" runat="server" class="input-large">
                                                                <asp:ListItem Value="96" Selected="True">DNI</asp:ListItem>
                                                                <asp:ListItem Value="80">CUIT</asp:ListItem>
                                                                <asp:ListItem Value="86">CUIL</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Nro. Documento:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorNroDoc" runat="server" MaxLength="15" class="input-large" TextMode="Number"></asp:TextBox>
                                                            (solo números, sin guiones)
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Fec. Nacimiento:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorFecNac" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Teléfono:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorTel" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Celular:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorCel" runat="server" MaxLength="45" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Email:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorEmail" runat="server" MaxLength="80" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Dirección:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorDireccion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Altura:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorAltura" runat="server" MaxLength="6" class="input-small"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Piso:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorPiso" runat="server" MaxLength="3" class="input-small"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Depto:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModActorDepto" runat="server" MaxLength="2" class="input-small"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel class="row-fluid" runat="server" ID="Panel6">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="control-group">
                                                <table id="Table3" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 7%">Acciones</th>
                                                            <th style="width: auto">Cliente</th>
                                                            <th style="width: auto">Dirección</th>
                                                            <th style="width: auto">Teléfono / Celular</th>
                                                            <th style="width: auto">Email</th>
                                                            <th style="width: auto">Documento</th>
                                                            <th style="width: auto">Estado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Literal ID="litActores" runat="server"></asp:Literal>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div id="Tab5" class="box-content">
                        <%--DOCUMENTACIÓN--%>
                        <asp:Panel class="row-fluid" runat="server" ID="pnl5">
                            <div class="row-fluid">
                                <div class="box span12">
                                    <div class="box-content">
                                        <div class="span12">
                                            <div class="box-content">
                                                <div class="control-group">
                                                    <label class="control-label">
                                                        Adjuntar Imágenes:</label>
                                                    <div class="controls">
                                                        <asp:FileUpload ID="fileUploadMod1" runat="server" AllowMultiple="true" />
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <asp:Label ID="Label2" runat="server" Text="NOTA: Para un mejor rendimiento del sitio, los archivos mayores a 4Mb. no serán cargados" ForeColor="#FF3300" Font-Size="Medium"></asp:Label>
                                                    </div>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel class="row-fluid" runat="server" ID="Panel3">
                            <div class="row-fluid">
                                <div class="box span12">
                                    <div class="box-content">
                                        <div class="span12">
                                            <ul class="thumbnails gallery">
                                                <asp:Literal ID="litAdjuntos" runat="server"></asp:Literal>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div id="Tab6" class="box-content">
                        <%--APORTES--%>
                        <div class="box-content">
                            <asp:Panel class="row-fluid" runat="server" ID="Panel1">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="span6">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Fecha Pago:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModFecPago" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Monto:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModMonto" runat="server" MaxLength="10" CssClass="input-large" TextMode="Number"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <div class="box-content form-horizontal">
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                            Moneda:</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddlModMoneda" runat="server" class="input-large">
                                                                <asp:ListItem Value="$" Selected="True">$</asp:ListItem>
                                                                <asp:ListItem Value="U$D">U$D</asp:ListItem>
                                                                <asp:ListItem Value="R$">R$</asp:ListItem>
                                                                <asp:ListItem Value="€">€</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group ">
                                                        <label class="control-label">
                                                            Observaciones Pago:</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txtModObservaciones" runat="server" class="input-xlarge" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel class="row-fluid" runat="server" ID="Panel2">
                                <div class="row-fluid">
                                    <div class="box span12">
                                        <div class="box-content">
                                            <div class="span12">
                                                <div class="control-group">
                                                    <table id="Table1" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10%; text-align: center">Fecha</th>
                                                                <th style="width: 10%;">Moneda</th>
                                                                <th style="width: 10%;">Monto</th>
                                                                <th style="width: auto">Observaciones</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Literal ID="litPagos" runat="server"></asp:Literal>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid sortable ui-sortable">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-content">
                        <center>
                        <div class="box-content">
                            <asp:Button ID="btnModGuardar" runat="server" Text="Guardar Legajo" class="btn btn-primary" OnClick="btnModGuardar_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnModSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlEliminar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;ELIMINAR UN LEGAJO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblElim" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <asp:Label ID="lblEliminar" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnElimOK" runat="server" Text="Eliminar Legajo" class="btn btn-primary" OnClick="btnElimOK_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnElimSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:HiddenField ID="hdnHistorial" runat="server" />
</asp:Content>
