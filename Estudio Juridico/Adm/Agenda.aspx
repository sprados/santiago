﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="Agenda.aspx.cs" Inherits="Estudio_Juridico.Adm.Agenda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="css/cupertino/jquery-ui-1.7.3.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/fullcalendar.css" rel="stylesheet" type="text/css" />
    <!--esta es la que tiene problemas para template de agenda -->
    <script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------->
    <script src="js/jquery-ui-1.7.3.custom.min.js" type="text/javascript"></script>
    <script src="js/jquery.qtip-1.0.0-rc3.min.js" type="text/javascript"></script>
    <script src="js/fullcalendar.min.js" type="text/javascript"></script>
    <script src="js/calendarscript.js" type="text/javascript"></script>
    <script src="js/jquery-ui-timepicker-addon-0.6.2.min.js" type="text/javascript"></script>
    <style type='text/css'>
        /* css for timepicker */
        .ui-timepicker-div dl
        {
            text-align: left;
        }
        .ui-timepicker-div dl dt
        {
            height: 25px;
        }
        .ui-timepicker-div dl dd
        {
            margin: -25px 0 10px 65px;
        }
        .style1
        {
            width: 100%;
        }
        
        /* table fields alignment*/
        .alignRight
        {
        	text-align:right;
        	padding-right:10px;
        	padding-bottom:10px;
        }
        .alignLeft
        {
        	text-align:left;
        	padding-bottom:10px;
        }
    </style>

    <%--<form id="form1" runat="server">--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="false">
    </asp:ScriptManager>
    
    <div>
        <ul class="breadcrumb">
            <li>
                <h3>Agenda
                </h3>
            </li>
            <li>
                <p>
                    Utilice la siguiente Agenda para dar de alta sus eventos.
                </p>

            </li>
        </ul>
    </div>
    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>
    <asp:Panel class="row-fluid" runat="server" ID="pnlVerListado">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;AGENDA</h2>
                    <div class="box-icon">
                    </div>
                </div>
                <div class="box-content">
                    <div id="calendar" style="width: 100%"></div>
                    <div id="updatedialog" style="font: 70% 'Trebuchet MS', sans-serif; margin: 50px;"
                        title="Update or Delete Event">
                        <table cellpadding="0" class="style1">
                            <tr>
                                <td class="alignRight">name:</td>
                                <td class="alignLeft">
                                    <input id="eventName" type="text" /><br />
                                </td>
                            </tr>
                            <tr>
                                <td class="alignRight">Descripcion:</td>
                                <td class="alignLeft">
                                    <textarea id="eventDesc" cols="30" rows="3"></textarea></td>
                            </tr>
                            <tr>
                                <td class="alignRight">Inicio:</td>
                                <td class="alignLeft">
                                    <span id="eventStart"></span></td>
                            </tr>
                            <tr>
                                <td class="alignRight">Fin: </td>
                                <td class="alignLeft">
                                    <span id="eventEnd"></span>
                                    <input type="hidden" id="eventId" /></td>
                            </tr>
                        </table>
                    </div>
                    <div id="addDialog" style="font: 70% 'Trebuchet MS', sans-serif; margin: 50px;" title="Agregar Evento">
                        <table cellpadding="0" class="style1">
                            <tr>
                                <td class="alignRight">Nombre:</td>
                                <td class="alignLeft">
                                    <input id="addEventName" type="text" size="50" /><br />
                                </td>
                            </tr>
                            <tr>
                                <td class="alignRight">Descripcion:</td>
                                <td class="alignLeft">
                                    <textarea id="addEventDesc" cols="30" rows="3"></textarea></td>
                            </tr>
                            <tr>
                                <td class="alignRight">Inicio:</td>
                                <td class="alignLeft">
                                    <span id="addEventStartDate"></span></td>
                            </tr>
                            <tr>
                                <td class="alignRight">Fin:</td>
                                <td class="alignLeft">
                                    <span id="addEventEndDate"></span></td>
                            </tr>
                        </table>

                    </div>
                    <div runat="server" id="jsonDiv" />
                    <input type="hidden" id="hdClient" runat="server" />
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
