﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="AdmPermisos.aspx.cs" Inherits="helpers.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .estiloColumnas {
            width: 55px;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <ul class="breadcrumb">
            <li>
                <h3>PERMISOS DE USUARIOS
                </h3>
            </li>
            <li>
                <p>
                    Utilice el presente formulario para establecer los permisos de los usuarios a los diferentes menúes del sistema.
                </p>
            </li>
        </ul>
    </div>
    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>

    <div class="row-fluid">

        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2>Usuarios del Sistema</h2>
                </div>
                <div class="box-content">

                    <div class="span6">
                        <div class="control-group form-horizontal">
                            <label class="control-label">Administrar Usuario:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlUsuario" runat="server" data-rel="chosen" CssClass="input-xlarge" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlUsuario_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2>Menúes del Sistema.
                    </h2>
                </div>
                <div class="box-content">
                    <p>Seleccione a qué menú puede acceder el usuario seleccionado</p>
                    <asp:GridView ID="grid" runat="server" class="table table-striped table-bordered table-condensed" AutoGenerateColumns="false">

                        <Columns>

                            <asp:TemplateField HeaderText="Id" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PosicionPorDefecto" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblPosicionPorDefecto" runat="server" Text='<%# Bind("PosicionPorDefecto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdm" runat="server" Text='<%# Bind("AdminSiempre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-CssClass="estiloColumnas" HeaderText="Alta" HeaderStyle-BackColor="#DDDDDD" HeaderStyle-ForeColor="#555555">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkTodoAlta" runat="server" Text="Alta" CssClass="checkChoto" OnCheckedChanged="chkTodoAlta_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkAlta" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-CssClass="estiloColumnas" HeaderText="Baja" HeaderStyle-BackColor="#DDDDDD" HeaderStyle-ForeColor="#555555">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkTodoBaja" runat="server" Text="Baja" CssClass="checkChoto" OnCheckedChanged="chkTodoBaja_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkBaja" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-CssClass="estiloColumnas" HeaderText="Modificación" HeaderStyle-BackColor="#DDDDDD" HeaderStyle-ForeColor="#555555">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkTodoModif" runat="server" Text="Modif." CssClass="checkChoto" OnCheckedChanged="chkTodoModif_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkModif" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-CssClass="estiloColumnas" HeaderText="Consulta" HeaderStyle-BackColor="#DDDDDD" HeaderStyle-ForeColor="#555555">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkTodoConsulta" runat="server" Text="Consulta" CssClass="checkChoto" OnCheckedChanged="chkTodoConsulta_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkConsulta" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Menú" HeaderStyle-Width="40px" HeaderStyle-BackColor="#DDDDDD">
                                <ItemTemplate>
                                    <asp:Label ID="lblMenu" runat="server" Text='<%# Bind("Menu") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>
                    <p>*: siempre visible para los administradores.</p>
                </div>
            </div>

            <p class="center">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar Permisos" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnSalir" runat="server" Text="Salir/Cancelar" CssClass="btn btn-danger" OnClick="btnSalir_Click" />
            </p>

        </div>
    </div>
</asp:Content>
