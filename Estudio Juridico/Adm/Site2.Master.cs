﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inmobiliaria.Adm
{
    public partial class Site2 : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //CONTROLAMOS QUE EL USUARIO SE HAYA LOGUEADO Y QUE LA SESION ESTÉ ABIERTA
            if (Session["idUser"] == null || Session["idUser"].ToString() == "")
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("/Inicio.aspx?goTo=" + Request.Path.Remove(0, 1));
                return;
            }
            else Session.Add("errMsg", "");
            //FIN CONTROL DE SESION

            Utiles u = new Utiles();
            u.abrirConexion(Response);
        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            Utiles u = new Utiles();
            u.cerrarConexion();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}