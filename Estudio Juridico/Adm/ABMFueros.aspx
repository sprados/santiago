﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="ABMFueros.aspx.cs" Inherits="Estudio_Juridico.Adm.ABMFueros" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <ul class="breadcrumb">
            <li>
                <h3>FUEROS
                </h3>
            </li>
            <li>
                <p>
                    Utilice el presente formulario para dar de alta, baja o modificar Fueros.
                </p>
            </li>
        </ul>
    </div>

    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>

    <asp:Panel class="row-fluid" runat="server" ID="pnlVerListado" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;LISTADO DE FUEROS</h2>
                    <div class="box-icon">
                    </div>
                </div>
                <div class="box-content">
                    <table id="tab1" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 100px">Acciones</th>
                                <th style="width: auto">Fuero</th>
                                <th style="text-align: right">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal ID="litGrilla" runat="server"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnNuevo" runat="server" Text="Registrar Nuevo Fuero" OnClick="btnNuevo_Click" class="btn btn-info" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnSalir1" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlAgregar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;NUEVO FUERO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblAdd" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Fuero:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddFuero" runat="server" MaxLength="100" class="input-large"></asp:TextBox>
                                </div>
                            </div><div class="control-group">
                                &nbsp;<div class="controls">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnAgregarGuardar" runat="server" Text="Registrar Fuero" class="btn btn-primary" OnClick="btnAgregarGuardar_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnAgregarSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlModificar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;MODIFICAR UN FUERO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblMod" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <div class="box-content form-horizontal">
                            <div class="control-group ">
                                <label class="control-label">
                                    Fuero:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtModFuero" runat="server" MaxLength="100" class="input-large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Estado:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlModEstado" runat="server">
                                        <asp:ListItem Selected="True" Value="a">Alta</asp:ListItem>
                                        <asp:ListItem Value="b">Baja</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnModGuardar" runat="server" Text="Guardar Fuero" class="btn btn-primary" OnClick="btnModGuardar_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnModSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel class="row-fluid" runat="server" ID="pnlEliminar" Visible="false">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-home"></i>&nbsp;&nbsp;ELIMINAR UN FUERO</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblElim" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <asp:Label ID="lblEliminar" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnElimOK" runat="server" Text="Eliminar Fuero" class="btn btn-primary" OnClick="btnElimOK_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnElimSalir" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
