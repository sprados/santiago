﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Text;

namespace CSMMonitor
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Permisos.verificarPermiso(Page);
                if (Request.QueryString["mId"] == null && Request.QueryString["eId"] == null)
                {
                    //ESTA QUERIENDO VER EL LISTADO
                    Permisos.verificarPermisoConsulta(Page);
                    llenarGrilla();
                    pnlVerListado.Visible = true;
                }
                else if (Request.QueryString["mId"] != null)
                {
                    //ESTA QUERIENDO MODIFICAR
                    Permisos.verificarPermisoModificar(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
                    Clientes cli = Clientes.obtenerPorId(idModif);
                    if (cli != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = false;
                        pnlModificar.Visible = true;
                        pnlVerListado.Visible = false;
                        txtModNombre.Text = cli.Nombre;
                        txtModApellido.Text = cli.Apellido;
                        txtModTelefono.Text = cli.Telef;
                        txtModCel.Text = cli.Celular;
                        txtModEmail.Text = cli.Email;
                        txtModDireccion.Text = cli.Direccion;
                        txtModAltura.Text = cli.Altura;
                        txtModPiso.Text = cli.Piso;
                        txtModDepto.Text = cli.Depto;
                        ddlModEstado.SelectedIndex = (cli.Estado == "a" ? 0 : 1);
                        u.seleccionarDDLSegunIDValue(ddlModTipoDoc, cli.Tipodoc);
                        txtModNroDoc.Text = cli.Nrodoc;
                        txtModRazSoc.Text = cli.Razsoc;
                        u.seleccionarDDLSegunIDValue(ddlModTipoIVA, cli.Tipoiva);
                        txtModFecNac.Text = cli.Fecnacimiento;
                    }
                    else
                    {
                        u.showMessage(this, "Error: no se encuentra el cliente.", 1);
                        return;
                    }
                }
                else if (Request.QueryString["eId"] != null)
                {
                    //ESTA QUERIENDO ELIMINAR
                    Permisos.verificarPermisoBaja(Page);
                    string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
                    Clientes cli = Clientes.obtenerPorId(idModif);
                    if (cli != null)
                    {
                        pnlAgregar.Visible = false;
                        pnlEliminar.Visible = true;
                        pnlModificar.Visible = false;
                        pnlVerListado.Visible = false;
                        u.labelResultado(lblEliminar, "¿Está seguro que desea eliminar el cliente " + cli.Nombre + "?", 3, "Atención!");
                        u.agregarModal(ref btnElimOK, "Confirmar", "¿Está seguro que desea eliminar el cliente " + cli.Nombre + "?", Page);
                    }
                }
            }
        }

        private void llenarGrilla()
        {
            Clientes[] clientes = Clientes.obtenerTodosTipoClientes();
            StringBuilder s = new StringBuilder();
            string tipoDoc = "";
            if (clientes != null)
            {
                foreach (Clientes c in clientes)
                {
                    switch (c.Tipodoc)
                    {
                        case "80":
                            tipoDoc = "CUIT";
                            break;
                        case "86":
                            tipoDoc = "CUIL";
                            break;
                        case "96":
                            tipoDoc = "DNI";
                            break;
                        default:
                            tipoDoc = "Sin Datos";
                            break;
                    }
                    //LOS CARGAMOS EN LA GRILLA.
                    string idEncrip = u.encriptarTexto(c.Id);
                    s.Append("<tr>");
                    s.Append("<td class='center'>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-info' href='ABMClientes.aspx?mId=" + idEncrip + "' data-rel='tooltip' data-title='Modificar Cliente...'><i class='icon-edit icon-white'></i></a>" +
                        "<a class='btn btn-mini btn-danger' href='ABMClientes.aspx?eId=" + idEncrip + "' data-rel='tooltip' data-title='Eliminar Cliente...'><i class='icon-trash icon-white'></i></a></td>");

                    s.Append("<td>" + c.Apellido + " " + c.Nombre + "</td>");
                    s.Append("<td>" + c.Direccion + " " + c.Altura + " (Piso " + c.Piso + " Depto " + c.Depto + ")</td>");
                    s.Append("<td>" + c.Telef + " / " + c.Celular + "</td>");
                    s.Append("<td>" + c.Email + "</td>");
                    if (!String.IsNullOrEmpty(c.Tipodoc) && !String.IsNullOrEmpty(c.Nrodoc))
                        s.Append("<td>(" + tipoDoc + ") - " + c.Nrodoc + "</td>");
                    else
                        s.Append("<td>" + tipoDoc + "</td>");
                    s.Append("<td>" + (c.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                    s.Append("</tr>");

                }
                litGrilla.Text = s.ToString();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            pnlAgregar.Visible = true;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = false;
            lblResultado.Visible = false;

            txtAddNombre.Text = "";
            txtAddDireccion.Text = "";
            txtAddTelefono.Text = "";
            txtAddCelular.Text = "";
            txtAddEmail.Text = "";
            txtAddRazSoc.Text = "";
            txtAddNroDoc.Text = "";
            txtAddFecNac.Text = "";
        }

        protected void btnAgregarGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoAlta(Page);
            string nom = u.sqlSeguro(txtAddNombre.Text.Trim());
            string ape = u.sqlSeguro(txtAddApellido.Text.Trim().ToUpper());
            string tel = u.sqlSeguro(txtAddTelefono.Text.Trim());
            string cel = u.sqlSeguro(txtAddCelular.Text.Trim());
            string ema = u.sqlSeguro(txtAddEmail.Text.Trim());
            string dir = u.sqlSeguro(txtAddDireccion.Text.Trim());
            string alt = u.sqlSeguro(txtAddAltura.Text.Trim());
            string pis = u.sqlSeguro(txtAddPiso.Text.Trim());
            string dep = u.sqlSeguro(txtAddDepto.Text.Trim());
            string nroDoc = u.sqlSeguro(txtAddNroDoc.Text.Trim());
            string raz = u.sqlSeguro(txtAddRazSoc.Text.Trim());

            Clientes cli = new Clientes();

            if (nom.Length > 80)
            {
                u.labelResultado(lblResultado, "El Nombre del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (ape.Length > 80)
            {
                u.labelResultado(lblResultado, "El Apellido del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (tel.Length > 45)
            {
                u.labelResultado(lblResultado, "El Teléfono del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (cel.Length > 45)
            {
                u.labelResultado(lblResultado, "El Celular del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (ema.Length > 45)
            {
                u.labelResultado(lblResultado, "El Email del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (dir.Length > 80)
            {
                u.labelResultado(lblResultado, "La Dirección del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (alt.Length > 6)
            {
                u.labelResultado(lblResultado, "La Altura del cliente no puede tener más de 6 caracteres.", 1, "Error!");
                return;
            }
            if (pis.Length > 3)
            {
                u.labelResultado(lblResultado, "El Piso del cliente no puede tener más de 3 caracteres.", 1, "Error!");
                return;
            }
            if (dep.Length > 2)
            {
                u.labelResultado(lblResultado, "El Depto del cliente no puede tener más de 2 caracteres.", 1, "Error!");
                return;
            }
            if (nroDoc.Length > 0)
            {
                if (!u.EsNumerico(nroDoc, true))
                {
                    u.labelResultado(lblResultado, "El Nro. de Documento del Cliente no es un dato numérico, sin guiones ni puntos.", 1, "Error!");
                    return;
                }
                else //validamos que el nro. de doc. no exista previamente en BD
                {
                    Clientes clienteExistente = new Clientes();
                    clienteExistente = Clientes.obtenerPorDNI(nroDoc);
                    if (clienteExistente != null)
                    {  
                        u.labelResultado(lblResultado, "Ya existe un cliente registrado con ese número de documento.", 1, "Error!");
                        return;
                    }

                }
            }
            if (raz.Length > 75)
            {
                u.labelResultado(lblResultado, "La Razón Social del Cliente no puede tener más de 75 caracteres.", 1, "Error!");
                return;
            }

            
            cli.Nombre = nom;
            cli.Apellido = ape;
            cli.Direccion = dir;
            cli.Altura = alt;
            cli.Piso = pis;
            cli.Depto = dep;
            cli.Telef = tel;
            cli.Celular = cel;
            cli.Email = ema;
            cli.Tipodoc = (nroDoc.Length > 0 ? ddlAddTipoDoc.SelectedValue : "");
            cli.Nrodoc = nroDoc;
            cli.Razsoc = raz;
            cli.Tipoiva = ddlAddTipoIVA.SelectedValue;
            cli.Fecnacimiento = txtAddFecNac.Text;
            cli.guardar();

            u.labelResultado(lblResultado, "Cliente agregado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnModGuardar_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoModificar(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["mId"].ToString());
            Clientes cli = Clientes.obtenerPorId(idModif);
            if (cli == null)
            {
                u.showMessage(this, "Error: no se encuentra el cliente.", 1);
                return;
            }

            string nom = u.sqlSeguro(txtModNombre.Text.Trim());
            string ape = u.sqlSeguro(txtModApellido.Text.Trim().ToUpper());
            string tel = u.sqlSeguro(txtModTelefono.Text.Trim());
            string cel = u.sqlSeguro(txtModCel.Text.Trim());
            string ema = u.sqlSeguro(txtModEmail.Text.Trim());
            string dir = u.sqlSeguro(txtModDireccion.Text.Trim());
            string alt = u.sqlSeguro(txtModAltura.Text.Trim());
            string pis = u.sqlSeguro(txtModPiso.Text.Trim());
            string dep = u.sqlSeguro(txtModDepto.Text.Trim());
            string est = ddlModEstado.SelectedValue;
            string nroDoc = u.sqlSeguro(txtModNroDoc.Text.Trim());
            string raz = u.sqlSeguro(txtModRazSoc.Text.Trim());

            if (nom.Length > 80)
            {
                u.labelResultado(lblResultado, "El nombre del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (ape.Length > 80)
            {
                u.labelResultado(lblResultado, "El Apellido del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (tel.Length > 45)
            {
                u.labelResultado(lblResultado, "El teléfono del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (cel.Length > 45)
            {
                u.labelResultado(lblResultado, "El Celular del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (ema.Length > 45)
            {
                u.labelResultado(lblResultado, "El Email del cliente no puede tener más de 45 caracteres.", 1, "Error!");
                return;
            }
            if (dir.Length > 80)
            {
                u.labelResultado(lblResultado, "La Dirección del cliente no puede tener más de 80 caracteres.", 1, "Error!");
                return;
            }
            if (alt.Length > 6)
            {
                u.labelResultado(lblResultado, "La Altura del cliente no puede tener más de 6 caracteres.", 1, "Error!");
                return;
            }
            if (pis.Length > 3)
            {
                u.labelResultado(lblResultado, "El Piso del cliente no puede tener más de 3 caracteres.", 1, "Error!");
                return;
            }
            if (dep.Length > 2)
            {
                u.labelResultado(lblResultado, "El Depto del cliente no puede tener más de 2 caracteres.", 1, "Error!");
                return;
            }
            if (nroDoc.Length > 0)
            {
                if (!u.EsNumerico(nroDoc, true))
                {
                    u.labelResultado(lblResultado, "El Nro. de Documento del Cliente no es un dato numérico, sin guiones ni puntos.", 1, "Error!");
                    return;
                }
                else //validamos que el nro. de doc. no exista previamente en BD con otro cliente
                {
                    Clientes cliExistente = new Clientes();
                    cliExistente = Clientes.obtenerPorDNI(nroDoc);
                    if (cliExistente != null)
                    {
                        if (cliExistente.Id != cli.Id)
                        {
                            u.labelResultado(lblResultado, "Ya existe un cliente registrado con ese número de documento.", 1, "Error!");
                            return;
                        }
                    }

                }
            }
            if (raz.Length > 75)
            {
                u.labelResultado(lblResultado, "La Razón Social del Cliente no puede tener más de 75 caracteres.", 1, "Error!");
                return;
            }

            cli.Estado = est;
            cli.Nombre = nom;
            cli.Apellido = ape;
            cli.Direccion = dir;
            cli.Altura = alt;
            cli.Piso = pis;
            cli.Depto = dep;
            cli.Telef = tel;
            cli.Celular = cel;
            cli.Email = ema;
            cli.Tipodoc = (nroDoc.Length > 0 ? ddlModTipoDoc.SelectedValue : "");
            cli.Nrodoc = nroDoc;
            cli.Razsoc = raz;
            cli.Tipoiva = ddlModTipoIVA.SelectedValue;
            cli.Fecnacimiento = txtModFecNac.Text;
            cli.update();

            u.labelResultado(lblResultado, "Cliente modificado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnElimOK_Click(object sender, EventArgs e)
        {
            Permisos.verificarPermisoBaja(Page);
            string idModif = u.desencriptarTexto(Request.QueryString["eId"].ToString());
            Clientes cli = Clientes.obtenerPorId(idModif);
            if (cli == null)
            {
                u.showMessage(this, "Error: no se encuentra el cliente.", 1);
                return;
            }

            cli.delete();
            u.labelResultado(lblResultado, "Cliente eliminado correctamente.", 0, "Éxito!");
            pnlAgregar.Visible = false;
            pnlEliminar.Visible = false;
            pnlModificar.Visible = false;
            pnlVerListado.Visible = true;
            llenarGrilla();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABMClientes.aspx");
        }
    }
}