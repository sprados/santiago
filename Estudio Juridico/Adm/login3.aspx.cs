﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Estudio_Juridico.Adm
{
    public partial class login3 : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            Utiles u = new Utiles();
            u.abrirConexion(Response);
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            Utiles u = new Utiles();
            u.cerrarConexion();
        }

        private bool esIE9()
        {
            Utiles u = new Utiles();
            if (u.esIE9oMenor(Request))
            {
                u.labelResultado(lblResultadoDerivado, "No se puede ingresar al sistema con Internet Explorer 9 (o inferior). Por favor descargue la última versión o intente con otro navegador web, como Firefox o Chrome.", 1, "Navegador Web No Permitido");
                return true;
            }
            else return false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //  Request.Form["user"];

            string idAviso = "";
            Utiles u = new Utiles();

            if (esIE9())
                return;

            if (Session["sUsuario"] != null) //si la sesión del usuario logueado sigue activa, directamente redirijimos al home.aspx
                Response.Redirect("/Adm/Inicio.aspx");

            if (Request.QueryString["idAviso"] != null)
            {
                idAviso = Request.QueryString["idAviso"].ToString();
                if (idAviso == "0")
                {
                    //Mensaje enviado desde Site1.Master.lnkCerrarSesion_Click
                    u.labelResultado(lblResultadoDerivado, "Sesión Finalizada con Éxito.", 0);
                }
                else if (idAviso == "1")
                {
                    //Mensaje enviado desde los Page_Load de las páginas cuando se finaliza la sesión por inactividad.
                  u.labelResultado(lblResultadoDerivado, "Iniciar sesión.", 3);
                }
                else
                {
                    lblResultadoDerivado.Visible = false;
                }
            }

           //Request.Form["user"].Focus();



        }


        protected void lnkIniciarSesion_Click(object sender, EventArgs e)
        {
            if (esIE9())
                return;
            

            Utiles u = new Utiles();
            lblResultadoDerivado.Visible = false;
            Usuarios us = Usuarios.esUsuario(Request.Form["user"], Request.Form["pass"]);
            if (us != null && us.Estado == "a")
            {
                Session["sUsuario"] = us; //cargamos el usuario logueado en una variable de sesión
                Session["idUser"] = us.Id;
                u.ejecutarTareasMantenimiento();

                Response.Redirect("/Adm/Inicio.aspx");
            }
            else
            {
                u.labelResultado(lblResultadoDerivado, "Usuario o password no válida", 1);
            }
        }



    }
}