﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="PlantCtaDoc.aspx.cs" Inherits="Estudio_Juridico.Adm.PlantCtaDoc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div>
        <ul class="breadcrumb">
            <li>
                <h3> Plantillas de Documentos  
                </h3>
            </li>
            <li>
                <p>
                     Seleccione el tipo  de Plantilla a Utilizar y el Cliente  sobre el que se utilizaran los  Datos.
                </p>
            </li>
        </ul>
    </div>
     <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>

        <asp:Panel class="row-fluid" runat="server" ID="pnlAgregar" Visible="true">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;Generacion de Plantilla</h2>
                    <div class="box-icon">
                        <h2>
                            <asp:Label ID="lblAdd" runat="server" Text=""></asp:Label></h2>
                    </div>
                </div>
                <div class="box-content">
                    <div class="span4">
                        <div class="box-content form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Cliente:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="DropDownListClientes" runat="server" data-rel="chosen" CssClass="input-xlarge" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group ">
                                <label class="control-label">Plantilla:</label>
                                <div class="controls">
                                    <asp:DropDownList ID="DropDownListTipoPlantilla" runat="server" data-rel="chosen" CssClass="input-xlarge">
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnGenerarDoc" runat="server" Text="Generar Documento" class="btn btn-primary" OnClick="btnGenerarDoc_Click"  UseSubmitBehavior="false" OnClientClick="this.disabled='true'; this.value='Espere...'"/>
                            &nbsp;&nbsp;&nbsp;
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>







</asp:Content>
