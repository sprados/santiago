﻿using Estudio_Juridico.Adm.clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Estudio_Juridico.Adm
{
    public partial class ConsultasLegajos : System.Web.UI.Page
    {
        Utiles u = new Utiles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Clientes.llenarDDL(ddlClientes, true);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            llenarGrilla();
        }

        public void llenarGrilla()
        {
            litGrilla.Text = "";
            string where = "";


            where = "inner join clientes c on l.idCliente = c.id ";

            if (ddlClientes.SelectedIndex > 0) //clientes
                where += " and c.id = '" + ddlClientes.SelectedValue + "'";
            if (!string.IsNullOrEmpty(u.sqlSeguro(txtDescripcion.Text))) //descripción de legajo
                where += " and l.descripcion like '%" + txtDescripcion.Text + "%'";
            if (!string.IsNullOrEmpty(u.sqlSeguro(txtFecDsd.Text))) //fecha desde
                where += " and l.fecha > '" + txtFecDsd.Text + "'";
            if (!string.IsNullOrEmpty(u.sqlSeguro(txtFecHst.Text)))//fecha hasta
                where += " and l.fecha < '" + txtFecHst.Text + "'";
            if (!string.IsNullOrEmpty(u.sqlSeguro(txtCodigo.Text))) //codigo de legajo
                where += " and l.codigo = '" + txtCodigo.Text + "'";
            if (ddlEstado.SelectedValue == "a") //estado
                where += " and l.estado = 'a'";
            else
                where += " and l.estado = 'b'";

            Legajos[] leg = Legajos.obtenerTodosConFiltros(where);
            StringBuilder s = new StringBuilder();

            if (leg != null)
            {
                foreach (Legajos l in leg)
                {
                    //LOS CARGAMOS EN LA GRILLA.
                    string idEncrip = u.encriptarTexto(l.Id);
                    Clientes cli = Clientes.obtenerPorId(l.IdCliente);

                    s.Append("<tr>");
                    s.Append("<td class='center'>" +
                        "<a style='margin-right: 2px' class='btn btn-mini btn-inverse' href='ABMLegajos.aspx?mId=" + idEncrip + "' data-rel='tooltip' data-title='Ingresar al Legajo...'><i class='icon-color icon-folder-open'></i></a></td>");

                    s.Append("<td>" + l.Descripcion + "</td>");
                    s.Append("<td>" + cli.Apellido + " " + cli.Nombre + "</td>");
                    s.Append("<td>" + l.Fecha + "</td>");
                    s.Append("<td>" + l.Codigo + "</td>");
                    s.Append("<td>" + (l.Estado == "a" ? "<span class='label label-success'>ALTA</span>" : "<span class='label label-important'>BAJA</span>") + "</td>");
                    s.Append("</tr>");
                }

                litGrilla.Text = s.ToString();
            }
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("ConsultasLegajos.aspx");
        }
    }
}