﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adm/Site1.Master" AutoEventWireup="true" CodeBehind="ConsultasLegajos.aspx.cs" Inherits="Estudio_Juridico.Adm.ConsultasLegajos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="loadingNormalPostBack" align="center">
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="Label84" runat="server" ForeColor="#3BB9FF" Text="Cargando... Por favor espere."
                        Width="100%" Font-Size="Medium" Font-Bold="true"></asp:Label>
                    <br />
                    <img src="img/ajax-loaders/ajax-loader-6.gif" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <ul class="breadcrumb">
            <li>
                <h3>CONSULTA DE LEGAJOS
                </h3>
            </li>
            <li>
                <p>
                    Utilice el presente formulario para consultar sus Legajos. 
                </p>
            </li>
        </ul>
    </div>
    <center>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </center>
    <div class="row-fluid" id="divFiltros" runat="server">
        <div class="box span12">
            <div class="box-content">
                <div class="span6">
                    <div class="control-group form-horizontal">
                        <div class="control-group ">
                            <label class="control-label">
                                Seleccionar Cliente:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlClientes" runat="server" data-rel="chosen" CssClass="input-xlarge"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group ">
                            <label class="control-label">
                                Descripción:</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="150" class="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group ">
                            <label class="control-label">Estado:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlEstado" runat="server" data-rel="chosen" CssClass="input-xlarge">
                                    <asp:ListItem Value="a">ALTAS</asp:ListItem>
                                    <asp:ListItem Value="b">BAJAS</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="control-group form-horizontal">
                        <div class="control-group ">
                            <label class="control-label">
                                Fecha Desde:</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFecDsd" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group ">
                            <label class="control-label">
                                Fecha Hasta:</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFecHst" runat="server" MaxLength="10" CssClass="input-large" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group ">
                            <label class="control-label">
                                Código Exp.:</label>
                            <div class="controls">
                                <asp:TextBox ID="txtCodigo" runat="server" MaxLength="50" class="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group ">
                            <label class="control-label"></label>
                            <div class="controls">
                                <asp:Button ID="btnBuscar" runat="server" Text="Buscar Legajos" CssClass="btn btn-primary" OnClick="btnBuscar_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel class="row-fluid" runat="server" ID="pnlVerListado">
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header well">
                    <h2><i class="icon-tasks"></i>&nbsp;&nbsp;LISTADO DE LEGAJOS</h2>
                    <div class="box-icon">
                    </div>
                </div>
                <div class="box-content">
                    <table id="tab1" class="table table-striped table-bordered bootstrap-datatable datatable table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 100px">Acciones</th>
                                <th style="width: auto">Descripción</th>
                                <th style="width: auto">Cliente</th>
                                <th style="width: auto">Fecha</th>
                                <th style="width: auto">Código Exp.</th>
                                <th style="width: auto">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal ID="litGrilla" runat="server"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-content">
                    <center>
                        <div class="box-content">
                            <asp:Button ID="btnSalir1" runat="server" Text="Salir/Cancelar" class="btn btn-danger" OnClick="btnSalir_Click" />
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
