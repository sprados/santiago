﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Estudio_Juridico;
using MySql.Data.MySqlClient;
using System.Web.UI;

namespace Estudio_Juridico
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["errMsg"] = ""; //mensaje de error que se manda a la pagina PageFail.aspx
            MySqlConnection conn = new MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringMySQL"].ConnectionString);
            Session["con"] = conn;

            //ScriptManager.ScriptResourceMapping.AddDefinition(
            //  "MicrosoftAjax.js",
            //  null,
            //  new ScriptResourceDefinition
            //  {
            //      ResourceName = "MicrosoftAjax.js",
            //      ResourceAssembly = typeof(ScriptManager).Assembly,
            //      Path = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjax.js",
            //      DebugPath = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjax.debug.js",
            //      CdnPath = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjax.js",
            //      CdnDebugPath = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjax.debug.js"
            //  });

            //ScriptManager.ScriptResourceMapping.AddDefinition(
            //  "MicrosoftAjaxWebForms.js",
            //  null,
            //  new ScriptResourceDefinition
            //  {
            //      ResourceName = "MicrosoftAjaxWebForms.js",
            //      ResourceAssembly = typeof(ScriptManager).Assembly,
            //      Path = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjaxWebForms.js",
            //      DebugPath = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjaxWebForms.debug.js",
            //      CdnPath = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjaxWebForms.js",
            //      CdnDebugPath = "http://www.wlmessenger.net/MicrosoftAjax/0911/MicrosoftAjaxWebForms.debug.js"
            //  });
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
