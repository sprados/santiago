﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="Estudio_Juridico.Inicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Estudio Jurídico</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Creative Solutions Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free Web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' media="all" />
    <!-- bootstrap -->
    <link href="css/style.css" rel='stylesheet' type='text/css' media="all" />
    <link href="css/component.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- font-awesome icons -->
    <link href="css/swipebox.css" rel="stylesheet" />
    <!-- //Custom Theme files -->
    <!-- js -->
    <script src="js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <!-- web fonts -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
        rel="stylesheet">
    <!-- web fonts -->
</head>
<body class="cbp-spmenu-push">
    <!-- banner -->
    <div class="banner" id="home">
        <div class="container">
            <div class="w3layouts-header">
                <div class="header-left">
                    <h1><a href="inicio.aspx"><span>Estudio Jurídico CABRERIZO</span></a></h1>
                </div>
                <div class="top-nav">
                    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
                        <h3>Menu</h3>
                        <ul>
                            <li><a href="#home" class="scroll">Inicio</a></li>
                            <li><a href="#services" class="scroll">Áreas de Práctica</a></li>
                            <li><a href="#work" class="scroll">Galería</a></li>
                            <li><a href="#contact" class="scroll">Contacto</a></li>
                            <li><a href="adm/login3.aspx">Ingreso</a></li>
                        </ul>
                    </nav>
                    <div class="main buttonset">
                        <!-- Class "cbp-spmenu-open" gets applied to menu and "cbp-spmenu-push-toleft" or "cbp-spmenu-push-toright" to the body -->
                        <button id="showRightPush">
                            <img src="images/menu.png" alt="" /></button>
                        <!-- <span class="menu"></span> -->
                    </div>
                    <!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
                    <script src="js/classie.js"></script>
                    <script>
                        var menuRight = document.getElementById('cbp-spmenu-s2'),
                        showRightPush = document.getElementById('showRightPush'),
                        body = document.body;

                        showRightPush.onclick = function () {
                            classie.toggle(this, 'active');
                            classie.toggle(body, 'cbp-spmenu-push-toleft');
                            classie.toggle(menuRight, 'cbp-spmenu-open');
                            disableOther('showRightPush');
                        };

                        function disableOther(button) {
                            if (button !== 'showRightPush') {
                                classie.toggle(showRightPush, 'disabled');
                            }
                        }
                    </script>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="banner-w3lsinfo">
            </div>
            <div class="agileits-intro">
            </div>
        </div>
    </div>
    <!-- //banner -->
    <!-- introduction -->
    <div class="introduction" id="introduction">
        <div class="container">
            <h3 class="agileits-title">
                <b>Nuestro estudio</b></h3>
            <p>
                El estudio cuenta con una extensa trayectoria en derecho previsional, y además, con el objetivo de brindar soluciones integrales a quienes se acercan al estudio, hemos incorporado también los siguientes servicios jurídicos:
            </p>
        </div>
    </div>
    <!-- //introduction -->
    <!-- download -->
    <div class="download-agile">
        <div class="container">
        </div>
    </div>
    <!-- //download -->
    <!-- logo-singular -->
    <div id="about" class="logo-singular">
        <div class="container">
            <h3 class="agileits-title"></h3>
            <div class="col-md-5 singular-left">
                <div class="singular-grids">
                    <div class="singular-grid agileits-w3layouts">
                        <div class="col-md-3 col-xs-3 singular-grid-left">
                            <span class="glyphicon glyphicon-user"></span>
                        </div>
                        <div class="col-md-9 col-xs-9 singular-grid-right">
                            <h4>María Laura Cabrerizo</h4>
                            <p>
                                Abogada - M.P 1-35371
                            </p>
                            <p>
                                Graduado en Facultad de Abogacía. Universidad Nacional de
                                Córdoba.
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="singular-grid">
                        <div class="col-md-3 col-xs-3 singular-grid-left">
                            <span class="glyphicon glyphicon-info-sign"></span>
                        </div>
                        <div class="col-md-9 col-xs-9 singular-grid-right">
                            <h4>Información de contacto</h4>
                            <p>
                                (0351) 153677877
                            </p>
                            <p>
                                laura_cabrerizo@hotmail.com
                            </p>
                            <p>
                                Arturo M. Bas 308 - 2° piso b
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 singular-right">
                <img src="images/imagen contacto.jpg" />
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <!-- //logo-singular -->
    <!-- services -->
    <div class="services" id="services">
        <div class="container">
            <h3 class="agileits-title">
                <b>Áreas de Práctica</b></h3>
            <div class="service-grids">
                <div class="col-md-3 col-xs-6 services-left w3-agile">
                    <div class="ref">
                        <span class="glyphicon glyphicon-cog"></span>
                    </div>
                    <div class="single-text w3-agile">
                        <p>
                            Previsional
                        </p>
                    </div>
                    <div class="clearfix">
                    </div>
                    <p class="w3-agile-amet">
                        Jubilación Nacional, Jubilación Provincial, Pensión por Fallecimiento, Retiro por Invalidez, Juicio de Reajuste Nacional, Juicio de Reajuste Provincia
                    </p>
                </div>
                <div class="col-md-3 col-xs-6 services-left">
                    <div class="ref4">
                        <span class="glyphicon glyphicon-briefcase"></span>
                    </div>
                    <div class="single-text">
                        <p>
                            Laboral
                        </p>
                    </div>
                    <div class="clearfix">
                    </div>
                    <p class="w3-agile-amet">
                        Accidentes laborales, Enfermedades laborales, Despidos, Diferencia de Haberes
                    </p>
                </div>
                <div class="col-md-3 col-xs-6 services-left">
                    <div class="ref7">
                        <span class="glyphicon glyphicon-bookmark"></span>
                    </div>
                    <div class="single-text">
                        <p>
                            Familia y Sucesiones
                        </p>
                    </div>
                    <div class="clearfix">
                    </div>
                    <p class="w3-agile-amet">
                        Sucesiones, Testamentos, Divorcios
                    </p>
                </div>
                <div class="col-md-3 col-xs-6 services-left">
                    <div class="ref5">
                        <span class="glyphicon glyphicon-gift"></span>
                    </div>
                    <div class="single-text">
                        <p>
                            Civil y Comercial
                        </p>
                    </div>
                    <div class="clearfix">
                    </div>
                    <p class="w3-agile-amet">
                        Contratos en general, Desalojos, Responsabilidad Civil. Daños y perjuicios, Accidentes de tránsito
                    </p>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>
    <!-- //services -->
    <!-- work -->
    <div class="work" id="work">
        <h3 class="agileits-title">
            <b>Galería</b></h3>
        <div class="work-grids">
            <div class="work-grid">
                <a href="images/galeria1.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria1.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 1.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="work-grid">
                <a href="images/galeria2.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria2.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 2.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="work-grid">
                <a href="images/galeria3.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria3.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 3.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="work-grid">
                <a href="images/galeria4.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria4.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 4.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="work-grids">
            <div class="work-grid w3-agileits">
                <a href="images/galeria5.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria5.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 5.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="work-grid w3-agileits">
                <a href="images/galeria7.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria7.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 6
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="work-grid w3-agileits">
                <a href="images/galeria6.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria6.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 7.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="work-grid w3-agileits">
                <a href="images/galeria8.jpg" class="b-link-stripe b-animate-go swipebox" title="">
                    <img src="images/galeria8.jpg" alt="" class="img-responsive" />
                    <div class="b-wrapper">
                        <div class="b-animate b-from-left">
                            <p>
                                Detalle 8.
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <!-- swipebox -->
        <script src="js/jquery.swipebox.min.js"></script>
        <script type="text/javascript">
            jQuery(function ($) {
                $(".swipebox").swipebox();
            });
        </script>
        <!-- //swipebox Ends -->
    </div>
    <!-- //work -->
    <!-- contact -->
    <div class="contact" id="contact">
        <div class="container">
            <p>
                Ingrese a CONTACTO para realizar una consulta
            </p>
            <div class="con">
                <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox wthree-hvr">Contacto</a>
            </div>
            <div class="map-w3ls">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3404.8842442671134!2d-64.19586838431267!3d-31.41731520344171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9432a2808b2d77a9%3A0xe29f1607b91f858c!2sArturo%20M.%20Bas%20308%2C%20C%C3%B3rdoba!5e0!3m2!1ses-419!2sar!4v1571521575728!5m2!1ses-419!2sar" width="600" height="450" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
            </div>
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#"></a>
            <div class="modal fade bs-example-modal-md light-box" tabindex="-1" role="dialog"
                aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content light-box-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                        <div class="pop">
                            <h3>
                                <b>Contacto</b></h3>
                            <div class="contact-grids">
                                <div class="inp agileits-inp">
                                    <h4>Formulario de consulta</h4>
                                    <form id="form1" runat="server" action="#" method="post">
                                        <input type="text" id="txtNombre" name="Name" placeholder="Nombre" required="" runat="server">
                                        <input type="email" id="txtMail" name="Email" placeholder="Email" required="" runat="server">
                                        <input type="text" id="txtTelefono" name="Subject" placeholder="Teléfono" required=""
                                            runat="server">
                                        <textarea name="Message" id="txtMensaje" placeholder="Mensaje" required="" runat="server"></textarea>
                                        <asp:Button ID="btnEnviar" runat="server" Text="ENVIAR" OnClick="btnEnviar_Click" />
                                    </form>
                                </div>
                                <h5>
                                    <asp:Label ID="lblMensaje" runat="server" Visible="false"></asp:Label></h5>
                                <div class="contact-grid">
                                    <h4>Dirección
                                        <p class="agile-qwe">
                                            Arturo M. Bas 308 - 2° piso b
                                        </p>
                                </div>
                                <div class="contact-grid">
                                    <h4>Teléfonos</h4>
                                    <p class="agile-qwe">
                                        (0351) 153677877
                                        <br />
                                    </p>
                                </div>
                                <div class="contact-grid">
                                    <h4>Mail</h4>
                                    <p class="agile-qwe">
                                        <a href="mailto:laura_cabrerizo@hotmail.com">laura_cabrerizo@hotmail.com</a>
                                    </p>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //contact -->
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="footer-left agileits">
                <%--Sitio desarrollado por <a href="mailto:martintchobanian@gmail.com?Subject=Sitio Web">Sistemas Web Blunian</a> - Todos los derechos reservados &reg;--%>
                <p>
                    © Todos los derechos reservados | Sitio desarrollado por <a href="mailto:martintchobanian@gmail.com?Subject=Sitio Web"
                        target="_blank"><b>Sistemas Web Blunian.</b></a>
                </p>
            </div>
            <div class="footer-right">
                <div class="social-icon agileits">
                    <a href="#" class="social-button twitter"><i class="fa fa-twitter"></i></a><a href="#"
                        class="social-button facebook"><i class="fa fa-facebook"></i></a><a href="#" class="social-button google">
                            <i class="fa fa-google-plus"></i></a><a href="#" class="social-button dribbble"><i
                                class="fa fa-dribbble"></i></a><a href="#" class="social-button skype"><i class="fa fa-skype"></i></a>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <!-- //footer -->
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
            });
        });
    </script>
    <!-- start-smooth-scrolling -->
    <!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function () {
            /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
            */

            $().UItoTop({ easingType: 'easeOutQuart' });

        });
    </script>
    <!-- //here ends scrolling icon -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>
</html>

