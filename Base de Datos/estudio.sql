CREATE DATABASE  IF NOT EXISTS `estudio_juridico` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `estudio_juridico`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: estudio_juridico
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `telef` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `estado` char(1) DEFAULT 'a',
  `tipodoc` int(11) DEFAULT NULL,
  `nrodoc` varchar(15) DEFAULT NULL,
  `razsoc` varchar(75) NOT NULL,
  `tipoiva` char(1) DEFAULT 'C',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1143 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_permisos`
--

DROP TABLE IF EXISTS `menu_permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_permisos` (
  `idMenu` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `visible` tinyint(4) DEFAULT '1',
  `posicion` int(11) DEFAULT NULL,
  `alta` tinyint(4) DEFAULT '0',
  `baja` tinyint(4) DEFAULT '0',
  `modif` tinyint(4) DEFAULT '0',
  `consulta` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idMenu`,`idUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permisos`
--

LOCK TABLES `menu_permisos` WRITE;
/*!40000 ALTER TABLE `menu_permisos` DISABLE KEYS */;
INSERT INTO `menu_permisos` VALUES (24,20,1,1,1,1,1,1),(34,20,1,2,1,1,1,1),(18,20,1,3,1,1,1,1);
/*!40000 ALTER TABLE `menu_permisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menues`
--

DROP TABLE IF EXISTS `menues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `tooltip` varchar(250) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `idPadre` int(11) DEFAULT NULL,
  `siempreVisibleAdmin` tinyint(4) DEFAULT '0',
  `PosicionPorDefecto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menues`
--

LOCK TABLES `menues` WRITE;
/*!40000 ALTER TABLE `menues` DISABLE KEYS */;
INSERT INTO `menues` VALUES (23,'CLIENTES',NULL,NULL,0,0,1),(29,'SEGURIDAD',NULL,NULL,0,0,7),(24,'Clientes',NULL,'/ABMClientes.aspx',23,0,1),(34,'Usuarios del Sistema',NULL,'/ABMUsuarios.aspx',29,1,5),(18,'Permisos de Usuarios',NULL,'/ADMPERMISOS.ASPX',29,1,1);
/*!40000 ALTER TABLE `menues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `nuevospermisos` char(1) DEFAULT NULL COMMENT 'Indica si se le han asignado recientemente nuevos permisos de menues',
  `puedeVerPrecios` char(1) NOT NULL DEFAULT '0',
  `nuevanovedad` char(1) DEFAULT '0' COMMENT 'indica si se le han asignado nuevas novedades',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (20,'martin','1234','Martin',NULL,NULL,'a',NULL,'0','0');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-15 20:10:50
