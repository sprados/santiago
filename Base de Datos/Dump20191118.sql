CREATE DATABASE  IF NOT EXISTS `estudio_juridico` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `estudio_juridico`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: estudio_juridico
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actores_x_legajo`
--

DROP TABLE IF EXISTS `actores_x_legajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actores_x_legajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `apellido` varchar(80) DEFAULT NULL,
  `tipodoc` char(2) DEFAULT '96',
  `nrodoc` varchar(10) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `altura` varchar(6) DEFAULT '0',
  `piso` varchar(3) DEFAULT '0',
  `depto` varchar(2) DEFAULT NULL,
  `telef` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `fecnacimiento` varchar(10) DEFAULT NULL,
  `estado` char(1) DEFAULT 'a',
  `idLegajo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actores_x_legajo`
--

LOCK TABLES `actores_x_legajo` WRITE;
/*!40000 ALTER TABLE `actores_x_legajo` DISABLE KEYS */;
INSERT INTO `actores_x_legajo` VALUES (5,'Teresa','FALCO','96','10772961','Lascano Colodrero','2655','0','0','4761540','156456789','terefalscam@yahoo.com','1966-06-06','a',3),(6,'Francisco','BLUNNO','96','12345678','Soler','36','0','0','123456789','987654321','algo@algo.com.ar','2000-11-22','b',2),(8,'Santiago','PRADOS','96','31997856','Claudio Cuenca 1828','','','','0351157052011','','santiago_prados@hotmail.com','1986-02-23','a',2);
/*!40000 ALTER TABLE `actores_x_legajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adjuntos_x_legajo`
--

DROP TABLE IF EXISTS `adjuntos_x_legajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adjuntos_x_legajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLegajo` varchar(5) DEFAULT NULL,
  `rutaAdjunto` varchar(200) DEFAULT NULL,
  `fecha_alta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=295 DEFAULT CHARSET=latin1 COMMENT='en esta tabla se guardarán todas las rutas físicas de los ar';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adjuntos_x_legajo`
--

LOCK TABLES `adjuntos_x_legajo` WRITE;
/*!40000 ALTER TABLE `adjuntos_x_legajo` DISABLE KEYS */;
INSERT INTO `adjuntos_x_legajo` VALUES (272,'2','/adj/legajos/2/-20191105190140-0.gsheet',''),(274,'2','/adj/legajos/2/-20191105191854-0.jpg','05-11-19'),(289,'1','/adj/legajos/1/288-20191105192539-1.jpg','05-11-19'),(288,'1','/adj/legajos/1/-20191105192539-0.jpg','05-11-19'),(287,'2','/adj/legajos/2/286-20191105192501-1.jpg','05-11-19'),(290,'2','/adj/legajos/2/-20191106195715-0.jpg','06-11-19'),(294,'3','/adj/legajos/3/-20191107201403-0.jpg','07-11-19'),(292,'2','/adj/legajos/2/-20191106204603-0.jpg','06-11-19');
/*!40000 ALTER TABLE `adjuntos_x_legajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `apellido` varchar(80) DEFAULT NULL,
  `tipodoc` char(2) DEFAULT '96',
  `nrodoc` varchar(10) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `altura` varchar(6) DEFAULT '0',
  `piso` varchar(3) DEFAULT '0',
  `depto` varchar(2) DEFAULT NULL,
  `telef` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `razsoc` varchar(75) DEFAULT NULL,
  `tipoiva` char(1) DEFAULT 'C',
  `fecnacimiento` varchar(10) DEFAULT NULL,
  `estado` char(1) DEFAULT 'a',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Cristina','BLUNNO','96','32542000','VARELA BERRO','4369','0','A','4761540','3512399520','cristinablunnorpi@gmail.com','','C','2017-08-25','a'),(2,'Martin','TCHOBANIAN','96','31218295','VARELA BERRO ','4369','500','A','03512356673','','martintchobanian@gmail.com','','C','2017-11-17','a'),(3,'Franco','ABATE','96','26179528','Armenios','2795','','','4761540','152384679','franco@franco.com.ar','','C','1980-11-14','a'),(4,'Santiago','PRADOS','96','31997856','Obispo Arregui','4652','','','0351157052011','','santiago_prados@hotmail.com','','C','1986-02-23','a');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `event_start` datetime DEFAULT NULL,
  `event_end` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Tabla que registra los eventos de la Agenda';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (4,'Capacitacion Laboral','Evento en ciudad universitarioa','2019-11-11 00:00:00','2019-11-11 00:00:00'),(22,'Teleconferencia','En ciudad universitaria','2019-11-17 00:00:00','2019-11-17 00:00:00'),(24,'Reunion jsugado','claudio torres','2019-11-19 13:30:00','2019-11-19 19:30:00');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueros`
--

DROP TABLE IF EXISTS `fueros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueros` (
  `idFueros` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  PRIMARY KEY (`idFueros`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueros`
--

LOCK TABLES `fueros` WRITE;
/*!40000 ALTER TABLE `fueros` DISABLE KEYS */;
INSERT INTO `fueros` VALUES (1,'Fuero Civil','a'),(2,'Fuero Penal','a'),(3,'Fuero Laboral','a');
/*!40000 ALTER TABLE `fueros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial_x_legajo`
--

DROP TABLE IF EXISTS `historial_x_legajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial_x_legajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `historial` varchar(5000) DEFAULT NULL,
  `idLegajo` int(11) NOT NULL,
  `fecha` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial_x_legajo`
--

LOCK TABLES `historial_x_legajo` WRITE;
/*!40000 ALTER TABLE `historial_x_legajo` DISABLE KEYS */;
INSERT INTO `historial_x_legajo` VALUES (1,'Inicio de Tramite',3,20191107),(2,'Consulta de avance de causa en jusgado 45 . Sin novedades .',3,20191118);
/*!40000 ALTER TABLE `historial_x_legajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juzgados`
--

DROP TABLE IF EXISTS `juzgados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `juzgados` (
  `idJuzgados` int(11) NOT NULL AUTO_INCREMENT,
  `idFueros` int(11) NOT NULL,
  `juzgado` varchar(100) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  PRIMARY KEY (`idJuzgados`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juzgados`
--

LOCK TABLES `juzgados` WRITE;
/*!40000 ALTER TABLE `juzgados` DISABLE KEYS */;
INSERT INTO `juzgados` VALUES (1,1,'3° Nominación','a'),(2,2,'1° Nominación','a'),(3,3,'1° Nominación','a');
/*!40000 ALTER TABLE `juzgados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legajos`
--

DROP TABLE IF EXISTS `legajos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legajos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `fecha` varchar(10) DEFAULT NULL,
  `idCliente` int(11) NOT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `estado` char(1) DEFAULT 'a',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legajos`
--

LOCK TABLES `legajos` WRITE;
/*!40000 ALTER TABLE `legajos` DISABLE KEYS */;
INSERT INTO `legajos` VALUES (2,'Declaratoria de Herederos','2017-11-01',1,'DEC2','a'),(3,'Jubilacion','2019-11-01',3,'JUB3','a');
/*!40000 ALTER TABLE `legajos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_permisos`
--

DROP TABLE IF EXISTS `menu_permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_permisos` (
  `idMenu` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `visible` tinyint(4) DEFAULT '1',
  `posicion` int(11) DEFAULT NULL,
  `alta` tinyint(4) DEFAULT '0',
  `baja` tinyint(4) DEFAULT '0',
  `modif` tinyint(4) DEFAULT '0',
  `consulta` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idMenu`,`idUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permisos`
--

LOCK TABLES `menu_permisos` WRITE;
/*!40000 ALTER TABLE `menu_permisos` DISABLE KEYS */;
INSERT INTO `menu_permisos` VALUES (70,20,1,1,1,1,1,1),(66,20,1,2,1,1,1,1),(65,20,1,1,1,1,1,1),(18,20,1,2,1,1,1,1),(34,20,1,1,1,1,1,1),(67,20,1,3,1,1,1,1),(62,20,1,2,1,1,1,1),(24,20,1,1,1,1,1,1),(72,20,1,1,1,1,1,1);
/*!40000 ALTER TABLE `menu_permisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menues`
--

DROP TABLE IF EXISTS `menues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `tooltip` varchar(250) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `idPadre` int(11) DEFAULT NULL,
  `siempreVisibleAdmin` tinyint(4) DEFAULT '0',
  `PosicionPorDefecto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menues`
--

LOCK TABLES `menues` WRITE;
/*!40000 ALTER TABLE `menues` DISABLE KEYS */;
INSERT INTO `menues` VALUES (23,'CLIENTES',NULL,NULL,0,0,1),(29,'SEGURIDAD',NULL,NULL,0,0,7),(24,'Clientes',NULL,'/ABMClientes.aspx',23,0,1),(34,'Usuarios del Sistema',NULL,'/ABMUsuarios.aspx',29,1,1),(18,'Permisos de Usuarios',NULL,'/ADMPERMISOS.ASPX',29,1,2),(62,'Legajos',NULL,'/ABMLegajos.aspx',23,1,2),(64,'PARÁMETROS',NULL,NULL,0,1,1),(65,'Fueros','','/ABMFueros.aspx',64,1,1),(66,'Juzgados',NULL,'/ABMJuzgados.aspx',64,1,2),(67,'Consultas',NULL,'/ConsultasLegajos.aspx',23,1,3),(69,'AGENDA',NULL,NULL,0,1,1),(70,'Agenda',NULL,'/Agenda.aspx',69,1,1),(71,'PLANTILLAS',NULL,NULL,0,1,1),(72,'Cartas Documento',NULL,'/PlantCtaDoc.aspx',71,1,1);
/*!40000 ALTER TABLE `menues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_x_legajo`
--

DROP TABLE IF EXISTS `pago_x_legajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_x_legajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` double NOT NULL DEFAULT '0',
  `fecha` varchar(10) NOT NULL,
  `idLegajo` int(11) NOT NULL,
  `observaciones` varchar(500) DEFAULT NULL,
  `moneda` char(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_x_legajo`
--

LOCK TABLES `pago_x_legajo` WRITE;
/*!40000 ALTER TABLE `pago_x_legajo` DISABLE KEYS */;
INSERT INTO `pago_x_legajo` VALUES (1,25000,'2019-11-18',3,'Primer pago de trámite','$'),(2,1500.35,'2019-11-01',2,'Cosnsulta Expendiente','$'),(3,560.45,'2019-11-18',3,'Pago de sellados','$'),(4,1356.45,'2019-11-01',3,'Arancel carta documento','$');
/*!40000 ALTER TABLE `pago_x_legajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sqlerrores`
--

DROP TABLE IF EXISTS `sqlerrores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sqlerrores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sqlerror` text,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `errormsg` text,
  `sqlerrorescol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='Log de registro de errores al ejecutar consulstas sql';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sqlerrores`
--

LOCK TABLES `sqlerrores` WRITE;
/*!40000 ALTER TABLE `sqlerrores` DISABLE KEYS */;
INSERT INTO `sqlerrores` VALUES (59,'select * from clientes where id=','2019-11-11','20:13:44',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1',NULL),(60,'select * from clientes where id=','2019-11-11','20:19:27',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1',NULL),(61,'select * from clientes where id=','2019-11-11','20:22:28',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1',NULL),(62,'select * from clientes where id=','2019-11-11','20:25:49',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1',NULL),(63,'select * from actores_x_legajo where idLegajo = 1 order by estado asc','2019-11-17','12:45:49',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(64,'select * from actores_x_legajo where idLegajo = 1 order by estado asc','2019-11-17','12:46:23',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(65,'select * from actores_x_legajo where idLegajo = 1 order by estado asc','2019-11-17','12:58:32',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(66,'insert into actores_x_legajo (nombre,apellido,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'1\')','2019-11-17','12:59:09',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(67,'select * from actores_x_legajo where idLegajo = 2 order by estado asc','2019-11-17','12:59:14',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(68,'select * from actores_x_legajo where idLegajo = 1 order by estado asc','2019-11-17','12:59:30',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(69,'insert into actores_x_legajo (nombre,apellido,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'1\')','2019-11-17','13:00:39',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(70,'select * from actores_x_legajo where idLegajo = 1 order by estado asc','2019-11-17','13:00:41',20,'Table \'estudio_juridico.actores_x_legajo\' doesn\'t exist',NULL),(71,'update clientes set nombre=UPPER(LEFT(franco, 1)),LOWER(SUBSTRING(franco, 2)),apellido=UPPER(LEFT(ABATEE, 1)),LOWER(SUBSTRING(ABATEE, 2)),tipodoc=\'96\',nrodoc=\'26179528\',direccion=\'Armenios\',altura=\'2795\',piso=\'\',depto=\'\',telef=\'4761540\',celular=\'152384679\',email=\'franco@franco.com.ar\',razsoc=\'\',tipoiva=\'C\',fecnacimiento=\'1980-11-14\',estado=\'a\' where id=3','2019-11-18','12:44:02',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'(SUBSTRING(franco, 2)),apellido=UPPER(LEFT(ABATEE, 1)),LOWER(SUBSTRING(ABATEE, 2\' at line 1',NULL),(72,'insert into actores_x_legajo (Santiago,Prados,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'santiago\',\'PRADOS\',\'\',\'\',\'Claudio Cuenca 1828\',\'\',\'\',\'\',\'0351157052011\',\'\',\'santiago_prados@hotmail.com\',\'\',\'2\')','2019-11-18','13:52:17',20,'Unknown column \'Santiago\' in \'field list\'',NULL),(73,'insert into actores_x_legajo (Santiago,Prados,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'Santiago\',\'PRADOS\',\'\',\'\',\'Claudio Cuenca 1828\',\'\',\'\',\'\',\'0351157052011\',\'\',\'santiago_prados@hotmail.com\',\'\',\'3\')','2019-11-18','13:52:54',20,'Unknown column \'Santiago\' in \'field list\'',NULL),(74,'insert into actores_x_legajo (,,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'3\')','2019-11-18','14:03:01',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \',tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,id\' at line 1',NULL),(75,'insert into actores_x_legajo (,,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'3\')','2019-11-18','14:03:10',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \',tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,id\' at line 1',NULL),(76,'insert into actores_x_legajo (Santiago,PRADOS,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'Santiago\',\'PRADOS\',\'96\',\'31997856\',\'Claudio Cuenca 1828\',\'\',\'\',\'\',\'0351157052011\',\'\',\'santiago_prados@hotmail.com\',\'1986-02-23\',\'3\')','2019-11-18','14:03:35',20,'Unknown column \'Santiago\' in \'field list\'',NULL),(77,'insert into actores_x_legajo (,,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'2\')','2019-11-18','14:13:15',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \',tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,id\' at line 1',NULL),(78,'insert into actores_x_legajo (,,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'3\')','2019-11-18','14:19:53',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \',tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,id\' at line 1',NULL),(79,'insert into actores_x_legajo (,,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'3\')','2019-11-18','14:24:20',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \',tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,id\' at line 1',NULL),(80,'insert into actores_x_legajo (,,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'\',\'3\')','2019-11-18','14:27:33',20,'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \',tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,id\' at line 1',NULL),(81,'insert into actores_x_legajo (Santiago,PRADOS,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'Santiago\',\'PRADOS\',\'\',\'\',\'Claudio Cuenca 1828\',\'\',\'\',\'\',\'0351157052011\',\'\',\'santiago_prados@hotmail.com\',\'\',\'2\')','2019-11-18','19:17:45',20,'Unknown column \'Santiago\' in \'field list\'',NULL),(82,'insert into actores_x_legajo (Santiago,SANTIAGO,tipodoc,nrodoc,direccion,altura,piso,depto,telef,celular,email,fecnacimiento,idLegajo) values (\'Santiago\',\'SANTIAGO\',\'96\',\'31997856\',\'Claudio Cuenca 1828\',\'\',\'\',\'\',\'0351157052011\',\'\',\'santiago_prados@hotmail.com\',\'\',\'2\')','2019-11-18','19:33:52',20,'Unknown column \'Santiago\' in \'field list\'',NULL);
/*!40000 ALTER TABLE `sqlerrores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `nuevospermisos` char(1) DEFAULT NULL COMMENT 'Indica si se le han asignado recientemente nuevos permisos de menues',
  `nuevanovedad` char(1) DEFAULT '0' COMMENT 'indica si se le han asignado nuevas novedades',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (20,'martin','1234','Martin','A','','a','0','0');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-18 19:40:14
