-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: estudio_juridico
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adjuntos_x_legajo`
--

DROP TABLE IF EXISTS `adjuntos_x_legajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adjuntos_x_legajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLegajo` varchar(5) DEFAULT NULL,
  `rutaAdjunto` varchar(200) DEFAULT NULL,
  `fecha_alta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=290 DEFAULT CHARSET=latin1 COMMENT='en esta tabla se guardarán todas las rutas físicas de los ar';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adjuntos_x_legajo`
--

LOCK TABLES `adjuntos_x_legajo` WRITE;
/*!40000 ALTER TABLE `adjuntos_x_legajo` DISABLE KEYS */;
INSERT INTO `adjuntos_x_legajo` VALUES (272,'2','/adj/legajos/2/-20191105190140-0.gsheet',''),(274,'2','/adj/legajos/2/-20191105191854-0.jpg','05-11-19'),(289,'1','/adj/legajos/1/288-20191105192539-1.jpg','05-11-19'),(288,'1','/adj/legajos/1/-20191105192539-0.jpg','05-11-19'),(287,'2','/adj/legajos/2/286-20191105192501-1.jpg','05-11-19'),(283,'2','/adj/legajos/2/-20191105192419-0.jpg','05-11-19'),(284,'2','/adj/legajos/2/283-20191105192419-1.jpg','05-11-19');
/*!40000 ALTER TABLE `adjuntos_x_legajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `apellido` varchar(80) DEFAULT NULL,
  `tipodoc` char(2) DEFAULT '96',
  `nrodoc` varchar(10) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `altura` varchar(6) DEFAULT '0',
  `piso` varchar(3) DEFAULT '0',
  `depto` varchar(2) DEFAULT NULL,
  `telef` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `razsoc` varchar(75) DEFAULT NULL,
  `tipoiva` char(1) DEFAULT 'C',
  `fecnacimiento` varchar(10) DEFAULT NULL,
  `estado` char(1) DEFAULT 'a',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Cristina','BLUNNO','96','32542000','VARELA BERRO','4369','0','A','4761540','3512399520','cristinablunnorpi@gmail.com','','C','06/12/1986','a'),(2,'Martin','TCHOBANIAN','96','31218295','VARELA BERRO 4369 DPTO A','','500','','03512356673','','martintchobanian@gmail.com','','C','','a');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueros`
--

DROP TABLE IF EXISTS `fueros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueros` (
  `idFueros` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  PRIMARY KEY (`idFueros`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueros`
--

LOCK TABLES `fueros` WRITE;
/*!40000 ALTER TABLE `fueros` DISABLE KEYS */;
INSERT INTO `fueros` VALUES (1,'Fuero Civil','a'),(2,'Fuero Penal','a'),(3,'Fuero Laboral','a');
/*!40000 ALTER TABLE `fueros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juzgados`
--

DROP TABLE IF EXISTS `juzgados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `juzgados` (
  `idJuzgados` int(11) NOT NULL AUTO_INCREMENT,
  `idFueros` int(11) NOT NULL,
  `juzgado` varchar(100) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  PRIMARY KEY (`idJuzgados`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juzgados`
--

LOCK TABLES `juzgados` WRITE;
/*!40000 ALTER TABLE `juzgados` DISABLE KEYS */;
INSERT INTO `juzgados` VALUES (1,1,'3° Nominación','a'),(2,2,'1° Nominación','a'),(3,3,'1° Nominación','a');
/*!40000 ALTER TABLE `juzgados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legajos`
--

DROP TABLE IF EXISTS `legajos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legajos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `fecha` varchar(10) DEFAULT NULL,
  `idCliente` int(11) NOT NULL,
  `historial` text,
  `codigo` varchar(50) DEFAULT NULL,
  `estado` char(1) DEFAULT 'a',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legajos`
--

LOCK TABLES `legajos` WRITE;
/*!40000 ALTER TABLE `legajos` DISABLE KEYS */;
INSERT INTO `legajos` VALUES (1,'Esto es un legajo de prueba','2019-11-04',2,'Hola que tal, todo bien!!!!??????','','a'),(2,'Lagajosssss','2017-11-01',1,'(2017-11-01) <span style=\"color: rgb(255, 0, 0); font-weight: bold;\">fdfsfsfsdfds más </span><span style=\"font-weight: bold;\">acotaciones</span><span style=\"color: rgb(255, 0, 0); font-weight: bold;\">!!!!!</span>','asdfghjkl','a');
/*!40000 ALTER TABLE `legajos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_permisos`
--

DROP TABLE IF EXISTS `menu_permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_permisos` (
  `idMenu` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `visible` tinyint(4) DEFAULT '1',
  `posicion` int(11) DEFAULT NULL,
  `alta` tinyint(4) DEFAULT '0',
  `baja` tinyint(4) DEFAULT '0',
  `modif` tinyint(4) DEFAULT '0',
  `consulta` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idMenu`,`idUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permisos`
--

LOCK TABLES `menu_permisos` WRITE;
/*!40000 ALTER TABLE `menu_permisos` DISABLE KEYS */;
INSERT INTO `menu_permisos` VALUES (65,20,1,1,1,1,1,1),(18,20,1,2,1,1,1,1),(34,20,1,1,1,1,1,1),(62,20,1,2,1,1,1,1),(24,20,1,1,1,1,1,1),(66,20,1,2,1,1,1,1);
/*!40000 ALTER TABLE `menu_permisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menues`
--

DROP TABLE IF EXISTS `menues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `tooltip` varchar(250) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `idPadre` int(11) DEFAULT NULL,
  `siempreVisibleAdmin` tinyint(4) DEFAULT '0',
  `PosicionPorDefecto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menues`
--

LOCK TABLES `menues` WRITE;
/*!40000 ALTER TABLE `menues` DISABLE KEYS */;
INSERT INTO `menues` VALUES (23,'CLIENTES',NULL,NULL,0,0,1),(29,'SEGURIDAD',NULL,NULL,0,0,7),(24,'Clientes',NULL,'/ABMClientes.aspx',23,0,1),(34,'Usuarios del Sistema',NULL,'/ABMUsuarios.aspx',29,1,1),(18,'Permisos de Usuarios',NULL,'/ADMPERMISOS.ASPX',29,1,2),(62,'Legajos',NULL,'/ABMLegajos.aspx',23,1,2),(64,'PARÁMETROS',NULL,NULL,0,1,1),(65,'Fueros','','/ABMFueros.aspx',64,1,1),(66,'Juzgados',NULL,'/ABMJuzgados.aspx',64,1,2);
/*!40000 ALTER TABLE `menues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `nuevospermisos` char(1) DEFAULT NULL COMMENT 'Indica si se le han asignado recientemente nuevos permisos de menues',
  `nuevanovedad` char(1) DEFAULT '0' COMMENT 'indica si se le han asignado nuevas novedades',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (20,'martin','1234','Martin','A','','a','0','0');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-05 21:44:17
